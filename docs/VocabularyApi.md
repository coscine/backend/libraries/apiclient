# Coscine.ApiClient.Core.Api.VocabularyApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2VocabulariesOptions**](VocabularyApi.md#apiv2vocabulariesoptions) | **OPTIONS** /api/v2/vocabularies | Responds with the HTTP methods allowed for the endpoint. |
| [**GetVocabularies**](VocabularyApi.md#getvocabularies) | **GET** /api/v2/vocabularies | Retrieves top-level instances from vocabularies. |
| [**GetVocabularyInstance**](VocabularyApi.md#getvocabularyinstance) | **GET** /api/v2/vocabularies/instances/{instance} | Retrieves a single instance from a vocabulary. |
| [**GetVocabularyInstances**](VocabularyApi.md#getvocabularyinstances) | **GET** /api/v2/vocabularies/instances | Retrieves vocabulary instances. |

<a id="apiv2vocabulariesoptions"></a>
# **ApiV2VocabulariesOptions**
> void ApiV2VocabulariesOptions ()

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2VocabulariesOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new VocabularyApi(config);

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2VocabulariesOptions();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling VocabularyApi.ApiV2VocabulariesOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2VocabulariesOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2VocabulariesOptionsWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling VocabularyApi.ApiV2VocabulariesOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getvocabularies"></a>
# **GetVocabularies**
> VocabularyDtoPagedResponse GetVocabularies (string? searchTerm = null, AcceptedLanguage? language = null, int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Retrieves top-level instances from vocabularies.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetVocabulariesExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new VocabularyApi(config);
            var searchTerm = "searchTerm_example";  // string? | Gets or sets the search term used to filter vocabularies. (optional) 
            var language = new AcceptedLanguage?(); // AcceptedLanguage? | Gets or sets the language for which the vocabularies are requested. (optional) 
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves top-level instances from vocabularies.
                VocabularyDtoPagedResponse result = apiInstance.GetVocabularies(searchTerm, language, pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling VocabularyApi.GetVocabularies: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetVocabulariesWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves top-level instances from vocabularies.
    ApiResponse<VocabularyDtoPagedResponse> response = apiInstance.GetVocabulariesWithHttpInfo(searchTerm, language, pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling VocabularyApi.GetVocabulariesWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **searchTerm** | **string?** | Gets or sets the search term used to filter vocabularies. | [optional]  |
| **language** | [**AcceptedLanguage?**](AcceptedLanguage?.md) | Gets or sets the language for which the vocabularies are requested. | [optional]  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**VocabularyDtoPagedResponse**](VocabularyDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the top-level vocabularies. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getvocabularyinstance"></a>
# **GetVocabularyInstance**
> VocabularyInstanceDtoResponse GetVocabularyInstance (string instance, AcceptedLanguage? acceptLanguage = null)

Retrieves a single instance from a vocabulary.

Could be a top-level instance, or an intermediate-level instance from a vocabulary.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetVocabularyInstanceExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new VocabularyApi(config);
            var instance = "instance_example";  // string | The URI of the vocabulary instance to retrieve.
            var acceptLanguage = new AcceptedLanguage?(); // AcceptedLanguage? | The preferred language for the instance data. (optional) 

            try
            {
                // Retrieves a single instance from a vocabulary.
                VocabularyInstanceDtoResponse result = apiInstance.GetVocabularyInstance(instance, acceptLanguage);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling VocabularyApi.GetVocabularyInstance: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetVocabularyInstanceWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves a single instance from a vocabulary.
    ApiResponse<VocabularyInstanceDtoResponse> response = apiInstance.GetVocabularyInstanceWithHttpInfo(instance, acceptLanguage);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling VocabularyApi.GetVocabularyInstanceWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **instance** | **string** | The URI of the vocabulary instance to retrieve. |  |
| **acceptLanguage** | [**AcceptedLanguage?**](AcceptedLanguage?.md) | The preferred language for the instance data. | [optional]  |

### Return type

[**VocabularyInstanceDtoResponse**](VocabularyInstanceDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the vocabulary instance. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getvocabularyinstances"></a>
# **GetVocabularyInstances**
> VocabularyInstanceDtoPagedResponse GetVocabularyInstances (string varClass, string? searchTerm = null, AcceptedLanguage? language = null, int? pageSize = null, int? pageNumber = null, string? orderBy = null)

Retrieves vocabulary instances.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetVocabularyInstancesExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new VocabularyApi(config);
            var varClass = "varClass_example";  // string | Gets or sets the URI class, which is a required field.
            var searchTerm = "searchTerm_example";  // string? | Gets or sets the search term used to filter vocabulary instances. (optional) 
            var language = new AcceptedLanguage?(); // AcceptedLanguage? | Gets or sets the language for which the vocabulary instances are requested. (optional) 
            var pageSize = 56;  // int? | Number of items per page. The maximum number of items per page is `150`. (optional) 
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves vocabulary instances.
                VocabularyInstanceDtoPagedResponse result = apiInstance.GetVocabularyInstances(varClass, searchTerm, language, pageSize, pageNumber, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling VocabularyApi.GetVocabularyInstances: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetVocabularyInstancesWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves vocabulary instances.
    ApiResponse<VocabularyInstanceDtoPagedResponse> response = apiInstance.GetVocabularyInstancesWithHttpInfo(varClass, searchTerm, language, pageSize, pageNumber, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling VocabularyApi.GetVocabularyInstancesWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **varClass** | **string** | Gets or sets the URI class, which is a required field. |  |
| **searchTerm** | **string?** | Gets or sets the search term used to filter vocabulary instances. | [optional]  |
| **language** | [**AcceptedLanguage?**](AcceptedLanguage?.md) | Gets or sets the language for which the vocabulary instances are requested. | [optional]  |
| **pageSize** | **int?** | Number of items per page. The maximum number of items per page is &#x60;150&#x60;. | [optional]  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**VocabularyInstanceDtoPagedResponse**](VocabularyInstanceDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the vocabulary instances. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

