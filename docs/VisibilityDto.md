# Coscine.ApiClient.Core.Model.VisibilityDto
Represents a Data Transfer Object (DTO) for visibility settings.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | The identifier for the visibility setting. | [optional] 
**DisplayName** | **string** | The display name for the visibility setting. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

