# Coscine.ApiClient.Core.Model.ResourceContentPageColumnsDto
Represents a set of columns to be displayed in the content view.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Always** | **List&lt;string&gt;** | Set of columns that should always be displayed in the content view. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

