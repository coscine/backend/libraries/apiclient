# Coscine.ApiClient.Core.Model.OrganizationForProjectManipulationDto
Data transfer object (DTO) representing an organization for project manipulation.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Uri** | **string** | Gets or initializes the URI of the organization. | 
**Responsible** | **bool** | Gets or initializes if the organization is responsible. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

