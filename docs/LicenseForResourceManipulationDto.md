# Coscine.ApiClient.Core.Model.LicenseForResourceManipulationDto
Data transfer object (DTO) representing a license for resource manipulation.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Gets or initializes the identifier of the license. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

