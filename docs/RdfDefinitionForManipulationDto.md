# Coscine.ApiClient.Core.Model.RdfDefinitionForManipulationDto
Represents the data transfer object (DTO) used for manipulating RDF definitions.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Content** | **string** | The content of the RDF definition. | 
**Type** | **RdfFormat** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

