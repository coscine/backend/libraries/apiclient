# Coscine.ApiClient.Core.Model.LanguageForUserManipulationDto
Data transfer object (DTO) representing a language for user manipulation.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Gets or initializes the identifier of the language. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

