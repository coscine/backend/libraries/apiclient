# Coscine.ApiClient.Core.Model.ProjectRoleForProjectManipulationDto
Data transfer object (DTO) representing a project role for manipulation within a project.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RoleId** | **Guid** | Gets or initializes the identifier of the role associated with the project manipulation. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

