# Coscine.ApiClient.Core.Model.ProvenanceForUpdateDto
Data transfer object (DTO) representing the update of provenance  Inherits from the base class Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ProvenanceParametersDto.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WasRevisionOf** | **List&lt;string&gt;** | Gets or sets the adapted versions from the specific metadata tree. | [optional] 
**Variants** | [**List&lt;VariantDto&gt;**](VariantDto.md) | Gets or sets the variants of the specific metadata tree. | [optional] 
**WasInvalidatedBy** | **string** | Information if the specific metadata tree was invalidated by something. | [optional] 
**SimilarityToLastVersion** | **double?** | The similarity to the last version. | [optional] 
**MetadataExtractorVersion** | **string** | Gets or initializes the version of the metadata extractor. | [optional] 
**HashParameters** | [**HashParametersDto**](HashParametersDto.md) |  | [optional] 
**Id** | **string** | Gets or sets the id of the specific metadata tree. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

