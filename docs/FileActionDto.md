# Coscine.ApiClient.Core.Model.FileActionDto
Represents a Data Transfer Object (DTO) for file actions, including the URL and HTTP method.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Url** | **string** | Gets or sets the presigned URL associated with the file action. | [optional] 
**Method** | **FileActionHttpMethod** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

