# Coscine.ApiClient.Core.Model.UserDto
Represents a Data Transfer Object (DTO) for user-related information.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | The unique identifier of the user. | 
**DisplayName** | **string** | The display name of the user. | 
**GivenName** | **string** | The given name of the user. | 
**FamilyName** | **string** | The family name of the user. | 
**Emails** | [**List&lt;UserEmailDto&gt;**](UserEmailDto.md) | The email addresses associated with the user. | 
**Title** | [**TitleDto**](TitleDto.md) |  | [optional] 
**Language** | [**LanguageDto**](LanguageDto.md) |  | 
**AreToSAccepted** | **bool** | Indicates if the terms of service are accepted by the user. | 
**LatestActivity** | **DateTime?** | The date and time of the latest activity of the user. | [optional] 
**Disciplines** | [**List&lt;DisciplineDto&gt;**](DisciplineDto.md) | The disciplines associated with the user. | 
**Organizations** | [**List&lt;UserOrganizationDto&gt;**](UserOrganizationDto.md) | The organizations associated with the user. | 
**Identities** | [**List&lt;IdentityProviderDto&gt;**](IdentityProviderDto.md) | The identity providers associated with the user. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

