# Coscine.ApiClient.Core.Model.VocabularyInstanceDtoResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | [**VocabularyInstanceDto**](VocabularyInstanceDto.md) |  | [optional] 
**IsSuccess** | **bool** |  | [optional] [readonly] 
**StatusCode** | **int?** |  | [optional] 
**TraceId** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

