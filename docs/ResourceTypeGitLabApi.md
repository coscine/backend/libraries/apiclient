# Coscine.ApiClient.Core.Api.ResourceTypeGitLabApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2ResourceTypesGitlabOptions**](ResourceTypeGitLabApi.md#apiv2resourcetypesgitlaboptions) | **OPTIONS** /api/v2/resource-types/gitlab | Responds with the HTTP methods allowed for the endpoint. |
| [**GetAllGitlabBranchesForProject**](ResourceTypeGitLabApi.md#getallgitlabbranchesforproject) | **GET** /api/v2/resource-types/gitlab/projects/{gitlabProjectId}/branches | Retrieves all branches of a GitLab project, that the user is a member of, based on the provided credentials. |
| [**GetAllGitlabProjects**](ResourceTypeGitLabApi.md#getallgitlabprojects) | **GET** /api/v2/resource-types/gitlab/projects | Retrieves all GitLab projects, that the user is a member of, based on the provided credentials. |
| [**GetGitlabProject**](ResourceTypeGitLabApi.md#getgitlabproject) | **GET** /api/v2/resource-types/gitlab/projects/{gitlabProjectId} | Retrieves a single GitLab project, that the user is a member of, based on the provided credentials. |

<a id="apiv2resourcetypesgitlaboptions"></a>
# **ApiV2ResourceTypesGitlabOptions**
> void ApiV2ResourceTypesGitlabOptions ()

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ResourceTypesGitlabOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ResourceTypeGitLabApi(config);

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ResourceTypesGitlabOptions();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ResourceTypeGitLabApi.ApiV2ResourceTypesGitlabOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ResourceTypesGitlabOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ResourceTypesGitlabOptionsWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ResourceTypeGitLabApi.ApiV2ResourceTypesGitlabOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getallgitlabbranchesforproject"></a>
# **GetAllGitlabBranchesForProject**
> GitlabBranchDtoIEnumerableResponse GetAllGitlabBranchesForProject (int gitlabProjectId, string domain, string accessToken)

Retrieves all branches of a GitLab project, that the user is a member of, based on the provided credentials.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetAllGitlabBranchesForProjectExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ResourceTypeGitLabApi(config);
            var gitlabProjectId = 56;  // int | The ID of the GitLab project.
            var domain = https://git.rwth-aachen.de/;  // string | Domain/Host of the GitLab Provider.
            var accessToken = "accessToken_example";  // string | GitLab Project or Group Access Token.

            try
            {
                // Retrieves all branches of a GitLab project, that the user is a member of, based on the provided credentials.
                GitlabBranchDtoIEnumerableResponse result = apiInstance.GetAllGitlabBranchesForProject(gitlabProjectId, domain, accessToken);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ResourceTypeGitLabApi.GetAllGitlabBranchesForProject: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetAllGitlabBranchesForProjectWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all branches of a GitLab project, that the user is a member of, based on the provided credentials.
    ApiResponse<GitlabBranchDtoIEnumerableResponse> response = apiInstance.GetAllGitlabBranchesForProjectWithHttpInfo(gitlabProjectId, domain, accessToken);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ResourceTypeGitLabApi.GetAllGitlabBranchesForProjectWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **gitlabProjectId** | **int** | The ID of the GitLab project. |  |
| **domain** | **string** | Domain/Host of the GitLab Provider. |  |
| **accessToken** | **string** | GitLab Project or Group Access Token. |  |

### Return type

[**GitlabBranchDtoIEnumerableResponse**](GitlabBranchDtoIEnumerableResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the GitLab project. |  -  |
| **403** | GitLab access token is invalid. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getallgitlabprojects"></a>
# **GetAllGitlabProjects**
> GitlabProjectDtoIEnumerableResponse GetAllGitlabProjects (string domain, string accessToken)

Retrieves all GitLab projects, that the user is a member of, based on the provided credentials.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetAllGitlabProjectsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ResourceTypeGitLabApi(config);
            var domain = https://git.rwth-aachen.de/;  // string | Domain/Host of the GitLab Provider.
            var accessToken = "accessToken_example";  // string | GitLab Project or Group Access Token.

            try
            {
                // Retrieves all GitLab projects, that the user is a member of, based on the provided credentials.
                GitlabProjectDtoIEnumerableResponse result = apiInstance.GetAllGitlabProjects(domain, accessToken);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ResourceTypeGitLabApi.GetAllGitlabProjects: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetAllGitlabProjectsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all GitLab projects, that the user is a member of, based on the provided credentials.
    ApiResponse<GitlabProjectDtoIEnumerableResponse> response = apiInstance.GetAllGitlabProjectsWithHttpInfo(domain, accessToken);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ResourceTypeGitLabApi.GetAllGitlabProjectsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **domain** | **string** | Domain/Host of the GitLab Provider. |  |
| **accessToken** | **string** | GitLab Project or Group Access Token. |  |

### Return type

[**GitlabProjectDtoIEnumerableResponse**](GitlabProjectDtoIEnumerableResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the GitLab projects. |  -  |
| **403** | GitLab access token is invalid. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getgitlabproject"></a>
# **GetGitlabProject**
> GitlabProjectDtoResponse GetGitlabProject (int gitlabProjectId, string domain, string accessToken)

Retrieves a single GitLab project, that the user is a member of, based on the provided credentials.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetGitlabProjectExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ResourceTypeGitLabApi(config);
            var gitlabProjectId = 56;  // int | The ID of the GitLab project.
            var domain = https://git.rwth-aachen.de/;  // string | Domain/Host of the GitLab Provider.
            var accessToken = "accessToken_example";  // string | GitLab Project or Group Access Token.

            try
            {
                // Retrieves a single GitLab project, that the user is a member of, based on the provided credentials.
                GitlabProjectDtoResponse result = apiInstance.GetGitlabProject(gitlabProjectId, domain, accessToken);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ResourceTypeGitLabApi.GetGitlabProject: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetGitlabProjectWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves a single GitLab project, that the user is a member of, based on the provided credentials.
    ApiResponse<GitlabProjectDtoResponse> response = apiInstance.GetGitlabProjectWithHttpInfo(gitlabProjectId, domain, accessToken);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ResourceTypeGitLabApi.GetGitlabProjectWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **gitlabProjectId** | **int** | The ID of the GitLab project. |  |
| **domain** | **string** | Domain/Host of the GitLab Provider. |  |
| **accessToken** | **string** | GitLab Project or Group Access Token. |  |

### Return type

[**GitlabProjectDtoResponse**](GitlabProjectDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the GitLab project. |  -  |
| **403** | GitLab access token is invalid. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

