# Coscine.ApiClient.Core.Model.ExtractedMetadataTreeForUpdateDto
Data transfer object (DTO) representing the update of a metadata tree.  Inherits from the base class Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.MetadataTreeForManipulationDto.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Path** | **string** | Gets or initializes the path of the metadata tree. | 
**Id** | **string** | Gets or sets the id of the to update metadata tree. | 
**Definition** | [**RdfDefinitionForManipulationDto**](RdfDefinitionForManipulationDto.md) |  | 
**Provenance** | [**ProvenanceParametersDto**](ProvenanceParametersDto.md) |  | 
**ForceNewMetadataVersion** | **bool** | Gets or initializes a flag for creating a new metadata version. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

