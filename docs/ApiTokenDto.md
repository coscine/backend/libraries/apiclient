# Coscine.ApiClient.Core.Model.ApiTokenDto
Represents an API token used for authentication.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | The unique identifier of the API token. | [optional] 
**Name** | **string** | The name associated with the API token. | [optional] 
**CreationDate** | **DateTime** | The date when the API token was created. | [optional] 
**ExpiryDate** | **DateTime** | The expiry date of the API token. | [optional] 
**Token** | **string** | The actual token used for authentication. | [optional] 
**Owner** | [**UserMinimalDto**](UserMinimalDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

