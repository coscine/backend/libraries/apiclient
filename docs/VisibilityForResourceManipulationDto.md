# Coscine.ApiClient.Core.Model.VisibilityForResourceManipulationDto
Represents the data transfer object (DTO) for manipulating the visibility of a resource.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | The unique identifier of the visibility setting. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

