# Coscine.ApiClient.Core.Model.RoleMinimalDto
Represents a Data Transfer Object (DTO) containing minimal information about a role.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | The unique identifier of the role. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

