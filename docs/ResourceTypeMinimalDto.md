# Coscine.ApiClient.Core.Model.ResourceTypeMinimalDto
Represents a minimal Data Transfer Object (DTO) for a resource type.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | The unique identifier of the resource type. | 
**SpecificType** | **string** | The specific type of the resource. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

