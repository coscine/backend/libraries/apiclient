# Coscine.ApiClient.Core.Model.SearchCategory
Represents a search category with a name and count of occurrences.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | The name of the search category. | [optional] 
**Count** | **long** | The count of occurrences for the search category. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

