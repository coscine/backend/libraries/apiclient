# Coscine.ApiClient.Core.Model.FileActionHttpMethod
Represents the possible HTTP methods associated with file actions.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

