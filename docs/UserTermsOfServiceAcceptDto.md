# Coscine.ApiClient.Core.Model.UserTermsOfServiceAcceptDto
Represents the data transfer object (DTO) for accepting the terms of service by a user.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VarVersion** | **string** | The version of the terms of service being accepted. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

