# Coscine.ApiClient.Core.Model.ProvenanceDto
Represents a data transfer object (DTO) for provenance information.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | Gets or sets the id of the current metadata graph. | 
**GeneratedAt** | **DateTime?** | Gets or sets the date and time when the metadata was generated. | [optional] 
**WasRevisionOf** | **List&lt;string&gt;** | Gets or sets the adapted versions from the specific metadata tree. | 
**Variants** | [**List&lt;VariantDto&gt;**](VariantDto.md) | Gets or sets the variants of the specific metadata tree. | 
**SimilarityToLastVersion** | **double?** | The similarity to the last version. | [optional] 
**WasInvalidatedBy** | **string** | Information if the specific metadata tree was invalidated by something. | [optional] 
**HashParameters** | [**HashParametersDto**](HashParametersDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

