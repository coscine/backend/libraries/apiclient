# Coscine.ApiClient.Core.Api.SelfApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**AcceptCurrentTos**](SelfApi.md#acceptcurrenttos) | **POST** /api/v2/self/tos | Accepts the current Terms Of Service for the current authenticated user. |
| [**ApiV2SelfOptions**](SelfApi.md#apiv2selfoptions) | **OPTIONS** /api/v2/self | Responds with the HTTP methods allowed for the endpoint. |
| [**ConfirmUserEmail**](SelfApi.md#confirmuseremail) | **POST** /api/v2/self/emails | Confirms the email of a user. |
| [**GetCurrentUser**](SelfApi.md#getcurrentuser) | **GET** /api/v2/self | Retrieves the current authenticated user. |
| [**InitiateUserMerge**](SelfApi.md#initiateusermerge) | **POST** /api/v2/self/identities | Initiates user merging for the current user. |
| [**ResolveProjectInvitation**](SelfApi.md#resolveprojectinvitation) | **POST** /api/v2/self/project-invitations | Resolves a project invitation for the authenticated user. |
| [**UpdateCurrentUser**](SelfApi.md#updatecurrentuser) | **PUT** /api/v2/self | Updates the current authenticated user. |

<a id="acceptcurrenttos"></a>
# **AcceptCurrentTos**
> void AcceptCurrentTos (UserTermsOfServiceAcceptDto? userTermsOfServiceAcceptDto = null)

Accepts the current Terms Of Service for the current authenticated user.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class AcceptCurrentTosExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SelfApi(config);
            var userTermsOfServiceAcceptDto = new UserTermsOfServiceAcceptDto?(); // UserTermsOfServiceAcceptDto? |  (optional) 

            try
            {
                // Accepts the current Terms Of Service for the current authenticated user.
                apiInstance.AcceptCurrentTos(userTermsOfServiceAcceptDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SelfApi.AcceptCurrentTos: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the AcceptCurrentTosWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Accepts the current Terms Of Service for the current authenticated user.
    apiInstance.AcceptCurrentTosWithHttpInfo(userTermsOfServiceAcceptDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SelfApi.AcceptCurrentTosWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **userTermsOfServiceAcceptDto** | [**UserTermsOfServiceAcceptDto?**](UserTermsOfServiceAcceptDto?.md) |  | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Terms of Service accepted. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="apiv2selfoptions"></a>
# **ApiV2SelfOptions**
> void ApiV2SelfOptions ()

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2SelfOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SelfApi(config);

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2SelfOptions();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SelfApi.ApiV2SelfOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2SelfOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2SelfOptionsWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SelfApi.ApiV2SelfOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="confirmuseremail"></a>
# **ConfirmUserEmail**
> void ConfirmUserEmail (Guid confirmationToken)

Confirms the email of a user.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ConfirmUserEmailExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SelfApi(config);
            var confirmationToken = "confirmationToken_example";  // Guid | Gets or initializes the confirmation token for user email confirmation.

            try
            {
                // Confirms the email of a user.
                apiInstance.ConfirmUserEmail(confirmationToken);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SelfApi.ConfirmUserEmail: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ConfirmUserEmailWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Confirms the email of a user.
    apiInstance.ConfirmUserEmailWithHttpInfo(confirmationToken);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SelfApi.ConfirmUserEmailWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **confirmationToken** | **Guid** | Gets or initializes the confirmation token for user email confirmation. |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Contact email confirmed. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |
| **400** | Provided input has a bad format. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getcurrentuser"></a>
# **GetCurrentUser**
> UserDtoResponse GetCurrentUser ()

Retrieves the current authenticated user.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetCurrentUserExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SelfApi(config);

            try
            {
                // Retrieves the current authenticated user.
                UserDtoResponse result = apiInstance.GetCurrentUser();
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SelfApi.GetCurrentUser: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetCurrentUserWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the current authenticated user.
    ApiResponse<UserDtoResponse> response = apiInstance.GetCurrentUserWithHttpInfo();
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SelfApi.GetCurrentUserWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

[**UserDtoResponse**](UserDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the current authenticated user. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="initiateusermerge"></a>
# **InitiateUserMerge**
> UserMergeDtoResponse InitiateUserMerge (IdentityProviders identityProvider)

Initiates user merging for the current user.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class InitiateUserMergeExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SelfApi(config);
            var identityProvider = (IdentityProviders) "Shibboleth";  // IdentityProviders | The identity provider from which user data is to be merged.

            try
            {
                // Initiates user merging for the current user.
                UserMergeDtoResponse result = apiInstance.InitiateUserMerge(identityProvider);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SelfApi.InitiateUserMerge: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the InitiateUserMergeWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Initiates user merging for the current user.
    ApiResponse<UserMergeDtoResponse> response = apiInstance.InitiateUserMergeWithHttpInfo(identityProvider);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SelfApi.InitiateUserMergeWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **identityProvider** | **IdentityProviders** | The identity provider from which user data is to be merged. |  |

### Return type

[**UserMergeDtoResponse**](UserMergeDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the user merge dto. |  -  |
| **400** | Provided input has a bad format. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="resolveprojectinvitation"></a>
# **ResolveProjectInvitation**
> void ResolveProjectInvitation (ProjectInvitationResolveDto? projectInvitationResolveDto = null)

Resolves a project invitation for the authenticated user.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ResolveProjectInvitationExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SelfApi(config);
            var projectInvitationResolveDto = new ProjectInvitationResolveDto?(); // ProjectInvitationResolveDto? | The project invitation resolve data. (optional) 

            try
            {
                // Resolves a project invitation for the authenticated user.
                apiInstance.ResolveProjectInvitation(projectInvitationResolveDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SelfApi.ResolveProjectInvitation: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ResolveProjectInvitationWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Resolves a project invitation for the authenticated user.
    apiInstance.ResolveProjectInvitationWithHttpInfo(projectInvitationResolveDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SelfApi.ResolveProjectInvitationWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectInvitationResolveDto** | [**ProjectInvitationResolveDto?**](ProjectInvitationResolveDto?.md) | The project invitation resolve data. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="updatecurrentuser"></a>
# **UpdateCurrentUser**
> void UpdateCurrentUser (UserForUpdateDto? userForUpdateDto = null)

Updates the current authenticated user.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class UpdateCurrentUserExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SelfApi(config);
            var userForUpdateDto = new UserForUpdateDto?(); // UserForUpdateDto? | The updated user data. (optional) 

            try
            {
                // Updates the current authenticated user.
                apiInstance.UpdateCurrentUser(userForUpdateDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SelfApi.UpdateCurrentUser: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the UpdateCurrentUserWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Updates the current authenticated user.
    apiInstance.UpdateCurrentUserWithHttpInfo(userForUpdateDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SelfApi.UpdateCurrentUserWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **userForUpdateDto** | [**UserForUpdateDto?**](UserForUpdateDto?.md) | The updated user data. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | User updated. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

