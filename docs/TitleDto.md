# Coscine.ApiClient.Core.Model.TitleDto
Represents the Data Transfer Object (DTO) for title information.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | The identifier for the title. | 
**DisplayName** | **string** | The displayed name of the title. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

