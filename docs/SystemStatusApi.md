# Coscine.ApiClient.Core.Api.SystemStatusApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2SystemStatusOptions**](SystemStatusApi.md#apiv2systemstatusoptions) | **OPTIONS** /api/v2/system/status | Responds with the HTTP methods allowed for the endpoint. |
| [**GetInternalMessages**](SystemStatusApi.md#getinternalmessages) | **GET** /api/v2/system/status/internal | Retrieves the internal messages. |
| [**GetNocMessages**](SystemStatusApi.md#getnocmessages) | **GET** /api/v2/system/status/noc | Retrieves the NOC messages. |

<a id="apiv2systemstatusoptions"></a>
# **ApiV2SystemStatusOptions**
> void ApiV2SystemStatusOptions ()

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2SystemStatusOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SystemStatusApi(config);

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2SystemStatusOptions();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SystemStatusApi.ApiV2SystemStatusOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2SystemStatusOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2SystemStatusOptionsWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SystemStatusApi.ApiV2SystemStatusOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getinternalmessages"></a>
# **GetInternalMessages**
> MessageDtoPagedResponse GetInternalMessages (DateTime? startDateAfter = null, DateTime? startDateBefore = null, DateTime? endDateAfter = null, DateTime? endDateBefore = null, MessageType? type = null, string? searchTerm = null, int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Retrieves the internal messages.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetInternalMessagesExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SystemStatusApi(config);
            var startDateAfter = DateTime.Parse("2013-10-20T19:20:30+01:00");  // DateTime? | Will only return messages that have their start date after this date (UTC). (optional) 
            var startDateBefore = DateTime.Parse("2013-10-20T19:20:30+01:00");  // DateTime? | Will only return messages that have their start date before this date (UTC). (optional) 
            var endDateAfter = DateTime.Parse("2013-10-20T19:20:30+01:00");  // DateTime? | Will only return messages that have their end date after this date (UTC). (optional) 
            var endDateBefore = DateTime.Parse("2013-10-20T19:20:30+01:00");  // DateTime? | Will only return messages that have their end date before this date (UTC). (optional) 
            var type = new MessageType?(); // MessageType? | Filter messages by their type  (e.g. information, warning, error). (optional) 
            var searchTerm = "searchTerm_example";  // string? | Filter messages by text contained in the messages or title. (optional) 
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves the internal messages.
                MessageDtoPagedResponse result = apiInstance.GetInternalMessages(startDateAfter, startDateBefore, endDateAfter, endDateBefore, type, searchTerm, pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SystemStatusApi.GetInternalMessages: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetInternalMessagesWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the internal messages.
    ApiResponse<MessageDtoPagedResponse> response = apiInstance.GetInternalMessagesWithHttpInfo(startDateAfter, startDateBefore, endDateAfter, endDateBefore, type, searchTerm, pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SystemStatusApi.GetInternalMessagesWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **startDateAfter** | **DateTime?** | Will only return messages that have their start date after this date (UTC). | [optional]  |
| **startDateBefore** | **DateTime?** | Will only return messages that have their start date before this date (UTC). | [optional]  |
| **endDateAfter** | **DateTime?** | Will only return messages that have their end date after this date (UTC). | [optional]  |
| **endDateBefore** | **DateTime?** | Will only return messages that have their end date before this date (UTC). | [optional]  |
| **type** | [**MessageType?**](MessageType?.md) | Filter messages by their type  (e.g. information, warning, error). | [optional]  |
| **searchTerm** | **string?** | Filter messages by text contained in the messages or title. | [optional]  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**MessageDtoPagedResponse**](MessageDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the internal message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getnocmessages"></a>
# **GetNocMessages**
> MessageDtoPagedResponse GetNocMessages (DateTime? startDateAfter = null, DateTime? startDateBefore = null, DateTime? endDateAfter = null, DateTime? endDateBefore = null, MessageType? type = null, string? searchTerm = null, int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Retrieves the NOC messages.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetNocMessagesExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SystemStatusApi(config);
            var startDateAfter = DateTime.Parse("2013-10-20T19:20:30+01:00");  // DateTime? | Will only return messages that have their start date after this date (UTC). (optional) 
            var startDateBefore = DateTime.Parse("2013-10-20T19:20:30+01:00");  // DateTime? | Will only return messages that have their start date before this date (UTC). (optional) 
            var endDateAfter = DateTime.Parse("2013-10-20T19:20:30+01:00");  // DateTime? | Will only return messages that have their end date after this date (UTC). (optional) 
            var endDateBefore = DateTime.Parse("2013-10-20T19:20:30+01:00");  // DateTime? | Will only return messages that have their end date before this date (UTC). (optional) 
            var type = new MessageType?(); // MessageType? | Filter messages by their type  (e.g. information, warning, error). (optional) 
            var searchTerm = "searchTerm_example";  // string? | Filter messages by text contained in the messages or title. (optional) 
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves the NOC messages.
                MessageDtoPagedResponse result = apiInstance.GetNocMessages(startDateAfter, startDateBefore, endDateAfter, endDateBefore, type, searchTerm, pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SystemStatusApi.GetNocMessages: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetNocMessagesWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the NOC messages.
    ApiResponse<MessageDtoPagedResponse> response = apiInstance.GetNocMessagesWithHttpInfo(startDateAfter, startDateBefore, endDateAfter, endDateBefore, type, searchTerm, pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SystemStatusApi.GetNocMessagesWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **startDateAfter** | **DateTime?** | Will only return messages that have their start date after this date (UTC). | [optional]  |
| **startDateBefore** | **DateTime?** | Will only return messages that have their start date before this date (UTC). | [optional]  |
| **endDateAfter** | **DateTime?** | Will only return messages that have their end date after this date (UTC). | [optional]  |
| **endDateBefore** | **DateTime?** | Will only return messages that have their end date before this date (UTC). | [optional]  |
| **type** | [**MessageType?**](MessageType?.md) | Filter messages by their type  (e.g. information, warning, error). | [optional]  |
| **searchTerm** | **string?** | Filter messages by text contained in the messages or title. | [optional]  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**MessageDtoPagedResponse**](MessageDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the NOC messages. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

