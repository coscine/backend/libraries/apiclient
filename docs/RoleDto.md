# Coscine.ApiClient.Core.Model.RoleDto
Represents a Data Transfer Object (DTO) for a role.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | The unique identifier of the role. | 
**DisplayName** | **string** | The display name of the role. | 
**Description** | **string** | The description of the role. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

