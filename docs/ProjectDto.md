# Coscine.ApiClient.Core.Model.ProjectDto
Represents a data transfer object (DTO) for project information.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Unique identifier for the project. | 
**Pid** | **string** | Persistent identifier for the project. | 
**Name** | **string** | Name of the project. | 
**Description** | **string** | Description of the project. | 
**StartDate** | **DateTime** | Start date of the project. | 
**EndDate** | **DateTime** | End date of the project. | 
**Keywords** | **List&lt;string&gt;** | Collection of keywords associated with the project. | [optional] 
**DisplayName** | **string** | Display name of the project. | [optional] 
**PrincipleInvestigators** | **string** | Principal investigators involved in the project. | [optional] 
**GrantId** | **string** | Grant ID associated with the project. | [optional] 
**Visibility** | [**VisibilityDto**](VisibilityDto.md) |  | 
**Disciplines** | [**List&lt;DisciplineDto&gt;**](DisciplineDto.md) | Disciplines related to the project. | 
**Organizations** | [**List&lt;ProjectOrganizationDto&gt;**](ProjectOrganizationDto.md) | Organizations associated with the project. | 
**Slug** | **string** | Slug for the project. | 
**Creator** | [**UserMinimalDto**](UserMinimalDto.md) |  | [optional] 
**CreationDate** | **DateTime?** | Date of creation of the project. | [optional] 
**SubProjects** | [**List&lt;ProjectDto&gt;**](ProjectDto.md) | Collection of sub-projects associated with this project. | [optional] 
**Parent** | [**ProjectMinimalDto**](ProjectMinimalDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

