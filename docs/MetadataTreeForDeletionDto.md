# Coscine.ApiClient.Core.Model.MetadataTreeForDeletionDto
Data transfer object (DTO) for deleting a specific version of metadata.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Path** | **string** | Gets or initializes the path of the metadata tree. | 
**VarVersion** | **long** | Gets or initializes the version of the metadata tree to be deleted. | 
**InvalidatedBy** | **string** | Gets or initializes the invalidation entity. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

