# Coscine.ApiClient.Core.Model.UserEmailDto
Represents a Data Transfer Object (DTO) for user email information.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Email** | **string** | The email address of the user. | [optional] 
**IsConfirmed** | **bool** | Indicates whether the email address is confirmed. | [optional] 
**IsPrimary** | **bool** | Indicates whether the email address is the primary one for the user. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

