# Coscine.ApiClient.Core.Model.RdfDefinitionDto
Represents a Data Transfer Object (DTO) for RDF (Resource Description Framework) definition details.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Content** | **string** | The content of the RDF definition. | [optional] 
**Type** | **string** | The type of the RDF definition. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

