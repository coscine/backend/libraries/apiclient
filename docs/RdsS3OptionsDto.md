# Coscine.ApiClient.Core.Model.RdsS3OptionsDto
Represents the data transfer object (DTO) for RDS S3 options.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BucketName** | **string** | The name of the bucket associated with RDS S3. | 
**AccessKeyRead** | **string** | The access key for reading from the RDS S3 bucket. | 
**SecretKeyRead** | **string** | The secret key for reading from the RDS S3 bucket. | 
**AccessKeyWrite** | **string** | The access key for writing to the RDS S3 bucket. | 
**SecretKeyWrite** | **string** | The secret key for writing to the RDS S3 bucket. | 
**Endpoint** | **string** | The endpoint for the RDS S3 bucket. | 
**Size** | [**QuotaDto**](QuotaDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

