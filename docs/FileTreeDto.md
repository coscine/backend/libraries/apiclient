# Coscine.ApiClient.Core.Model.FileTreeDto
Represents a Data Transfer Object (DTO) for a file within a tree structure, extending the base TreeDto.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Path** | **string** | The path of the tree item. | [optional] 
**Type** | **TreeDataType** |  | [optional] 
**Directory** | **string** | Gets or sets the directory of the file. | [optional] 
**Name** | **string** | Gets or sets the name of the file. | [optional] 
**Extension** | **string** | Gets or sets the extension of the file. | [optional] 
**Size** | **long** | Gets or sets the size of the file in bytes. | [optional] 
**CreationDate** | **DateTime?** | Gets or sets the creation date of the file. | [optional] 
**ChangeDate** | **DateTime?** | Gets or sets the last change date of the file. | [optional] 
**Actions** | [**FileActionsDto**](FileActionsDto.md) |  | [optional] 
**Hidden** | **bool** | Determines if the specific tree entry is hidden. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

