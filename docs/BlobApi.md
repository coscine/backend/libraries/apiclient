# Coscine.ApiClient.Core.Api.BlobApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2ProjectsProjectIdResourcesResourceIdBlobsOptions**](BlobApi.md#apiv2projectsprojectidresourcesresourceidblobsoptions) | **OPTIONS** /api/v2/projects/{projectId}/resources/{resourceId}/blobs | Responds with the HTTP methods allowed for the endpoint. |
| [**CreateBlob**](BlobApi.md#createblob) | **POST** /api/v2/projects/{projectId}/resources/{resourceId}/blobs/{key} | Creates a new blob for a resource. |
| [**DeleteBlob**](BlobApi.md#deleteblob) | **DELETE** /api/v2/projects/{projectId}/resources/{resourceId}/blobs/{key} | Deletes a blob from a resource. |
| [**GetBlob**](BlobApi.md#getblob) | **GET** /api/v2/projects/{projectId}/resources/{resourceId}/blobs/{key} | Download a blob from a resource. |
| [**UpdateBlob**](BlobApi.md#updateblob) | **PUT** /api/v2/projects/{projectId}/resources/{resourceId}/blobs/{key} | Updates an existing blob of a resource. |

<a id="apiv2projectsprojectidresourcesresourceidblobsoptions"></a>
# **ApiV2ProjectsProjectIdResourcesResourceIdBlobsOptions**
> void ApiV2ProjectsProjectIdResourcesResourceIdBlobsOptions (string projectId, string resourceId)

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ProjectsProjectIdResourcesResourceIdBlobsOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new BlobApi(config);
            var projectId = "projectId_example";  // string | 
            var resourceId = "resourceId_example";  // string | 

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ProjectsProjectIdResourcesResourceIdBlobsOptions(projectId, resourceId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling BlobApi.ApiV2ProjectsProjectIdResourcesResourceIdBlobsOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ProjectsProjectIdResourcesResourceIdBlobsOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ProjectsProjectIdResourcesResourceIdBlobsOptionsWithHttpInfo(projectId, resourceId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling BlobApi.ApiV2ProjectsProjectIdResourcesResourceIdBlobsOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** |  |  |
| **resourceId** | **string** |  |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="createblob"></a>
# **CreateBlob**
> void CreateBlob (string projectId, Guid resourceId, string key, System.IO.Stream? file = null)

Creates a new blob for a resource.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class CreateBlobExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new BlobApi(config);
            var projectId = "projectId_example";  // string | The identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The identifier of the resource.
            var key = "key_example";  // string | The key for the new blob.
            var file = new System.IO.MemoryStream(System.IO.File.ReadAllBytes("/path/to/file.txt"));  // System.IO.Stream? |  (optional) 

            try
            {
                // Creates a new blob for a resource.
                apiInstance.CreateBlob(projectId, resourceId, key, file);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling BlobApi.CreateBlob: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the CreateBlobWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Creates a new blob for a resource.
    apiInstance.CreateBlobWithHttpInfo(projectId, resourceId, key, file);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling BlobApi.CreateBlobWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The identifier or slug of the project. |  |
| **resourceId** | **Guid** | The identifier of the resource. |  |
| **key** | **string** | The key for the new blob. |  |
| **file** | **System.IO.Stream?****System.IO.Stream?** |  | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Blob created. |  -  |
| **400** | Blob already exists. |  -  |
| **415** | Blob is not of the type \&quot;multipart/form-data\&quot; |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="deleteblob"></a>
# **DeleteBlob**
> void DeleteBlob (string projectId, Guid resourceId, string key)

Deletes a blob from a resource.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class DeleteBlobExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new BlobApi(config);
            var projectId = "projectId_example";  // string | The identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The identifier of the resource.
            var key = "key_example";  // string | The key associated with the blob to delete.

            try
            {
                // Deletes a blob from a resource.
                apiInstance.DeleteBlob(projectId, resourceId, key);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling BlobApi.DeleteBlob: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the DeleteBlobWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Deletes a blob from a resource.
    apiInstance.DeleteBlobWithHttpInfo(projectId, resourceId, key);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling BlobApi.DeleteBlobWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The identifier or slug of the project. |  |
| **resourceId** | **Guid** | The identifier of the resource. |  |
| **key** | **string** | The key associated with the blob to delete. |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Blob deleted. |  -  |
| **404** | Blob does not exist. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getblob"></a>
# **GetBlob**
> System.IO.Stream GetBlob (string projectId, Guid resourceId, string key)

Download a blob from a resource.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetBlobExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new BlobApi(config);
            var projectId = "projectId_example";  // string | The identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The identifier of the resource.
            var key = "key_example";  // string | The key associated with the blob.

            try
            {
                // Download a blob from a resource.
                System.IO.Stream result = apiInstance.GetBlob(projectId, resourceId, key);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling BlobApi.GetBlob: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetBlobWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Download a blob from a resource.
    ApiResponse<System.IO.Stream> response = apiInstance.GetBlobWithHttpInfo(projectId, resourceId, key);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling BlobApi.GetBlobWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The identifier or slug of the project. |  |
| **resourceId** | **Guid** | The identifier of the resource. |  |
| **key** | **string** | The key associated with the blob. |  |

### Return type

**System.IO.Stream**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the blob. |  -  |
| **404** | Blob does not exist. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="updateblob"></a>
# **UpdateBlob**
> void UpdateBlob (string projectId, Guid resourceId, string key, System.IO.Stream? file = null)

Updates an existing blob of a resource.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class UpdateBlobExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new BlobApi(config);
            var projectId = "projectId_example";  // string | The identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The identifier of the resource.
            var key = "key_example";  // string | The key associated with the blob to update.
            var file = new System.IO.MemoryStream(System.IO.File.ReadAllBytes("/path/to/file.txt"));  // System.IO.Stream? |  (optional) 

            try
            {
                // Updates an existing blob of a resource.
                apiInstance.UpdateBlob(projectId, resourceId, key, file);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling BlobApi.UpdateBlob: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the UpdateBlobWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Updates an existing blob of a resource.
    apiInstance.UpdateBlobWithHttpInfo(projectId, resourceId, key, file);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling BlobApi.UpdateBlobWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The identifier or slug of the project. |  |
| **resourceId** | **Guid** | The identifier of the resource. |  |
| **key** | **string** | The key associated with the blob to update. |  |
| **file** | **System.IO.Stream?****System.IO.Stream?** |  | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Blob updated. |  -  |
| **400** | Blob already exists. |  -  |
| **415** | Blob is not of the type \&quot;multipart/form-data\&quot; |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

