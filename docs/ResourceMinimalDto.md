# Coscine.ApiClient.Core.Model.ResourceMinimalDto
Represents a minimal Data Transfer Object (DTO) for a resource, containing essential identifiers.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Unique identifier for the resource. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

