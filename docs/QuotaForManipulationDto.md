# Coscine.ApiClient.Core.Model.QuotaForManipulationDto
Represents a data transfer object (DTO) used for manipulating quota values.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Value** | **long** | The numerical value of the quota. Must be a positive System.Int64. | 
**Unit** | **QuotaUnit** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

