# Coscine.ApiClient.Core.Model.ResourceTypeOptionsForUpdateDto
Represents the data transfer object (DTO) used for updating options related to any resource type.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LinkedResourceTypeOptions** | **Object** | Represents the data transfer object (DTO) for manipulating linked data resource type options. | [optional] 
**GitlabResourceTypeOptions** | [**GitlabResourceTypeOptionsForUpdateDto**](GitlabResourceTypeOptionsForUpdateDto.md) |  | [optional] 
**RdsResourceTypeOptions** | [**RdsResourceTypeOptionsForManipulationDto**](RdsResourceTypeOptionsForManipulationDto.md) |  | [optional] 
**RdsS3ResourceTypeOptions** | [**RdsS3ResourceTypeOptionsForManipulationDto**](RdsS3ResourceTypeOptionsForManipulationDto.md) |  | [optional] 
**RdsS3WormResourceTypeOptions** | [**RdsS3WormResourceTypeOptionsForManipulationDto**](RdsS3WormResourceTypeOptionsForManipulationDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

