# Coscine.ApiClient.Core.Model.ResourceTypeDto
Represents a resource type.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | The unique identifier of the resource type. | [optional] 
**GeneralType** | **string** | The general type of the resource. | 
**SpecificType** | **string** | The specific type of the resource. | 
**Options** | [**ResourceTypeOptionsDto**](ResourceTypeOptionsDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

