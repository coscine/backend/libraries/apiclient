# Coscine.ApiClient.Core.Model.FileSystemStorageOptionsDto
Represents the data transfer object (DTO) for FileSystemStorage options.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Directory** | **string** | The directory where the files are stored. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

