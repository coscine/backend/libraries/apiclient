# Coscine.ApiClient.Core.Model.PublicationAdvisoryServiceDto
Data transfer object (DTO) representing the publication advisory service of an organization.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | The data publication service&#39;s display name. | 
**Email** | **string** | The data publication service&#39;s email address. | 
**Description** | **string** | The data publication service&#39;s description. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

