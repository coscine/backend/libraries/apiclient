# Coscine.ApiClient.Core.Model.HashParametersDto
Data transfer object (DTO) representing Hash Parameters in a request.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AlgorithmName** | **string** | Gets or initializes the hash algorithm name. | 
**Value** | **string** | Gets or initializes the hash value. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

