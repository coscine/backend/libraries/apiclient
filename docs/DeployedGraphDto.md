# Coscine.ApiClient.Core.Model.DeployedGraphDto
Represents a Data Transfer Object (DTO) for deployed graph details.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Uri** | **string** | Graph URI and identifier. | [optional] 
**FileHashes** | **List&lt;string&gt;** | Collection of file hashes associated with the graph. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

