# Coscine.ApiClient.Core.Model.ResourceDto
Represents a Data Transfer Object (DTO) for resource details.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Unique identifier for the resource. | [optional] 
**Pid** | **string** | Persistent identifier for the resource. | 
**Type** | [**ResourceTypeDto**](ResourceTypeDto.md) |  | 
**Name** | **string** | Name of the resource. | 
**DisplayName** | **string** | Display name of the resource. | 
**Description** | **string** | Description of the resource. | 
**Keywords** | **List&lt;string&gt;** | Keywords associated with the resource. | [optional] 
**License** | [**LicenseDto**](LicenseDto.md) |  | [optional] 
**UsageRights** | **string** | Usage rights associated with the resource. | [optional] 
**MetadataLocalCopy** | **bool** | Indicates whether a local copy of the metadata is available for the resource. | [optional] 
**MetadataExtraction** | **bool** | Indicates whether metadata extraction is enabled for the resource. | [optional] 
**ApplicationProfile** | [**ApplicationProfileMinimalDto**](ApplicationProfileMinimalDto.md) |  | 
**FixedValues** | **Dictionary&lt;string, Dictionary&lt;string, List&lt;FixedValueForResourceManipulationDto&gt;&gt;&gt;** | Fixed values associated with resource manipulation.  This dictionary may contain multiple levels and is structured as follows:  Dictionary (Key: string) -&gt; Dictionary (Key: string) -&gt; List of FixedValueForResourceManipulationDto. | 
**Disciplines** | [**List&lt;DisciplineDto&gt;**](DisciplineDto.md) | Disciplines associated with the resource. | 
**Visibility** | [**VisibilityDto**](VisibilityDto.md) |  | 
**DateCreated** | **DateTime?** | Date when the resource was created. | [optional] 
**Creator** | [**UserMinimalDto**](UserMinimalDto.md) |  | [optional] 
**Archived** | **bool** | Indicates whether the resource is archived. | [optional] 
**MaintenanceMode** | **bool** | Indicates whether the resource is in maintenance mode. | [optional] 
**Projects** | [**List&lt;ProjectMinimalDto&gt;**](ProjectMinimalDto.md) | The projects associated with the resource. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

