# Coscine.ApiClient.Core.Model.TermsOfServiceDto
Represents the Data Transfer Object (DTO) for terms of service information.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VarVersion** | **string** | The version of the terms of service. | 
**Href** | **string** | The URI point to the content of ToS&#39; version | 
**IsCurrent** | **bool** | Indicates whether these terms of service are current or not. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

