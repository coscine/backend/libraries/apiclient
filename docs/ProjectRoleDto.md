# Coscine.ApiClient.Core.Model.ProjectRoleDto
Represents a data transfer object (DTO) for a project role.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Identifier of the project role. | [optional] 
**Project** | [**ProjectMinimalDto**](ProjectMinimalDto.md) |  | [optional] 
**Role** | [**RoleDto**](RoleDto.md) |  | [optional] 
**User** | [**PublicUserDto**](PublicUserDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

