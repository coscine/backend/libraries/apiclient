# Coscine.ApiClient.Core.Model.VocabularyDto
Represents a Data Transfer Object (DTO) for vocabulary details.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GraphUri** | **string** | The URI of the graph containing the vocabulary. | 
**ClassUri** | **string** | The URI of the top-level parent class in the vocabulary. | 
**DisplayName** | **string** | The display name of the vocabulary. | [optional] 
**Description** | **string** | The description of the vocabulary. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

