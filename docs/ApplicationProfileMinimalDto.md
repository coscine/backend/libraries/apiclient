# Coscine.ApiClient.Core.Model.ApplicationProfileMinimalDto
Represents a minimalistic application profile data transfer object.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Uri** | **string** | The URI associated with the application profile. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

