# Coscine.ApiClient.Core.Api.TreeApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2ProjectsProjectIdResourcesResourceIdTreesOptions**](TreeApi.md#apiv2projectsprojectidresourcesresourceidtreesoptions) | **OPTIONS** /api/v2/projects/{projectId}/resources/{resourceId}/trees | Responds with the HTTP methods allowed for the endpoint. |
| [**CreateExtractedMetadataTree**](TreeApi.md#createextractedmetadatatree) | **POST** /api/v2/projects/{projectId}/resources/{resourceId}/trees/metadata/extracted | Creates a new extracted metadata tree for a resource. |
| [**CreateMetadataTree**](TreeApi.md#createmetadatatree) | **POST** /api/v2/projects/{projectId}/resources/{resourceId}/trees/metadata | Creates a new metadata tree for a resource. |
| [**DeleteMetadataTree**](TreeApi.md#deletemetadatatree) | **DELETE** /api/v2/projects/{projectId}/resources/{resourceId}/trees/metadata | Deletes (invalidates) a metadata tree associated with a resource. |
| [**GetFileTree**](TreeApi.md#getfiletree) | **GET** /api/v2/projects/{projectId}/resources/{resourceId}/trees/files | Retrieves the file tree associated with a resource. |
| [**GetMetadataTree**](TreeApi.md#getmetadatatree) | **GET** /api/v2/projects/{projectId}/resources/{resourceId}/trees/metadata | Retrieves the metadata tree associated with a resource. |
| [**GetSpecificMetadataTree**](TreeApi.md#getspecificmetadatatree) | **GET** /api/v2/projects/{projectId}/resources/{resourceId}/trees/metadata/specific | Retrieves the specific metadata tree associated with a resource. |
| [**UpdateExtractedMetadataTree**](TreeApi.md#updateextractedmetadatatree) | **PUT** /api/v2/projects/{projectId}/resources/{resourceId}/trees/metadata/extracted | Updates an existing metadata tree of a resource. |
| [**UpdateMetadataTree**](TreeApi.md#updatemetadatatree) | **PUT** /api/v2/projects/{projectId}/resources/{resourceId}/trees/metadata | Updates an existing metadata tree of a resource. |

<a id="apiv2projectsprojectidresourcesresourceidtreesoptions"></a>
# **ApiV2ProjectsProjectIdResourcesResourceIdTreesOptions**
> void ApiV2ProjectsProjectIdResourcesResourceIdTreesOptions (string projectId, string resourceId)

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ProjectsProjectIdResourcesResourceIdTreesOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TreeApi(config);
            var projectId = "projectId_example";  // string | 
            var resourceId = "resourceId_example";  // string | 

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ProjectsProjectIdResourcesResourceIdTreesOptions(projectId, resourceId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling TreeApi.ApiV2ProjectsProjectIdResourcesResourceIdTreesOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ProjectsProjectIdResourcesResourceIdTreesOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ProjectsProjectIdResourcesResourceIdTreesOptionsWithHttpInfo(projectId, resourceId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling TreeApi.ApiV2ProjectsProjectIdResourcesResourceIdTreesOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** |  |  |
| **resourceId** | **string** |  |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="createextractedmetadatatree"></a>
# **CreateExtractedMetadataTree**
> MetadataTreeDtoResponse CreateExtractedMetadataTree (string projectId, Guid resourceId, ExtractedMetadataTreeForCreationDto? extractedMetadataTreeForCreationDto = null)

Creates a new extracted metadata tree for a resource.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class CreateExtractedMetadataTreeExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TreeApi(config);
            var projectId = "projectId_example";  // string | The unique identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The unique identifier of the resource.
            var extractedMetadataTreeForCreationDto = new ExtractedMetadataTreeForCreationDto?(); // ExtractedMetadataTreeForCreationDto? | The metadata tree data for creation. (optional) 

            try
            {
                // Creates a new extracted metadata tree for a resource.
                MetadataTreeDtoResponse result = apiInstance.CreateExtractedMetadataTree(projectId, resourceId, extractedMetadataTreeForCreationDto);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling TreeApi.CreateExtractedMetadataTree: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the CreateExtractedMetadataTreeWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Creates a new extracted metadata tree for a resource.
    ApiResponse<MetadataTreeDtoResponse> response = apiInstance.CreateExtractedMetadataTreeWithHttpInfo(projectId, resourceId, extractedMetadataTreeForCreationDto);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling TreeApi.CreateExtractedMetadataTreeWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The unique identifier or slug of the project. |  |
| **resourceId** | **Guid** | The unique identifier of the resource. |  |
| **extractedMetadataTreeForCreationDto** | [**ExtractedMetadataTreeForCreationDto?**](ExtractedMetadataTreeForCreationDto?.md) | The metadata tree data for creation. | [optional]  |

### Return type

[**MetadataTreeDtoResponse**](MetadataTreeDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Extracted Metadata tree created. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="createmetadatatree"></a>
# **CreateMetadataTree**
> MetadataTreeDtoResponse CreateMetadataTree (string projectId, Guid resourceId, MetadataTreeForCreationDto? metadataTreeForCreationDto = null)

Creates a new metadata tree for a resource.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class CreateMetadataTreeExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TreeApi(config);
            var projectId = "projectId_example";  // string | The unique identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The unique identifier of the resource.
            var metadataTreeForCreationDto = new MetadataTreeForCreationDto?(); // MetadataTreeForCreationDto? | The metadata tree data for creation. (optional) 

            try
            {
                // Creates a new metadata tree for a resource.
                MetadataTreeDtoResponse result = apiInstance.CreateMetadataTree(projectId, resourceId, metadataTreeForCreationDto);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling TreeApi.CreateMetadataTree: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the CreateMetadataTreeWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Creates a new metadata tree for a resource.
    ApiResponse<MetadataTreeDtoResponse> response = apiInstance.CreateMetadataTreeWithHttpInfo(projectId, resourceId, metadataTreeForCreationDto);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling TreeApi.CreateMetadataTreeWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The unique identifier or slug of the project. |  |
| **resourceId** | **Guid** | The unique identifier of the resource. |  |
| **metadataTreeForCreationDto** | [**MetadataTreeForCreationDto?**](MetadataTreeForCreationDto?.md) | The metadata tree data for creation. | [optional]  |

### Return type

[**MetadataTreeDtoResponse**](MetadataTreeDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Metadata tree created. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="deletemetadatatree"></a>
# **DeleteMetadataTree**
> void DeleteMetadataTree (string projectId, Guid resourceId, MetadataTreeForDeletionDto? metadataTreeForDeletionDto = null)

Deletes (invalidates) a metadata tree associated with a resource.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class DeleteMetadataTreeExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TreeApi(config);
            var projectId = "projectId_example";  // string | The unique identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The unique identifier of the resource.
            var metadataTreeForDeletionDto = new MetadataTreeForDeletionDto?(); // MetadataTreeForDeletionDto? | The dto for the deletion. (optional) 

            try
            {
                // Deletes (invalidates) a metadata tree associated with a resource.
                apiInstance.DeleteMetadataTree(projectId, resourceId, metadataTreeForDeletionDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling TreeApi.DeleteMetadataTree: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the DeleteMetadataTreeWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Deletes (invalidates) a metadata tree associated with a resource.
    apiInstance.DeleteMetadataTreeWithHttpInfo(projectId, resourceId, metadataTreeForDeletionDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling TreeApi.DeleteMetadataTreeWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The unique identifier or slug of the project. |  |
| **resourceId** | **Guid** | The unique identifier of the resource. |  |
| **metadataTreeForDeletionDto** | [**MetadataTreeForDeletionDto?**](MetadataTreeForDeletionDto?.md) | The dto for the deletion. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Metadata tree deleted. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format or the resource is write-protected due to its archived status. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getfiletree"></a>
# **GetFileTree**
> FileTreeDtoPagedResponse GetFileTree (string projectId, Guid resourceId, string? path = null, int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Retrieves the file tree associated with a resource.

The `OrderBy` query is currently not supported.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetFileTreeExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TreeApi(config);
            var projectId = "projectId_example";  // string | The unique identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The unique identifier of the resource.
            var path = "path_example";  // string? | Gets or sets the path of the file tree. (optional) 
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves the file tree associated with a resource.
                FileTreeDtoPagedResponse result = apiInstance.GetFileTree(projectId, resourceId, path, pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling TreeApi.GetFileTree: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetFileTreeWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the file tree associated with a resource.
    ApiResponse<FileTreeDtoPagedResponse> response = apiInstance.GetFileTreeWithHttpInfo(projectId, resourceId, path, pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling TreeApi.GetFileTreeWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The unique identifier or slug of the project. |  |
| **resourceId** | **Guid** | The unique identifier of the resource. |  |
| **path** | **string?** | Gets or sets the path of the file tree. | [optional]  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**FileTreeDtoPagedResponse**](FileTreeDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the file tree of a resource. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Project does not exist or has been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getmetadatatree"></a>
# **GetMetadataTree**
> MetadataTreeDtoPagedResponse GetMetadataTree (string projectId, Guid resourceId, string? path = null, RdfFormat? format = null, bool? includeExtractedMetadata = null, bool? includeProvenance = null, int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Retrieves the metadata tree associated with a resource.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetMetadataTreeExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TreeApi(config);
            var projectId = "projectId_example";  // string | The unique identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The unique identifier of the resource.
            var path = "path_example";  // string? | Gets or sets the path of the metadata tree. (optional) 
            var format = new RdfFormat?(); // RdfFormat? | Gets or sets the format of the RDF data. (optional) 
            var includeExtractedMetadata = true;  // bool? | Gets or sets if extracted metadata should be included. (optional) 
            var includeProvenance = true;  // bool? | Gets or sets if provenance metadata should be included. (optional) 
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves the metadata tree associated with a resource.
                MetadataTreeDtoPagedResponse result = apiInstance.GetMetadataTree(projectId, resourceId, path, format, includeExtractedMetadata, includeProvenance, pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling TreeApi.GetMetadataTree: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetMetadataTreeWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the metadata tree associated with a resource.
    ApiResponse<MetadataTreeDtoPagedResponse> response = apiInstance.GetMetadataTreeWithHttpInfo(projectId, resourceId, path, format, includeExtractedMetadata, includeProvenance, pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling TreeApi.GetMetadataTreeWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The unique identifier or slug of the project. |  |
| **resourceId** | **Guid** | The unique identifier of the resource. |  |
| **path** | **string?** | Gets or sets the path of the metadata tree. | [optional]  |
| **format** | [**RdfFormat?**](RdfFormat?.md) | Gets or sets the format of the RDF data. | [optional]  |
| **includeExtractedMetadata** | **bool?** | Gets or sets if extracted metadata should be included. | [optional]  |
| **includeProvenance** | **bool?** | Gets or sets if provenance metadata should be included. | [optional]  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**MetadataTreeDtoPagedResponse**](MetadataTreeDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the metadata tree of a resource. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Project does not exist or has been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getspecificmetadatatree"></a>
# **GetSpecificMetadataTree**
> MetadataTreeDtoResponse GetSpecificMetadataTree (string projectId, Guid resourceId, string path, RdfFormat? format = null, bool? includeExtractedMetadata = null, bool? includeProvenance = null, int? version = null)

Retrieves the specific metadata tree associated with a resource.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetSpecificMetadataTreeExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TreeApi(config);
            var projectId = "projectId_example";  // string | The unique identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The unique identifier of the resource.
            var path = "path_example";  // string | Gets or sets the path of the metadata tree.
            var format = new RdfFormat?(); // RdfFormat? | Gets or sets the format of the RDF data. (optional) 
            var includeExtractedMetadata = true;  // bool? | Gets or sets if extracted metadata should be included. (optional) 
            var includeProvenance = true;  // bool? | Gets or sets if provenance should be included. (optional) 
            var version = 56;  // int? | Gets or sets the desired version.  If the version is null, the newest will be returned. (optional) 

            try
            {
                // Retrieves the specific metadata tree associated with a resource.
                MetadataTreeDtoResponse result = apiInstance.GetSpecificMetadataTree(projectId, resourceId, path, format, includeExtractedMetadata, includeProvenance, version);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling TreeApi.GetSpecificMetadataTree: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetSpecificMetadataTreeWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the specific metadata tree associated with a resource.
    ApiResponse<MetadataTreeDtoResponse> response = apiInstance.GetSpecificMetadataTreeWithHttpInfo(projectId, resourceId, path, format, includeExtractedMetadata, includeProvenance, version);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling TreeApi.GetSpecificMetadataTreeWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The unique identifier or slug of the project. |  |
| **resourceId** | **Guid** | The unique identifier of the resource. |  |
| **path** | **string** | Gets or sets the path of the metadata tree. |  |
| **format** | [**RdfFormat?**](RdfFormat?.md) | Gets or sets the format of the RDF data. | [optional]  |
| **includeExtractedMetadata** | **bool?** | Gets or sets if extracted metadata should be included. | [optional]  |
| **includeProvenance** | **bool?** | Gets or sets if provenance should be included. | [optional]  |
| **version** | **int?** | Gets or sets the desired version.  If the version is null, the newest will be returned. | [optional]  |

### Return type

[**MetadataTreeDtoResponse**](MetadataTreeDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the specific metadata tree of a resource. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Project does not exist or has been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="updateextractedmetadatatree"></a>
# **UpdateExtractedMetadataTree**
> void UpdateExtractedMetadataTree (string projectId, Guid resourceId, ExtractedMetadataTreeForUpdateDto? extractedMetadataTreeForUpdateDto = null)

Updates an existing metadata tree of a resource.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class UpdateExtractedMetadataTreeExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TreeApi(config);
            var projectId = "projectId_example";  // string | The unique identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The unique identifier of the resource.
            var extractedMetadataTreeForUpdateDto = new ExtractedMetadataTreeForUpdateDto?(); // ExtractedMetadataTreeForUpdateDto? | The updated metadata tree data. (optional) 

            try
            {
                // Updates an existing metadata tree of a resource.
                apiInstance.UpdateExtractedMetadataTree(projectId, resourceId, extractedMetadataTreeForUpdateDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling TreeApi.UpdateExtractedMetadataTree: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the UpdateExtractedMetadataTreeWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Updates an existing metadata tree of a resource.
    apiInstance.UpdateExtractedMetadataTreeWithHttpInfo(projectId, resourceId, extractedMetadataTreeForUpdateDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling TreeApi.UpdateExtractedMetadataTreeWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The unique identifier or slug of the project. |  |
| **resourceId** | **Guid** | The unique identifier of the resource. |  |
| **extractedMetadataTreeForUpdateDto** | [**ExtractedMetadataTreeForUpdateDto?**](ExtractedMetadataTreeForUpdateDto?.md) | The updated metadata tree data. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Metadata tree updated. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format or the resource is write-protected due to its archived status. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="updatemetadatatree"></a>
# **UpdateMetadataTree**
> void UpdateMetadataTree (string projectId, Guid resourceId, MetadataTreeForUpdateDto? metadataTreeForUpdateDto = null)

Updates an existing metadata tree of a resource.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class UpdateMetadataTreeExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new TreeApi(config);
            var projectId = "projectId_example";  // string | The unique identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The unique identifier of the resource.
            var metadataTreeForUpdateDto = new MetadataTreeForUpdateDto?(); // MetadataTreeForUpdateDto? | The updated metadata tree data. (optional) 

            try
            {
                // Updates an existing metadata tree of a resource.
                apiInstance.UpdateMetadataTree(projectId, resourceId, metadataTreeForUpdateDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling TreeApi.UpdateMetadataTree: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the UpdateMetadataTreeWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Updates an existing metadata tree of a resource.
    apiInstance.UpdateMetadataTreeWithHttpInfo(projectId, resourceId, metadataTreeForUpdateDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling TreeApi.UpdateMetadataTreeWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The unique identifier or slug of the project. |  |
| **resourceId** | **Guid** | The unique identifier of the resource. |  |
| **metadataTreeForUpdateDto** | [**MetadataTreeForUpdateDto?**](MetadataTreeForUpdateDto?.md) | The updated metadata tree data. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Metadata tree updated. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format or the resource is write-protected due to its archived status. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

