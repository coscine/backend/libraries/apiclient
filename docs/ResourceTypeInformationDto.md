# Coscine.ApiClient.Core.Model.ResourceTypeInformationDto
Represents information about a resource type.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | The unique identifier of the resource type. | [optional] 
**GeneralType** | **string** | The general type of the resource. | [optional] 
**SpecificType** | **string** | The specific type of the resource. | [optional] 
**Status** | **ResourceTypeStatus** |  | [optional] 
**CanCreate** | **bool** | Indicates if the resource type supports creation. | [optional] 
**CanRead** | **bool** | Indicates if the resource type supports reading. | [optional] 
**CanSetResourceReadonly** | **bool** | Indicates if the resource type supports read-only. | [optional] 
**CanUpdate** | **bool** | Indicates if the resource type supports updating. | [optional] 
**CanUpdateResource** | **bool** | Indicates if the resource type supports updating (not an Object). | [optional] 
**CanDelete** | **bool** | Indicates if the resource type supports deletion. | [optional] 
**CanDeleteResource** | **bool** | Indicates if the resource type supports deletion (not an Object). | [optional] 
**CanList** | **bool** | Indicates if the resource type supports listing. | [optional] 
**CanCreateLinks** | **bool** | Indicates if the resource type supports linking. | [optional] 
**CanCopyLocalMetadata** | **bool** | Indicates whether local backup of metadata is supported. | [optional] 
**IsArchived** | **bool** | Indicates if the resource type is archived. | [optional] 
**IsQuotaAvailable** | **bool** | Indicates if the resource type supports quota. | [optional] 
**IsQuotaAdjustable** | **bool** | Indicates if the resource type quota can be changed. | [optional] 
**IsEnabled** | **bool** | Indicates if the resource type is enabled. | [optional] [readonly] 
**ResourceCreation** | [**ResourceCreationPageDto**](ResourceCreationPageDto.md) |  | [optional] 
**ResourceContent** | [**ResourceContentPageDto**](ResourceContentPageDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

