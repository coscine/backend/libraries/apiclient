# Coscine.ApiClient.Core.Model.RdsS3WormResourceTypeOptionsForManipulationDto
Represents the data transfer object (DTO) for manipulating RDS S3 WORM resource type options.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Quota** | [**QuotaForManipulationDto**](QuotaForManipulationDto.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

