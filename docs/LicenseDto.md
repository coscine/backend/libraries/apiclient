# Coscine.ApiClient.Core.Model.LicenseDto
Represents a Data Transfer Object (DTO) for license details.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Gets or sets the unique identifier for the license. | [optional] 
**DisplayName** | **string** | Gets or sets the display name of the license. | [optional] 
**Url** | **string** | Gets or sets the Uri of the license. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

