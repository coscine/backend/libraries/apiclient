# Coscine.ApiClient.Core.Model.IdentityProviderDto
Represents a Data Transfer Object (DTO) for an identity provider.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Gets or sets the unique identifier for the identity provider. | 
**DisplayName** | **string** | Gets or sets the display name of the identity provider. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

