# Coscine.ApiClient.Core.Model.ProjectInvitationResolveDto
Data transfer object (DTO) for resolving a project invitation.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Token** | **Guid** | Gets or initializes the token associated with resolving the project invitation. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

