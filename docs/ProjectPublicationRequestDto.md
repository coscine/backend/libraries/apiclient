# Coscine.ApiClient.Core.Model.ProjectPublicationRequestDto
Represents a data transfer object (DTO) for a project publication request.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Unique identifier for the project publication request. | [optional] 
**Project** | [**ProjectMinimalDto**](ProjectMinimalDto.md) |  | [optional] 
**PublicationServiceRorId** | **string** | Identifier for the publication service used for this request. | [optional] 
**Creator** | [**UserMinimalDto**](UserMinimalDto.md) |  | [optional] 
**DateCreated** | **DateTime** | The date and time when the request was created. | [optional] 
**Message** | **string** | Optional message associated with the publication request. | [optional] 
**Resources** | [**List&lt;ResourceMinimalDto&gt;**](ResourceMinimalDto.md) | Collection of the resources involved in the publication request. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

