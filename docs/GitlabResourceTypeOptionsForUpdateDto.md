# Coscine.ApiClient.Core.Model.GitlabResourceTypeOptionsForUpdateDto
Represents the data transfer object (DTO) for updating GitLab resource type options.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Branch** | **string** | The branch associated with the GitLab resource. | 
**AccessToken** | **string** | The access token for authentication with GitLab. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

