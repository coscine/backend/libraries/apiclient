# Coscine.ApiClient.Core.Model.FixedValueForResourceManipulationDto
Data transfer object (DTO) representing the manipulation of a fixed value associated with a resource.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Value** | **string** | Gets or initializes the value of the fixed resource value. | [optional] 
**Type** | **string** | Gets or initializes the type of the fixed resource value. | [optional] 
**Datatype** | **string** | Gets or initializes the data type URI of the fixed resource value. | [optional] 
**TargetClass** | **string** | The target class of the provided value (e.g., \&quot;https://purl.org/coscine/ap/base/\&quot;) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

