# Coscine.ApiClient.Core.Api.MaintenanceApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2MaintenancesOptions**](MaintenanceApi.md#apiv2maintenancesoptions) | **OPTIONS** /api/v2/maintenances | Responds with the HTTP methods allowed for the endpoint. |
| [**GetCurrentMaintenances**](MaintenanceApi.md#getcurrentmaintenances) | **GET** /api/v2/maintenances | Retrieves the current maintenance messages. |

<a id="apiv2maintenancesoptions"></a>
# **ApiV2MaintenancesOptions**
> void ApiV2MaintenancesOptions ()

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2MaintenancesOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new MaintenanceApi(config);

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2MaintenancesOptions();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling MaintenanceApi.ApiV2MaintenancesOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2MaintenancesOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2MaintenancesOptionsWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling MaintenanceApi.ApiV2MaintenancesOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getcurrentmaintenances"></a>
# **GetCurrentMaintenances**
> MaintenanceDtoPagedResponse GetCurrentMaintenances ()

Retrieves the current maintenance messages.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetCurrentMaintenancesExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new MaintenanceApi(config);

            try
            {
                // Retrieves the current maintenance messages.
                MaintenanceDtoPagedResponse result = apiInstance.GetCurrentMaintenances();
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling MaintenanceApi.GetCurrentMaintenances: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetCurrentMaintenancesWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the current maintenance messages.
    ApiResponse<MaintenanceDtoPagedResponse> response = apiInstance.GetCurrentMaintenancesWithHttpInfo();
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling MaintenanceApi.GetCurrentMaintenancesWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

[**MaintenanceDtoPagedResponse**](MaintenanceDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the maintenances. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

