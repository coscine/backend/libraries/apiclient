# Coscine.ApiClient.Core.Api.ProjectQuotaApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2ProjectsProjectIdQuotasOptions**](ProjectQuotaApi.md#apiv2projectsprojectidquotasoptions) | **OPTIONS** /api/v2/projects/{projectId}/quotas | Responds with the HTTP methods allowed for the endpoint. |
| [**GetProjectQuota**](ProjectQuotaApi.md#getprojectquota) | **GET** /api/v2/projects/{projectId}/quotas/{resourceTypeId} | Retrieves a project quota for a specified project and resource type. |
| [**GetProjectQuotas**](ProjectQuotaApi.md#getprojectquotas) | **GET** /api/v2/projects/{projectId}/quotas | Retrieves all project quotas for a specified project. |
| [**UpdateProjectQuota**](ProjectQuotaApi.md#updateprojectquota) | **PUT** /api/v2/projects/{projectId}/quotas/{resourceTypeId} | Updates a project quota for a specified project and resource type. |

<a id="apiv2projectsprojectidquotasoptions"></a>
# **ApiV2ProjectsProjectIdQuotasOptions**
> void ApiV2ProjectsProjectIdQuotasOptions (string projectId)

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ProjectsProjectIdQuotasOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectQuotaApi(config);
            var projectId = "projectId_example";  // string | 

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ProjectsProjectIdQuotasOptions(projectId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectQuotaApi.ApiV2ProjectsProjectIdQuotasOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ProjectsProjectIdQuotasOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ProjectsProjectIdQuotasOptionsWithHttpInfo(projectId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectQuotaApi.ApiV2ProjectsProjectIdQuotasOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** |  |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getprojectquota"></a>
# **GetProjectQuota**
> ProjectQuotaDtoResponse GetProjectQuota (string projectId, Guid resourceTypeId)

Retrieves a project quota for a specified project and resource type.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetProjectQuotaExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectQuotaApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var resourceTypeId = "resourceTypeId_example";  // Guid | The ID of the resource type.

            try
            {
                // Retrieves a project quota for a specified project and resource type.
                ProjectQuotaDtoResponse result = apiInstance.GetProjectQuota(projectId, resourceTypeId);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectQuotaApi.GetProjectQuota: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetProjectQuotaWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves a project quota for a specified project and resource type.
    ApiResponse<ProjectQuotaDtoResponse> response = apiInstance.GetProjectQuotaWithHttpInfo(projectId, resourceTypeId);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectQuotaApi.GetProjectQuotaWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **resourceTypeId** | **Guid** | The ID of the resource type. |  |

### Return type

[**ProjectQuotaDtoResponse**](ProjectQuotaDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the project quota. |  -  |
| **404** | Project quota does not exist. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getprojectquotas"></a>
# **GetProjectQuotas**
> ProjectQuotaDtoPagedResponse GetProjectQuotas (string projectId, int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Retrieves all project quotas for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetProjectQuotasExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectQuotaApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves all project quotas for a specified project.
                ProjectQuotaDtoPagedResponse result = apiInstance.GetProjectQuotas(projectId, pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectQuotaApi.GetProjectQuotas: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetProjectQuotasWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all project quotas for a specified project.
    ApiResponse<ProjectQuotaDtoPagedResponse> response = apiInstance.GetProjectQuotasWithHttpInfo(projectId, pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectQuotaApi.GetProjectQuotasWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**ProjectQuotaDtoPagedResponse**](ProjectQuotaDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the project quotas. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="updateprojectquota"></a>
# **UpdateProjectQuota**
> void UpdateProjectQuota (string projectId, Guid resourceTypeId, ProjectQuotaForUpdateDto? projectQuotaForUpdateDto = null)

Updates a project quota for a specified project and resource type.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class UpdateProjectQuotaExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectQuotaApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var resourceTypeId = "resourceTypeId_example";  // Guid | The ID of the resource type.
            var projectQuotaForUpdateDto = new ProjectQuotaForUpdateDto?(); // ProjectQuotaForUpdateDto? | The updated project quota data. (optional) 

            try
            {
                // Updates a project quota for a specified project and resource type.
                apiInstance.UpdateProjectQuota(projectId, resourceTypeId, projectQuotaForUpdateDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectQuotaApi.UpdateProjectQuota: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the UpdateProjectQuotaWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Updates a project quota for a specified project and resource type.
    apiInstance.UpdateProjectQuotaWithHttpInfo(projectId, resourceTypeId, projectQuotaForUpdateDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectQuotaApi.UpdateProjectQuotaWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **resourceTypeId** | **Guid** | The ID of the resource type. |  |
| **projectQuotaForUpdateDto** | [**ProjectQuotaForUpdateDto?**](ProjectQuotaForUpdateDto?.md) | The updated project quota data. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Project quota updated. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

