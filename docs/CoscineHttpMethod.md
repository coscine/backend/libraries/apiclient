# Coscine.ApiClient.Core.Model.CoscineHttpMethod
An enumeration representing the supported HTTP verbs.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

