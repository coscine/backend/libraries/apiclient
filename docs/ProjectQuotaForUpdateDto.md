# Coscine.ApiClient.Core.Model.ProjectQuotaForUpdateDto
Data transfer object (DTO) representing the update of project quotas.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Allocated** | [**QuotaForManipulationDto**](QuotaForManipulationDto.md) |  | 
**Maximum** | [**QuotaForManipulationDto**](QuotaForManipulationDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

