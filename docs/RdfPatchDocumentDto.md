# Coscine.ApiClient.Core.Model.RdfPatchDocumentDto
Represents an RDF Patch document containing a series of operations.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Operations** | [**List&lt;RdfPatchOperationDto&gt;**](RdfPatchOperationDto.md) | The list of operations in the RDF Patch document. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

