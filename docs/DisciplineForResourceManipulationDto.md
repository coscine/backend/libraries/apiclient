# Coscine.ApiClient.Core.Model.DisciplineForResourceManipulationDto
Data transfer object (DTO) representing a discipline for resource manipulation.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Gets or initializes the identifier of the discipline. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

