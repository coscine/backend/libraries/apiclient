# Coscine.ApiClient.Core.Model.ProvenanceParametersDto
Data transfer object (DTO) representing Provenance Parameters in a request.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**WasRevisionOf** | **List&lt;string&gt;** | Gets or sets the adapted versions from the specific metadata tree. | [optional] 
**Variants** | [**List&lt;VariantDto&gt;**](VariantDto.md) | Gets or sets the variants of the specific metadata tree. | [optional] 
**WasInvalidatedBy** | **string** | Information if the specific metadata tree was invalidated by something. | [optional] 
**SimilarityToLastVersion** | **double?** | The similarity to the last version. | [optional] 
**MetadataExtractorVersion** | **string** | Gets or initializes the version of the metadata extractor. | [optional] 
**HashParameters** | [**HashParametersDto**](HashParametersDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

