# Coscine.ApiClient.Core.Model.ResourceForUpdateDto
Represents the data transfer object (DTO) used for updating a resource.  Inherits properties from Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ResourceForManipulationDto.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | The name of the resource. | 
**DisplayName** | **string** | The display name of the resource. | [optional] 
**Description** | **string** | The description of the resource. | 
**Keywords** | **List&lt;string&gt;** | The keywords associated with the resource. | [optional] 
**FixedValues** | **Dictionary&lt;string, Dictionary&lt;string, List&lt;FixedValueForResourceManipulationDto&gt;&gt;&gt;** | Fixed values associated with the resource. | [optional] 
**License** | [**LicenseForResourceManipulationDto**](LicenseForResourceManipulationDto.md) |  | [optional] 
**UsageRights** | **string** | The usage rights description of the resource. | [optional] 
**MetadataLocalCopy** | **bool?** | If a local copy for the metadata should be created. | [optional] 
**Visibility** | [**VisibilityForResourceManipulationDto**](VisibilityForResourceManipulationDto.md) |  | 
**Disciplines** | [**List&lt;DisciplineForResourceManipulationDto&gt;**](DisciplineForResourceManipulationDto.md) | The disciplines associated with the resource. | 
**Archived** | **bool** | Indicates whether the resource is archived. | 
**ResourceTypeOptions** | [**ResourceTypeOptionsForUpdateDto**](ResourceTypeOptionsForUpdateDto.md) |  | [optional] 
**MaintenanceMode** | **bool?** | Indicates whether the resource is in maintenance mode. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

