# Coscine.ApiClient.Core.Model.ApiTokenForCreationDto
Data transfer object (DTO) representing the creation of an API token.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | Gets or sets the name of the API token. | 
**ExpiresInDays** | **int** | Gets or sets the expiration duration of the token in days. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

