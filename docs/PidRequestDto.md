# Coscine.ApiClient.Core.Model.PidRequestDto
Data transfer object (DTO) representing a PID request.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | Gets or initializes the name associated with the PID request. | 
**Email** | **string** | Gets or initializes the email associated with the PID request. | 
**Message** | **string** | Gets or initializes the message for the PID request. | 
**SendConfirmationEmail** | **bool** | Gets or initializes whether to send a confirmation email for the PID request. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

