# Coscine.ApiClient.Core.Model.PublicationRequestForCreationDto
Data transfer object (DTO) representing the creation of a publication request.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PublicationServiceRorId** | **string** | The data publication service&#39;s ror id. | 
**ResourceIds** | **List&lt;Guid&gt;** | The resource guids. | 
**Message** | **string** | A message of the requester. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

