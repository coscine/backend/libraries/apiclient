# Coscine.ApiClient.Core.Model.ActivityLogDto
Represents a data transfer object (DTO) for an activity log.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** |  | [optional] 
**ApiPath** | **string** |  | [optional] 
**HttpAction** | **string** |  | [optional] 
**ControllerName** | **string** |  | [optional] 
**ActionName** | **string** |  | [optional] 
**UserId** | **Guid** |  | [optional] 
**ActivityTimestamp** | **DateTime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

