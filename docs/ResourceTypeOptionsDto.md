# Coscine.ApiClient.Core.Model.ResourceTypeOptionsDto
Represents the options available for different resource types.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LinkedData** | **Object** | Represents the data transfer object (DTO) for Linked Data options. | [optional] 
**GitLab** | [**GitLabOptionsDto**](GitLabOptionsDto.md) |  | [optional] 
**Rds** | [**RdsOptionsDto**](RdsOptionsDto.md) |  | [optional] 
**RdsS3** | [**RdsS3OptionsDto**](RdsS3OptionsDto.md) |  | [optional] 
**RdsS3Worm** | [**RdsS3WormOptionsDto**](RdsS3WormOptionsDto.md) |  | [optional] 
**FileSystemStorage** | [**FileSystemStorageOptionsDto**](FileSystemStorageOptionsDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

