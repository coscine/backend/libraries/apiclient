# Coscine.ApiClient.Core.Model.TreeDataType
Represents the types of nodes within a hierarchical tree structure.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

