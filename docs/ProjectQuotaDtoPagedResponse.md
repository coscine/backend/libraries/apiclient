# Coscine.ApiClient.Core.Model.ProjectQuotaDtoPagedResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | [**List&lt;ProjectQuotaDto&gt;**](ProjectQuotaDto.md) |  | [optional] 
**IsSuccess** | **bool** |  | [optional] [readonly] 
**StatusCode** | **int?** |  | [optional] 
**TraceId** | **string** |  | [optional] 
**Pagination** | [**Pagination**](Pagination.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

