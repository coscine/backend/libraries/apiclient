# Coscine.ApiClient.Core.Model.OrganizationDto
Represents a data transfer object (DTO) for an organization, including its ROR (Research Organization Registry) ID, display name, and email address.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Uri** | **string** | The ROR (Research Organization Registry) ID of the organization. | 
**DisplayName** | **string** | The display name of the organization. | 
**Email** | **string** | The email address of the organization. | [optional] 
**PublicationAdvisoryService** | [**PublicationAdvisoryServiceDto**](PublicationAdvisoryServiceDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

