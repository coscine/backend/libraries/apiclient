# Coscine.ApiClient.Core.Model.MaintenanceDto
This class represents a maintenance with its significant properties, which is returned from the API.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DisplayName** | **string** | Gets or sets the maintenance title. | [optional] 
**Href** | **string** | Gets or sets the URL related to the maintenance. | [optional] 
**Type** | **string** | Gets or sets the type of maintenance. | [optional] 
**Body** | **string** | Gets or sets the description of the maintenance. | [optional] 
**StartsDate** | **DateTime?** | Gets or sets the start date of the maintenance, if available. | [optional] 
**EndsDate** | **DateTime?** | Gets or sets the end date of the maintenance, if available. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

