# Coscine.ApiClient.Core.Model.ProjectAdminDto
Represents a data transfer object (DTO) for an administrative view of a project.  Extends the base information in Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ProjectDto.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Unique identifier for the project. | 
**Pid** | **string** | Persistent identifier for the project. | 
**Name** | **string** | Name of the project. | 
**Description** | **string** | Description of the project. | 
**StartDate** | **DateTime** | Start date of the project. | 
**EndDate** | **DateTime** | End date of the project. | 
**Keywords** | **List&lt;string&gt;** | Collection of keywords associated with the project. | [optional] 
**DisplayName** | **string** | Display name of the project. | [optional] 
**PrincipleInvestigators** | **string** | Principal investigators involved in the project. | [optional] 
**GrantId** | **string** | Grant ID associated with the project. | [optional] 
**Visibility** | [**VisibilityDto**](VisibilityDto.md) |  | 
**Disciplines** | [**List&lt;DisciplineDto&gt;**](DisciplineDto.md) | Disciplines related to the project. | 
**Organizations** | [**List&lt;ProjectOrganizationDto&gt;**](ProjectOrganizationDto.md) | Organizations associated with the project. | 
**Slug** | **string** | Slug for the project. | 
**Creator** | [**UserMinimalDto**](UserMinimalDto.md) |  | [optional] 
**CreationDate** | **DateTime?** | Date of creation of the project. | [optional] 
**SubProjects** | [**List&lt;ProjectDto&gt;**](ProjectDto.md) | Collection of sub-projects associated with this project. | [optional] 
**Parent** | [**ProjectMinimalDto**](ProjectMinimalDto.md) |  | [optional] 
**Deleted** | **bool** | Indicates whether the project is marked as deleted. | [optional] 
**ProjectResources** | [**List&lt;ProjectResourceMinimalDto&gt;**](ProjectResourceMinimalDto.md) | Collection of minimal project resource details associated with the project. | [optional] 
**ProjectRoles** | [**List&lt;ProjectRoleMinimalDto&gt;**](ProjectRoleMinimalDto.md) | Collection of minimal project role details associated with the project. | [optional] 
**ProjectQuota** | [**List&lt;ProjectQuotaDto&gt;**](ProjectQuotaDto.md) | Collection of project quotas associated with the project. | [optional] 
**PublicationRequests** | [**List&lt;ProjectPublicationRequestDto&gt;**](ProjectPublicationRequestDto.md) | Collection of project publication requests associated with the project. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

