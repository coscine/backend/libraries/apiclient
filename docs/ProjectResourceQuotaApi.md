# Coscine.ApiClient.Core.Api.ProjectResourceQuotaApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2ProjectsProjectIdResourcesResourceIdQuotaOptions**](ProjectResourceQuotaApi.md#apiv2projectsprojectidresourcesresourceidquotaoptions) | **OPTIONS** /api/v2/projects/{projectId}/resources/{resourceId}/quota | Responds with the HTTP methods allowed for the endpoint. |
| [**GetQuotaForResourceForProject**](ProjectResourceQuotaApi.md#getquotaforresourceforproject) | **GET** /api/v2/projects/{projectId}/resources/{resourceId}/quota | Retrieves the resource quota for a specific resource in a project. |

<a id="apiv2projectsprojectidresourcesresourceidquotaoptions"></a>
# **ApiV2ProjectsProjectIdResourcesResourceIdQuotaOptions**
> void ApiV2ProjectsProjectIdResourcesResourceIdQuotaOptions (string projectId, string resourceId)

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ProjectsProjectIdResourcesResourceIdQuotaOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectResourceQuotaApi(config);
            var projectId = "projectId_example";  // string | 
            var resourceId = "resourceId_example";  // string | 

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ProjectsProjectIdResourcesResourceIdQuotaOptions(projectId, resourceId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectResourceQuotaApi.ApiV2ProjectsProjectIdResourcesResourceIdQuotaOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ProjectsProjectIdResourcesResourceIdQuotaOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ProjectsProjectIdResourcesResourceIdQuotaOptionsWithHttpInfo(projectId, resourceId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectResourceQuotaApi.ApiV2ProjectsProjectIdResourcesResourceIdQuotaOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** |  |  |
| **resourceId** | **string** |  |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getquotaforresourceforproject"></a>
# **GetQuotaForResourceForProject**
> ResourceQuotaDtoResponse GetQuotaForResourceForProject (string projectId, Guid resourceId)

Retrieves the resource quota for a specific resource in a project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetQuotaForResourceForProjectExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectResourceQuotaApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The ID of the resource.

            try
            {
                // Retrieves the resource quota for a specific resource in a project.
                ResourceQuotaDtoResponse result = apiInstance.GetQuotaForResourceForProject(projectId, resourceId);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectResourceQuotaApi.GetQuotaForResourceForProject: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetQuotaForResourceForProjectWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the resource quota for a specific resource in a project.
    ApiResponse<ResourceQuotaDtoResponse> response = apiInstance.GetQuotaForResourceForProjectWithHttpInfo(projectId, resourceId);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectResourceQuotaApi.GetQuotaForResourceForProjectWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **resourceId** | **Guid** | The ID of the resource. |  |

### Return type

[**ResourceQuotaDtoResponse**](ResourceQuotaDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the resource quota. |  -  |
| **403** | User is missing authorization requirements. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

