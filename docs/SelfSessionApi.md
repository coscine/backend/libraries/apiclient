# Coscine.ApiClient.Core.Api.SelfSessionApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2SelfSessionLogoutPost**](SelfSessionApi.md#apiv2selfsessionlogoutpost) | **POST** /api/v2/self/session/logout | Initiate the Log out workflow. |
| [**ApiV2SelfSessionOptions**](SelfSessionApi.md#apiv2selfsessionoptions) | **OPTIONS** /api/v2/self/session | Responds with the HTTP methods allowed for the endpoint. |
| [**Login**](SelfSessionApi.md#login) | **GET** /api/v2/self/session | Initiate the login workflow with the default (NFDI4Ing AAI) provider. |
| [**LoginWithProvider**](SelfSessionApi.md#loginwithprovider) | **GET** /api/v2/self/session/{externalAuthenticatorId} | Initiate the login workflow with specific provider. |
| [**Merge**](SelfSessionApi.md#merge) | **GET** /api/v2/self/session/merge/{externalAuthenticatorId} | Initiate the merge workflow with specific provider. |

<a id="apiv2selfsessionlogoutpost"></a>
# **ApiV2SelfSessionLogoutPost**
> void ApiV2SelfSessionLogoutPost ()

Initiate the Log out workflow.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2SelfSessionLogoutPostExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SelfSessionApi(config);

            try
            {
                // Initiate the Log out workflow.
                apiInstance.ApiV2SelfSessionLogoutPost();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SelfSessionApi.ApiV2SelfSessionLogoutPost: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2SelfSessionLogoutPostWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Initiate the Log out workflow.
    apiInstance.ApiV2SelfSessionLogoutPostWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SelfSessionApi.ApiV2SelfSessionLogoutPostWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="apiv2selfsessionoptions"></a>
# **ApiV2SelfSessionOptions**
> void ApiV2SelfSessionOptions ()

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2SelfSessionOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SelfSessionApi(config);

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2SelfSessionOptions();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SelfSessionApi.ApiV2SelfSessionOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2SelfSessionOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2SelfSessionOptionsWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SelfSessionApi.ApiV2SelfSessionOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="login"></a>
# **Login**
> void Login ()

Initiate the login workflow with the default (NFDI4Ing AAI) provider.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class LoginExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SelfSessionApi(config);

            try
            {
                // Initiate the login workflow with the default (NFDI4Ing AAI) provider.
                apiInstance.Login();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SelfSessionApi.Login: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the LoginWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Initiate the login workflow with the default (NFDI4Ing AAI) provider.
    apiInstance.LoginWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SelfSessionApi.LoginWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="loginwithprovider"></a>
# **LoginWithProvider**
> void LoginWithProvider (Guid externalAuthenticatorId)

Initiate the login workflow with specific provider.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class LoginWithProviderExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SelfSessionApi(config);
            var externalAuthenticatorId = "externalAuthenticatorId_example";  // Guid | 

            try
            {
                // Initiate the login workflow with specific provider.
                apiInstance.LoginWithProvider(externalAuthenticatorId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SelfSessionApi.LoginWithProvider: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the LoginWithProviderWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Initiate the login workflow with specific provider.
    apiInstance.LoginWithProviderWithHttpInfo(externalAuthenticatorId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SelfSessionApi.LoginWithProviderWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **externalAuthenticatorId** | **Guid** |  |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="merge"></a>
# **Merge**
> void Merge (Guid externalAuthenticatorId)

Initiate the merge workflow with specific provider.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class MergeExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new SelfSessionApi(config);
            var externalAuthenticatorId = "externalAuthenticatorId_example";  // Guid | 

            try
            {
                // Initiate the merge workflow with specific provider.
                apiInstance.Merge(externalAuthenticatorId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling SelfSessionApi.Merge: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the MergeWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Initiate the merge workflow with specific provider.
    apiInstance.MergeWithHttpInfo(externalAuthenticatorId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling SelfSessionApi.MergeWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **externalAuthenticatorId** | **Guid** |  |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

