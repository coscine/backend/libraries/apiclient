# Coscine.ApiClient.Core.Model.ProjectQuotaDto
Represents a data transfer object (DTO) containing quota information for a project.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ProjectId** | **Guid** | Identifier of the project. | [optional] 
**TotalUsed** | [**QuotaDto**](QuotaDto.md) |  | [optional] 
**TotalReserved** | [**QuotaDto**](QuotaDto.md) |  | [optional] 
**Allocated** | [**QuotaDto**](QuotaDto.md) |  | [optional] 
**Maximum** | [**QuotaDto**](QuotaDto.md) |  | [optional] 
**ResourceType** | [**ResourceTypeMinimalDto**](ResourceTypeMinimalDto.md) |  | [optional] 
**ResourceQuotas** | [**List&lt;ResourceQuotaDto&gt;**](ResourceQuotaDto.md) | Quota information for individual resources of the selected project&#39;s resource type. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

