# Coscine.ApiClient.Core.Model.VocabularyInstanceDto
Represents a Data Transfer Object (DTO) for vocabulary instance details.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GraphUri** | **string** | The URI of the graph containing the vocabulary. | 
**InstanceUri** | **string** | The URI of the instance. | [optional] 
**TypeUri** | **string** | The URI representing the type of the instance. | [optional] 
**SubClassOfUri** | **string** | The URI of the direct parent class. | [optional] 
**DisplayName** | **string** | The display name of the vocabulary instance. | [optional] 
**Description** | **string** | The description of the vocabulary instance. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

