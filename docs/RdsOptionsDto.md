# Coscine.ApiClient.Core.Model.RdsOptionsDto
Represents the data transfer object (DTO) for RDS options.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BucketName** | **string** | The name of the bucket associated with RDS. | 
**Size** | [**QuotaDto**](QuotaDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

