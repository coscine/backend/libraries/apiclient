# Coscine.ApiClient.Core.Model.PidDto
Represents a data transfer object (DTO) for a PID (Persistent Identifier).

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Prefix** | **string** | Gets or sets the prefix of the PID. | 
**Suffix** | **string** | Gets or sets the suffix of the PID. | 
**Identifier** | **string** | Gets the constructed PID by combining the prefix and suffix (\&quot;{Prefix}/{Suffix}\&quot;). | [optional] [readonly] 
**Type** | **PidType** |  | [optional] 
**IsEntityValid** | **bool** | Gets or sets a value indicating whether the linked entity is considered valid. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

