# Coscine.ApiClient.Core.Model.MetadataTreeDto
Represents a data transfer object (DTO) for metadata within a tree structure, extending the base TreeDto.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Path** | **string** | The path of the tree item. | [optional] 
**Type** | **TreeDataType** |  | [optional] 
**Id** | **string** | Gets or sets the id of the specific metadata tree. | [optional] 
**VarVersion** | **string** | Gets or sets the version associated with the metadata. | [optional] 
**AvailableVersions** | **List&lt;string&gt;** | Gets or sets the collection of available versions related to the metadata. | [optional] 
**Definition** | [**RdfDefinitionDto**](RdfDefinitionDto.md) |  | [optional] 
**Extracted** | [**MetadataTreeExtractedDto**](MetadataTreeExtractedDto.md) |  | [optional] 
**Provenance** | [**ProvenanceDto**](ProvenanceDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

