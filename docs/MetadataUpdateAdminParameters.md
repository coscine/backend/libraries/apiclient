# Coscine.ApiClient.Core.Model.MetadataUpdateAdminParameters
Represents parameters for updating metadata in an administrative context.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Definition** | [**RdfDefinitionForManipulationDto**](RdfDefinitionForManipulationDto.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

