# Coscine.ApiClient.Core.Model.ResourceQuotaDto
Represents a Data Transfer Object (DTO) containing quota information for a resource.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Resource** | [**ResourceMinimalDto**](ResourceMinimalDto.md) |  | 
**UsedPercentage** | **float?** | The percentage of quota used by the resource. | [optional] 
**Used** | [**QuotaDto**](QuotaDto.md) |  | [optional] 
**Reserved** | [**QuotaDto**](QuotaDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

