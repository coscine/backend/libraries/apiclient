# Coscine.ApiClient.Core.Api.ProjectResourceTypeApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2ProjectsProjectIdResourceTypesOptions**](ProjectResourceTypeApi.md#apiv2projectsprojectidresourcetypesoptions) | **OPTIONS** /api/v2/projects/{projectId}/resource-types | Responds with the HTTP methods allowed for the endpoint. |
| [**GetAvailableResourceTypesInformationForProject**](ProjectResourceTypeApi.md#getavailableresourcetypesinformationforproject) | **GET** /api/v2/projects/{projectId}/resource-types | Retrieves the available resource types information for a specific project. |

<a id="apiv2projectsprojectidresourcetypesoptions"></a>
# **ApiV2ProjectsProjectIdResourceTypesOptions**
> void ApiV2ProjectsProjectIdResourceTypesOptions (string projectId)

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ProjectsProjectIdResourceTypesOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectResourceTypeApi(config);
            var projectId = "projectId_example";  // string | 

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ProjectsProjectIdResourceTypesOptions(projectId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectResourceTypeApi.ApiV2ProjectsProjectIdResourceTypesOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ProjectsProjectIdResourceTypesOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ProjectsProjectIdResourceTypesOptionsWithHttpInfo(projectId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectResourceTypeApi.ApiV2ProjectsProjectIdResourceTypesOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** |  |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getavailableresourcetypesinformationforproject"></a>
# **GetAvailableResourceTypesInformationForProject**
> ResourceTypeInformationDtoIEnumerableResponse GetAvailableResourceTypesInformationForProject (string projectId)

Retrieves the available resource types information for a specific project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetAvailableResourceTypesInformationForProjectExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectResourceTypeApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.

            try
            {
                // Retrieves the available resource types information for a specific project.
                ResourceTypeInformationDtoIEnumerableResponse result = apiInstance.GetAvailableResourceTypesInformationForProject(projectId);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectResourceTypeApi.GetAvailableResourceTypesInformationForProject: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetAvailableResourceTypesInformationForProjectWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the available resource types information for a specific project.
    ApiResponse<ResourceTypeInformationDtoIEnumerableResponse> response = apiInstance.GetAvailableResourceTypesInformationForProjectWithHttpInfo(projectId);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectResourceTypeApi.GetAvailableResourceTypesInformationForProjectWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |

### Return type

[**ResourceTypeInformationDtoIEnumerableResponse**](ResourceTypeInformationDtoIEnumerableResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the available resourceTypeInformation types information. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

