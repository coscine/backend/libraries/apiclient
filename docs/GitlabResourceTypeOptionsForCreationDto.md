# Coscine.ApiClient.Core.Model.GitlabResourceTypeOptionsForCreationDto
Represents the data transfer object (DTO) for creating GitLab resource type options.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Branch** | **string** | The branch associated with the GitLab resource. | 
**AccessToken** | **string** | The access token for authentication with GitLab. | 
**RepoUrl** | **string** | The repository URL for the GitLab resource. | 
**ProjectId** | **int** | The project ID associated with the GitLab resource. | 
**TosAccepted** | **bool** | Indicates whether the terms of service for the GitLab resource are accepted. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

