# Coscine.ApiClient.Core.Api.ProvenanceApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2ProjectsProjectIdResourcesResourceIdProvenanceOptions**](ProvenanceApi.md#apiv2projectsprojectidresourcesresourceidprovenanceoptions) | **OPTIONS** /api/v2/projects/{projectId}/resources/{resourceId}/provenance | Responds with the HTTP methods allowed for the endpoint. |
| [**GetSpecificProvenance**](ProvenanceApi.md#getspecificprovenance) | **GET** /api/v2/projects/{projectId}/resources/{resourceId}/provenance/specific | Retrieves the specific provenance information associated with a resource. |
| [**UpdateSpecificProvenance**](ProvenanceApi.md#updatespecificprovenance) | **PUT** /api/v2/projects/{projectId}/resources/{resourceId}/provenance/specific | Updates existing specific provenance information of a resource. |

<a id="apiv2projectsprojectidresourcesresourceidprovenanceoptions"></a>
# **ApiV2ProjectsProjectIdResourcesResourceIdProvenanceOptions**
> void ApiV2ProjectsProjectIdResourcesResourceIdProvenanceOptions (string projectId, string resourceId)

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ProjectsProjectIdResourcesResourceIdProvenanceOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProvenanceApi(config);
            var projectId = "projectId_example";  // string | 
            var resourceId = "resourceId_example";  // string | 

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ProjectsProjectIdResourcesResourceIdProvenanceOptions(projectId, resourceId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProvenanceApi.ApiV2ProjectsProjectIdResourcesResourceIdProvenanceOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ProjectsProjectIdResourcesResourceIdProvenanceOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ProjectsProjectIdResourcesResourceIdProvenanceOptionsWithHttpInfo(projectId, resourceId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProvenanceApi.ApiV2ProjectsProjectIdResourcesResourceIdProvenanceOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** |  |  |
| **resourceId** | **string** |  |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getspecificprovenance"></a>
# **GetSpecificProvenance**
> ProvenanceDtoResponse GetSpecificProvenance (string projectId, Guid resourceId, string path, int? version = null)

Retrieves the specific provenance information associated with a resource.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetSpecificProvenanceExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProvenanceApi(config);
            var projectId = "projectId_example";  // string | The unique identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The unique identifier of the resource.
            var path = "path_example";  // string | Gets or sets the path of the metadata tree.
            var version = 56;  // int? | Gets or sets the desired version.  If the version is null, the newest will be returned. (optional) 

            try
            {
                // Retrieves the specific provenance information associated with a resource.
                ProvenanceDtoResponse result = apiInstance.GetSpecificProvenance(projectId, resourceId, path, version);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProvenanceApi.GetSpecificProvenance: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetSpecificProvenanceWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the specific provenance information associated with a resource.
    ApiResponse<ProvenanceDtoResponse> response = apiInstance.GetSpecificProvenanceWithHttpInfo(projectId, resourceId, path, version);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProvenanceApi.GetSpecificProvenanceWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The unique identifier or slug of the project. |  |
| **resourceId** | **Guid** | The unique identifier of the resource. |  |
| **path** | **string** | Gets or sets the path of the metadata tree. |  |
| **version** | **int?** | Gets or sets the desired version.  If the version is null, the newest will be returned. | [optional]  |

### Return type

[**ProvenanceDtoResponse**](ProvenanceDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the specific metadata tree of a resource. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Project does not exist or has been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="updatespecificprovenance"></a>
# **UpdateSpecificProvenance**
> void UpdateSpecificProvenance (string projectId, Guid resourceId, ProvenanceForUpdateDto? provenanceForUpdateDto = null)

Updates existing specific provenance information of a resource.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class UpdateSpecificProvenanceExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProvenanceApi(config);
            var projectId = "projectId_example";  // string | The unique identifier or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The unique identifier of the resource.
            var provenanceForUpdateDto = new ProvenanceForUpdateDto?(); // ProvenanceForUpdateDto? | The updated provenance information. (optional) 

            try
            {
                // Updates existing specific provenance information of a resource.
                apiInstance.UpdateSpecificProvenance(projectId, resourceId, provenanceForUpdateDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProvenanceApi.UpdateSpecificProvenance: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the UpdateSpecificProvenanceWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Updates existing specific provenance information of a resource.
    apiInstance.UpdateSpecificProvenanceWithHttpInfo(projectId, resourceId, provenanceForUpdateDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProvenanceApi.UpdateSpecificProvenanceWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The unique identifier or slug of the project. |  |
| **resourceId** | **Guid** | The unique identifier of the resource. |  |
| **provenanceForUpdateDto** | [**ProvenanceForUpdateDto?**](ProvenanceForUpdateDto?.md) | The updated provenance information. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Provenance information updated. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format or the resource is write-protected due to its archived status. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

