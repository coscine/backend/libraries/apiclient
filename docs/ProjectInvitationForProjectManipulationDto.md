# Coscine.ApiClient.Core.Model.ProjectInvitationForProjectManipulationDto
Data transfer object (DTO) representing an invitation for project manipulation.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RoleId** | **Guid** | Gets or initializes the identifier of the role associated with the invitation. | 
**Email** | **string** | Gets or initializes the email address of the invited user. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

