# Coscine.ApiClient.Core.Model.UserMergeDto
Represents a Data Transfer Object (DTO) for merging user accounts.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Token** | **string** | The token required for merging user accounts. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

