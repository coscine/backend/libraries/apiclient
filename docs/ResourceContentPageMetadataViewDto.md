# Coscine.ApiClient.Core.Model.ResourceContentPageMetadataViewDto
Represents the metadata view for a resource's content page.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EditableDataUrl** | **bool** | Indicates if a data URL can be provided and is editable. | [optional] 
**EditableKey** | **bool** | Indicates if a key can be provided and is editable. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

