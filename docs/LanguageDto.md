# Coscine.ApiClient.Core.Model.LanguageDto
Represents a Data Transfer Object (DTO) for language details.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Gets or sets the unique identifier for the language. | 
**DisplayName** | **string** | Gets or sets the display name of the language. | 
**Abbreviation** | **string** | Gets or sets the abbreviation for the language. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

