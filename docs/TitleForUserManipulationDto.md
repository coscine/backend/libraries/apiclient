# Coscine.ApiClient.Core.Model.TitleForUserManipulationDto
Represents the data transfer object (DTO) for manipulating a user's title.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | The unique identifier of the title. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

