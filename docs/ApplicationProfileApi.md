# Coscine.ApiClient.Core.Api.ApplicationProfileApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2ApplicationProfilesOptions**](ApplicationProfileApi.md#apiv2applicationprofilesoptions) | **OPTIONS** /api/v2/application-profiles | Responds with the HTTP methods allowed for the endpoint. |
| [**CreateApplicationProfileRequest**](ApplicationProfileApi.md#createapplicationprofilerequest) | **POST** /api/v2/application-profiles/requests | Submits a request to create a new application profile. |
| [**GetApplicationProfile**](ApplicationProfileApi.md#getapplicationprofile) | **GET** /api/v2/application-profiles/profiles/{profile} | Retrieves an application profile by its URI. |
| [**GetApplicationProfiles**](ApplicationProfileApi.md#getapplicationprofiles) | **GET** /api/v2/application-profiles/profiles | Retrieves all application profiles. |
| [**GetRawApplicationProfile**](ApplicationProfileApi.md#getrawapplicationprofile) | **GET** /api/v2/application-profiles/profiles/{profile}/raw | Retrieves the &#x60;raw&#x60; application profile definition by its URI. |

<a id="apiv2applicationprofilesoptions"></a>
# **ApiV2ApplicationProfilesOptions**
> void ApiV2ApplicationProfilesOptions ()

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ApplicationProfilesOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ApplicationProfileApi(config);

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ApplicationProfilesOptions();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ApplicationProfileApi.ApiV2ApplicationProfilesOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ApplicationProfilesOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ApplicationProfilesOptionsWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ApplicationProfileApi.ApiV2ApplicationProfilesOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="createapplicationprofilerequest"></a>
# **CreateApplicationProfileRequest**
> ApplicationProfileForCreationDtoResponse CreateApplicationProfileRequest (ApplicationProfileForCreationDto? applicationProfileForCreationDto = null)

Submits a request to create a new application profile.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class CreateApplicationProfileRequestExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ApplicationProfileApi(config);
            var applicationProfileForCreationDto = new ApplicationProfileForCreationDto?(); // ApplicationProfileForCreationDto? | The details required to create a new application profile. (optional) 

            try
            {
                // Submits a request to create a new application profile.
                ApplicationProfileForCreationDtoResponse result = apiInstance.CreateApplicationProfileRequest(applicationProfileForCreationDto);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ApplicationProfileApi.CreateApplicationProfileRequest: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the CreateApplicationProfileRequestWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Submits a request to create a new application profile.
    ApiResponse<ApplicationProfileForCreationDtoResponse> response = apiInstance.CreateApplicationProfileRequestWithHttpInfo(applicationProfileForCreationDto);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ApplicationProfileApi.CreateApplicationProfileRequestWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **applicationProfileForCreationDto** | [**ApplicationProfileForCreationDto?**](ApplicationProfileForCreationDto?.md) | The details required to create a new application profile. | [optional]  |

### Return type

[**ApplicationProfileForCreationDtoResponse**](ApplicationProfileForCreationDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Application profile request created. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getapplicationprofile"></a>
# **GetApplicationProfile**
> ApplicationProfileDtoResponse GetApplicationProfile (string profile, RdfFormat? format = null, AcceptedLanguage? acceptLanguage = null)

Retrieves an application profile by its URI.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetApplicationProfileExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ApplicationProfileApi(config);
            var profile = "profile_example";  // string | The URI of the application profile to retrieve.
            var format = new RdfFormat?(); // RdfFormat? | The desired data format for the returned application profile. (optional) 
            var acceptLanguage = new AcceptedLanguage?(); // AcceptedLanguage? | The preferred language for the application profile data. (optional) 

            try
            {
                // Retrieves an application profile by its URI.
                ApplicationProfileDtoResponse result = apiInstance.GetApplicationProfile(profile, format, acceptLanguage);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ApplicationProfileApi.GetApplicationProfile: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetApplicationProfileWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves an application profile by its URI.
    ApiResponse<ApplicationProfileDtoResponse> response = apiInstance.GetApplicationProfileWithHttpInfo(profile, format, acceptLanguage);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ApplicationProfileApi.GetApplicationProfileWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **profile** | **string** | The URI of the application profile to retrieve. |  |
| **format** | [**RdfFormat?**](RdfFormat?.md) | The desired data format for the returned application profile. | [optional]  |
| **acceptLanguage** | [**AcceptedLanguage?**](AcceptedLanguage?.md) | The preferred language for the application profile data. | [optional]  |

### Return type

[**ApplicationProfileDtoResponse**](ApplicationProfileDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the application profile. |  -  |
| **404** | Application profile does not exist. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getapplicationprofiles"></a>
# **GetApplicationProfiles**
> ApplicationProfileDtoPagedResponse GetApplicationProfiles (string? searchTerm = null, AcceptedLanguage? language = null, bool? modules = null, int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Retrieves all application profiles.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetApplicationProfilesExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ApplicationProfileApi(config);
            var searchTerm = "searchTerm_example";  // string? | Gets or sets the search term used to filter application profiles. (optional) 
            var language = new AcceptedLanguage?(); // AcceptedLanguage? | Gets or sets the language for which the application profiles are requested. (optional) 
            var modules = true;  // bool? | Gets or sets a value indicating whether to include modules in the application profiles. (optional) 
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves all application profiles.
                ApplicationProfileDtoPagedResponse result = apiInstance.GetApplicationProfiles(searchTerm, language, modules, pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ApplicationProfileApi.GetApplicationProfiles: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetApplicationProfilesWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all application profiles.
    ApiResponse<ApplicationProfileDtoPagedResponse> response = apiInstance.GetApplicationProfilesWithHttpInfo(searchTerm, language, modules, pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ApplicationProfileApi.GetApplicationProfilesWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **searchTerm** | **string?** | Gets or sets the search term used to filter application profiles. | [optional]  |
| **language** | [**AcceptedLanguage?**](AcceptedLanguage?.md) | Gets or sets the language for which the application profiles are requested. | [optional]  |
| **modules** | **bool?** | Gets or sets a value indicating whether to include modules in the application profiles. | [optional]  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**ApplicationProfileDtoPagedResponse**](ApplicationProfileDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the application profiles. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getrawapplicationprofile"></a>
# **GetRawApplicationProfile**
> string GetRawApplicationProfile (string profile)

Retrieves the `raw` application profile definition by its URI.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetRawApplicationProfileExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ApplicationProfileApi(config);
            var profile = "profile_example";  // string | The URI of the application profile.

            try
            {
                // Retrieves the `raw` application profile definition by its URI.
                string result = apiInstance.GetRawApplicationProfile(profile);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ApplicationProfileApi.GetRawApplicationProfile: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetRawApplicationProfileWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the `raw` application profile definition by its URI.
    ApiResponse<string> response = apiInstance.GetRawApplicationProfileWithHttpInfo(profile);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ApplicationProfileApi.GetRawApplicationProfileWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **profile** | **string** | The URI of the application profile. |  |

### Return type

**string**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/turtle, application/ld+json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the raw application profile. |  -  |
| **404** | Application profile does not exist. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

