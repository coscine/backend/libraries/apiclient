# Coscine.ApiClient.Core.Model.ProjectResourceMinimalDto
Represents a minimal data transfer object (DTO) for a project resource.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ResourceId** | **Guid** | Identifier of the resource. | [optional] 
**ProjectId** | **Guid** | Identifier of the project associated with the resource. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

