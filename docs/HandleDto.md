# Coscine.ApiClient.Core.Model.HandleDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Pid** | [**PidDto**](PidDto.md) |  | [optional] 
**Values** | [**List&lt;HandleValueDto&gt;**](HandleValueDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

