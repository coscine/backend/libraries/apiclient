# Coscine.ApiClient.Core.Api.ProjectPublicationRequestApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2ProjectsProjectIdPublicationsRequestsOptions**](ProjectPublicationRequestApi.md#apiv2projectsprojectidpublicationsrequestsoptions) | **OPTIONS** /api/v2/projects/{projectId}/publications/requests | Responds with the HTTP methods allowed for the endpoint. |
| [**CreatePublicationRequest**](ProjectPublicationRequestApi.md#createpublicationrequest) | **POST** /api/v2/projects/{projectId}/publications/requests | Creates a new publication request. |
| [**GetPublicationRequest**](ProjectPublicationRequestApi.md#getpublicationrequest) | **GET** /api/v2/projects/{projectId}/publications/requests/{publicationRequestId} | Retrieves a publication request. |

<a id="apiv2projectsprojectidpublicationsrequestsoptions"></a>
# **ApiV2ProjectsProjectIdPublicationsRequestsOptions**
> void ApiV2ProjectsProjectIdPublicationsRequestsOptions (string projectId)

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ProjectsProjectIdPublicationsRequestsOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectPublicationRequestApi(config);
            var projectId = "projectId_example";  // string | 

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ProjectsProjectIdPublicationsRequestsOptions(projectId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectPublicationRequestApi.ApiV2ProjectsProjectIdPublicationsRequestsOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ProjectsProjectIdPublicationsRequestsOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ProjectsProjectIdPublicationsRequestsOptionsWithHttpInfo(projectId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectPublicationRequestApi.ApiV2ProjectsProjectIdPublicationsRequestsOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** |  |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="createpublicationrequest"></a>
# **CreatePublicationRequest**
> ProjectPublicationRequestDtoResponse CreatePublicationRequest (string projectId, PublicationRequestForCreationDto? publicationRequestForCreationDto = null)

Creates a new publication request.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class CreatePublicationRequestExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectPublicationRequestApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var publicationRequestForCreationDto = new PublicationRequestForCreationDto?(); // PublicationRequestForCreationDto? | The publication request data for creation. (optional) 

            try
            {
                // Creates a new publication request.
                ProjectPublicationRequestDtoResponse result = apiInstance.CreatePublicationRequest(projectId, publicationRequestForCreationDto);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectPublicationRequestApi.CreatePublicationRequest: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the CreatePublicationRequestWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Creates a new publication request.
    ApiResponse<ProjectPublicationRequestDtoResponse> response = apiInstance.CreatePublicationRequestWithHttpInfo(projectId, publicationRequestForCreationDto);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectPublicationRequestApi.CreatePublicationRequestWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **publicationRequestForCreationDto** | [**PublicationRequestForCreationDto?**](PublicationRequestForCreationDto?.md) | The publication request data for creation. | [optional]  |

### Return type

[**ProjectPublicationRequestDtoResponse**](ProjectPublicationRequestDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Publication request created. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getpublicationrequest"></a>
# **GetPublicationRequest**
> ProjectPublicationRequestDtoResponse GetPublicationRequest (string projectId, Guid publicationRequestId)

Retrieves a publication request.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetPublicationRequestExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectPublicationRequestApi(config);
            var projectId = "projectId_example";  // string | The ID of the project.
            var publicationRequestId = "publicationRequestId_example";  // Guid | The ID of the publication request.

            try
            {
                // Retrieves a publication request.
                ProjectPublicationRequestDtoResponse result = apiInstance.GetPublicationRequest(projectId, publicationRequestId);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectPublicationRequestApi.GetPublicationRequest: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetPublicationRequestWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves a publication request.
    ApiResponse<ProjectPublicationRequestDtoResponse> response = apiInstance.GetPublicationRequestWithHttpInfo(projectId, publicationRequestId);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectPublicationRequestApi.GetPublicationRequestWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The ID of the project. |  |
| **publicationRequestId** | **Guid** | The ID of the publication request. |  |

### Return type

[**ProjectPublicationRequestDtoResponse**](ProjectPublicationRequestDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the publication request. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Publication request does not exist or has been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

