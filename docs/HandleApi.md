# Coscine.ApiClient.Core.Api.HandleApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2HandlesOptions**](HandleApi.md#apiv2handlesoptions) | **OPTIONS** /api/v2/handles | Responds with the HTTP methods allowed for the endpoint. |
| [**GetHandle**](HandleApi.md#gethandle) | **GET** /api/v2/handles/{prefix}/{suffix} | Retrieves all values of a handle by its PID. |
| [**UpdateHandle**](HandleApi.md#updatehandle) | **PUT** /api/v2/handles/{prefix}/{suffix} | Updates a handle. |

<a id="apiv2handlesoptions"></a>
# **ApiV2HandlesOptions**
> void ApiV2HandlesOptions ()

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2HandlesOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HandleApi(config);

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2HandlesOptions();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling HandleApi.ApiV2HandlesOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2HandlesOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2HandlesOptionsWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling HandleApi.ApiV2HandlesOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="gethandle"></a>
# **GetHandle**
> HandleDtoResponse GetHandle (string prefix, string suffix)

Retrieves all values of a handle by its PID.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetHandleExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HandleApi(config);
            var prefix = "prefix_example";  // string | The prefix of the PID
            var suffix = "suffix_example";  // string | The suffix of the PID

            try
            {
                // Retrieves all values of a handle by its PID.
                HandleDtoResponse result = apiInstance.GetHandle(prefix, suffix);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling HandleApi.GetHandle: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetHandleWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all values of a handle by its PID.
    ApiResponse<HandleDtoResponse> response = apiInstance.GetHandleWithHttpInfo(prefix, suffix);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling HandleApi.GetHandleWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **prefix** | **string** | The prefix of the PID |  |
| **suffix** | **string** | The suffix of the PID |  |

### Return type

[**HandleDtoResponse**](HandleDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the handles. |  -  |
| **403** | User is missing authorization requirements. This endpoint is only available to Coscine Admin users. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="updatehandle"></a>
# **UpdateHandle**
> void UpdateHandle (string prefix, string suffix, HandleForUpdateDto? handleForUpdateDto = null)

Updates a handle.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class UpdateHandleExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new HandleApi(config);
            var prefix = "prefix_example";  // string | The prefix of the PID
            var suffix = "suffix_example";  // string | The suffix of the PID
            var handleForUpdateDto = new HandleForUpdateDto?(); // HandleForUpdateDto? | The handle for updating. (optional) 

            try
            {
                // Updates a handle.
                apiInstance.UpdateHandle(prefix, suffix, handleForUpdateDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling HandleApi.UpdateHandle: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the UpdateHandleWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Updates a handle.
    apiInstance.UpdateHandleWithHttpInfo(prefix, suffix, handleForUpdateDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling HandleApi.UpdateHandleWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **prefix** | **string** | The prefix of the PID |  |
| **suffix** | **string** | The suffix of the PID |  |
| **handleForUpdateDto** | [**HandleForUpdateDto?**](HandleForUpdateDto?.md) | The handle for updating. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Handle updated. |  -  |
| **403** | User is missing authorization requirements. This endpoint is only available to Coscine Admin users. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

