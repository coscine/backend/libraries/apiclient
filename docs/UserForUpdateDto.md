# Coscine.ApiClient.Core.Model.UserForUpdateDto
Represents the data transfer object (DTO) for updating user details.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GivenName** | **string** | The user&#39;s given name. | 
**FamilyName** | **string** | The user&#39;s family name. | 
**Email** | **string** | The user&#39;s email address. | 
**Title** | [**TitleForUserManipulationDto**](TitleForUserManipulationDto.md) |  | [optional] 
**Language** | [**LanguageForUserManipulationDto**](LanguageForUserManipulationDto.md) |  | 
**Organization** | **string** | The user&#39;s organization. | [optional] 
**Disciplines** | [**List&lt;DisciplineForUserManipulationDto&gt;**](DisciplineForUserManipulationDto.md) | The disciplines associated with the user for manipulation. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

