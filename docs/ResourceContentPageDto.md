# Coscine.ApiClient.Core.Model.ResourceContentPageDto
Represents the content page details for a resource.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ReadOnly** | **bool** | Indicates whether the resource is read-only. | [optional] 
**MetadataView** | [**ResourceContentPageMetadataViewDto**](ResourceContentPageMetadataViewDto.md) |  | [optional] 
**EntriesView** | [**ResourceContentPageEntriesViewDto**](ResourceContentPageEntriesViewDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

