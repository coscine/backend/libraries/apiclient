# Coscine.ApiClient.Core.Api.ProjectInvitationApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2ProjectsProjectIdInvitationsOptions**](ProjectInvitationApi.md#apiv2projectsprojectidinvitationsoptions) | **OPTIONS** /api/v2/projects/{projectId}/invitations | Responds with the HTTP methods allowed for the endpoint. |
| [**CreateProjectInvitation**](ProjectInvitationApi.md#createprojectinvitation) | **POST** /api/v2/projects/{projectId}/invitations | Creates a project invitation for a specified project. |
| [**DeleteProjectInvitation**](ProjectInvitationApi.md#deleteprojectinvitation) | **DELETE** /api/v2/projects/{projectId}/invitations/{projectInvitationId} | Deletes a project invitation for a specified project. |
| [**GetProjectInvitation**](ProjectInvitationApi.md#getprojectinvitation) | **GET** /api/v2/projects/{projectId}/invitations/{projectInvitationId} | Retrieves a project invitation for a specified project. |
| [**GetProjectInvitations**](ProjectInvitationApi.md#getprojectinvitations) | **GET** /api/v2/projects/{projectId}/invitations | Retrieves all project invitations for a specified project. |

<a id="apiv2projectsprojectidinvitationsoptions"></a>
# **ApiV2ProjectsProjectIdInvitationsOptions**
> void ApiV2ProjectsProjectIdInvitationsOptions (string projectId)

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ProjectsProjectIdInvitationsOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectInvitationApi(config);
            var projectId = "projectId_example";  // string | 

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ProjectsProjectIdInvitationsOptions(projectId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectInvitationApi.ApiV2ProjectsProjectIdInvitationsOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ProjectsProjectIdInvitationsOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ProjectsProjectIdInvitationsOptionsWithHttpInfo(projectId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectInvitationApi.ApiV2ProjectsProjectIdInvitationsOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** |  |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="createprojectinvitation"></a>
# **CreateProjectInvitation**
> ProjectInvitationDtoResponse CreateProjectInvitation (string projectId, ProjectInvitationForProjectManipulationDto? projectInvitationForProjectManipulationDto = null)

Creates a project invitation for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class CreateProjectInvitationExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectInvitationApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var projectInvitationForProjectManipulationDto = new ProjectInvitationForProjectManipulationDto?(); // ProjectInvitationForProjectManipulationDto? | The project invitation data for creation. (optional) 

            try
            {
                // Creates a project invitation for a specified project.
                ProjectInvitationDtoResponse result = apiInstance.CreateProjectInvitation(projectId, projectInvitationForProjectManipulationDto);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectInvitationApi.CreateProjectInvitation: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the CreateProjectInvitationWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Creates a project invitation for a specified project.
    ApiResponse<ProjectInvitationDtoResponse> response = apiInstance.CreateProjectInvitationWithHttpInfo(projectId, projectInvitationForProjectManipulationDto);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectInvitationApi.CreateProjectInvitationWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **projectInvitationForProjectManipulationDto** | [**ProjectInvitationForProjectManipulationDto?**](ProjectInvitationForProjectManipulationDto?.md) | The project invitation data for creation. | [optional]  |

### Return type

[**ProjectInvitationDtoResponse**](ProjectInvitationDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Project invitation created. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="deleteprojectinvitation"></a>
# **DeleteProjectInvitation**
> void DeleteProjectInvitation (string projectId, Guid projectInvitationId)

Deletes a project invitation for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class DeleteProjectInvitationExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectInvitationApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var projectInvitationId = "projectInvitationId_example";  // Guid | The ID of the project invitation to delete.

            try
            {
                // Deletes a project invitation for a specified project.
                apiInstance.DeleteProjectInvitation(projectId, projectInvitationId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectInvitationApi.DeleteProjectInvitation: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the DeleteProjectInvitationWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Deletes a project invitation for a specified project.
    apiInstance.DeleteProjectInvitationWithHttpInfo(projectId, projectInvitationId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectInvitationApi.DeleteProjectInvitationWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **projectInvitationId** | **Guid** | The ID of the project invitation to delete. |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Project invitation deleted. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getprojectinvitation"></a>
# **GetProjectInvitation**
> ProjectInvitationDtoResponse GetProjectInvitation (string projectId, Guid projectInvitationId)

Retrieves a project invitation for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetProjectInvitationExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectInvitationApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var projectInvitationId = "projectInvitationId_example";  // Guid | The ID of the project invitation to retrieve.

            try
            {
                // Retrieves a project invitation for a specified project.
                ProjectInvitationDtoResponse result = apiInstance.GetProjectInvitation(projectId, projectInvitationId);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectInvitationApi.GetProjectInvitation: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetProjectInvitationWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves a project invitation for a specified project.
    ApiResponse<ProjectInvitationDtoResponse> response = apiInstance.GetProjectInvitationWithHttpInfo(projectId, projectInvitationId);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectInvitationApi.GetProjectInvitationWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **projectInvitationId** | **Guid** | The ID of the project invitation to retrieve. |  |

### Return type

[**ProjectInvitationDtoResponse**](ProjectInvitationDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the project invitation. |  -  |
| **404** | Project invitation does not exist. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getprojectinvitations"></a>
# **GetProjectInvitations**
> ProjectInvitationDtoPagedResponse GetProjectInvitations (string projectId, int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Retrieves all project invitations for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetProjectInvitationsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectInvitationApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves all project invitations for a specified project.
                ProjectInvitationDtoPagedResponse result = apiInstance.GetProjectInvitations(projectId, pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectInvitationApi.GetProjectInvitations: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetProjectInvitationsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all project invitations for a specified project.
    ApiResponse<ProjectInvitationDtoPagedResponse> response = apiInstance.GetProjectInvitationsWithHttpInfo(projectId, pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectInvitationApi.GetProjectInvitationsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**ProjectInvitationDtoPagedResponse**](ProjectInvitationDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the project invitations. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

