# Coscine.ApiClient.Core.Model.ProjectInvitationDto
Represents a data transfer object (DTO) for project invitations.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Unique identifier for the invitation. | [optional] 
**ExpirationDate** | **DateTime** | Expiration date of the invitation. | [optional] 
**Email** | **string** | Email associated with the invitation. | [optional] 
**Issuer** | [**PublicUserDto**](PublicUserDto.md) |  | [optional] 
**Project** | [**ProjectMinimalDto**](ProjectMinimalDto.md) |  | [optional] 
**Role** | [**RoleMinimalDto**](RoleMinimalDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

