# Coscine.ApiClient.Core.Model.ProjectForUpdateDto
Data transfer object (DTO) representing the update of a project.  Inherits from the base class Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ProjectForManipulationDto.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | Gets or initializes the name of the project. | 
**Description** | **string** | Gets or initializes the description of the project. | 
**StartDate** | **DateTime** | Gets or initializes the start date of the project. | [optional] 
**EndDate** | **DateTime** | Gets or initializes the end date of the project. | [optional] 
**Keywords** | **List&lt;string&gt;** | Gets or initializes the keywords associated with the project. | [optional] 
**DisplayName** | **string** | Gets or initializes the display name of the project. | [optional] 
**PrincipleInvestigators** | **string** | Gets or initializes the principal investigators associated with the project. | [optional] 
**GrantId** | **string** | Gets or initializes the grant ID associated with the project. | [optional] 
**Visibility** | [**VisibilityForProjectManipulationDto**](VisibilityForProjectManipulationDto.md) |  | 
**Disciplines** | [**List&lt;DisciplineForProjectManipulationDto&gt;**](DisciplineForProjectManipulationDto.md) | Gets or initializes the disciplines associated with the project. | 
**Organizations** | [**List&lt;OrganizationForProjectManipulationDto&gt;**](OrganizationForProjectManipulationDto.md) | Gets or initializes the organizations associated with the project. | 
**Slug** | **string** | Gets or initializes the slug associated with the project for identification purposes. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

