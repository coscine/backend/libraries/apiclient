# Coscine.ApiClient.Core.Api.PidApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2PidsOptions**](PidApi.md#apiv2pidsoptions) | **OPTIONS** /api/v2/pids | Responds with the HTTP methods allowed for the endpoint. |
| [**GetPid**](PidApi.md#getpid) | **GET** /api/v2/pids/{prefix}/{suffix} | Retrieves the Persistent Identifier (PID) for a given prefix and suffix. |
| [**GetPids**](PidApi.md#getpids) | **GET** /api/v2/pids | Retrieves all PIDs. |
| [**SendRequestToOwner**](PidApi.md#sendrequesttoowner) | **POST** /api/v2/pids/{prefix}/{suffix}/requests | Sends an inquiry to the owner of the given PID. |

<a id="apiv2pidsoptions"></a>
# **ApiV2PidsOptions**
> void ApiV2PidsOptions ()

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2PidsOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new PidApi(config);

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2PidsOptions();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling PidApi.ApiV2PidsOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2PidsOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2PidsOptionsWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PidApi.ApiV2PidsOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getpid"></a>
# **GetPid**
> PidDtoResponse GetPid (string prefix, Guid suffix)

Retrieves the Persistent Identifier (PID) for a given prefix and suffix.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetPidExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new PidApi(config);
            var prefix = "prefix_example";  // string | The PID prefix. Limited to the values provided by the API.
            var suffix = "suffix_example";  // Guid | The PID suffix of a project or a resource, represented as a GUID.

            try
            {
                // Retrieves the Persistent Identifier (PID) for a given prefix and suffix.
                PidDtoResponse result = apiInstance.GetPid(prefix, suffix);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling PidApi.GetPid: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetPidWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the Persistent Identifier (PID) for a given prefix and suffix.
    ApiResponse<PidDtoResponse> response = apiInstance.GetPidWithHttpInfo(prefix, suffix);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PidApi.GetPidWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **prefix** | **string** | The PID prefix. Limited to the values provided by the API. |  |
| **suffix** | **Guid** | The PID suffix of a project or a resource, represented as a GUID. |  |

### Return type

[**PidDtoResponse**](PidDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful response with the PID |  -  |
| **404** | The specified PID does not exist |  -  |
| **410** | The PID is no longer valid (e.g., resource deleted) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getpids"></a>
# **GetPids**
> PidDtoPagedResponse GetPids (bool? includeProjects = null, bool? includeResources = null, bool? includeDeleted = null, int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Retrieves all PIDs.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetPidsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new PidApi(config);
            var includeProjects = true;  // bool? | Gets or sets a value indicating whether to include projects when retrieving pid information. (optional) 
            var includeResources = true;  // bool? | Gets or sets a value indicating whether to include resources when retrieving pid information. (optional) 
            var includeDeleted = true;  // bool? | Gets or sets a value indicating whether to include deleted pid information. (optional) 
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves all PIDs.
                PidDtoPagedResponse result = apiInstance.GetPids(includeProjects, includeResources, includeDeleted, pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling PidApi.GetPids: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetPidsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all PIDs.
    ApiResponse<PidDtoPagedResponse> response = apiInstance.GetPidsWithHttpInfo(includeProjects, includeResources, includeDeleted, pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PidApi.GetPidsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **includeProjects** | **bool?** | Gets or sets a value indicating whether to include projects when retrieving pid information. | [optional]  |
| **includeResources** | **bool?** | Gets or sets a value indicating whether to include resources when retrieving pid information. | [optional]  |
| **includeDeleted** | **bool?** | Gets or sets a value indicating whether to include deleted pid information. | [optional]  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**PidDtoPagedResponse**](PidDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the PIDs. |  -  |
| **403** | User is missing authorization requirements. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="sendrequesttoowner"></a>
# **SendRequestToOwner**
> void SendRequestToOwner (string prefix, Guid suffix, PidRequestDto? pidRequestDto = null)

Sends an inquiry to the owner of the given PID.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class SendRequestToOwnerExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new PidApi(config);
            var prefix = "prefix_example";  // string | The PID prefix of a project or a resource to validate
            var suffix = "suffix_example";  // Guid | The PID body of a project or a resource to validate
            var pidRequestDto = new PidRequestDto?(); // PidRequestDto? | The data transfer object containing the inquiry details. (optional) 

            try
            {
                // Sends an inquiry to the owner of the given PID.
                apiInstance.SendRequestToOwner(prefix, suffix, pidRequestDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling PidApi.SendRequestToOwner: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the SendRequestToOwnerWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Sends an inquiry to the owner of the given PID.
    apiInstance.SendRequestToOwnerWithHttpInfo(prefix, suffix, pidRequestDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling PidApi.SendRequestToOwnerWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **prefix** | **string** | The PID prefix of a project or a resource to validate |  |
| **suffix** | **Guid** | The PID body of a project or a resource to validate |  |
| **pidRequestDto** | [**PidRequestDto?**](PidRequestDto?.md) | The data transfer object containing the inquiry details. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Email sent successfully |  -  |
| **400** | PID has a bad format |  -  |
| **404** | PID does not exist or is invalid |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

