# Coscine.ApiClient.Core.Model.GitlabBranchDto
Represents a GitLab branch data transfer object (DTO).

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | The name of the GitLab branch. | 
**Merged** | **bool** | Indicates if the branch is merged. | 
**Protected** | **bool** | Indicates if the branch is protected. | 
**Default** | **bool** | Indicates if the branch is set as default. | 
**DevelopersCanPush** | **bool** | Indicates if developers can push changes to the branch. | 
**DevelopersCanMerge** | **bool** | Indicates if developers can merge changes into the branch. | 
**CanPush** | **bool** | Indicates if the user can push changes to the branch. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

