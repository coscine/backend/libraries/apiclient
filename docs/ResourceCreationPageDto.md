# Coscine.ApiClient.Core.Model.ResourceCreationPageDto
Represents the structure of resource type-specific components for steps in the resource creation page.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Components** | **List&lt;List&lt;string&gt;&gt;** | List of Lists containing all the resource type specific components for the steps in the resource creation page. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

