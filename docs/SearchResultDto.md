# Coscine.ApiClient.Core.Model.SearchResultDto
Represents a Data Transfer Object (DTO) for search results.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Uri** | **string** | The URI associated with the search result. | 
**Type** | **SearchCategoryType** |  | 
**Source** | **Object** | The dynamic source data for the search result. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

