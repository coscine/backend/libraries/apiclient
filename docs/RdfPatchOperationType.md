# Coscine.ApiClient.Core.Model.RdfPatchOperationType
Represents the types of operations that can be performed in an RDF Patch document.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

