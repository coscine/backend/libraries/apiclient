# Coscine.ApiClient.Core.Model.VariantDto
Represents the variants of this specific metadata tree.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GraphName** | **string** | Name of the graph. | [optional] 
**Similarity** | **double** | Similarity value 0-1 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

