# Coscine.ApiClient.Core.Model.RdfPatchOperationDto
Represents a single operation in an RDF Patch document.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**OperationType** | **RdfPatchOperationType** |  | [optional] 
**Changes** | [**RdfDefinitionForManipulationDto**](RdfDefinitionForManipulationDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

