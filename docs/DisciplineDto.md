# Coscine.ApiClient.Core.Model.DisciplineDto
Represents the data transfer object for a discipline.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | The unique identifier for the discipline. | [optional] 
**Uri** | **string** | The URI associated with the discipline. | [optional] 
**DisplayNameDe** | **string** | The display name of the discipline in German. | [optional] 
**DisplayNameEn** | **string** | The display name of the discipline in English. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

