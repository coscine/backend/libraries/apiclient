# Coscine.ApiClient.Core.Model.UserOrganizationDto
Represents a Data Transfer Object (DTO) for user-related organization information, inheriting from OrganizationDto.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Uri** | **string** | The ROR (Research Organization Registry) ID of the organization. | 
**DisplayName** | **string** | The display name of the organization. | 
**Email** | **string** | The email address of the organization. | [optional] 
**PublicationAdvisoryService** | [**PublicationAdvisoryServiceDto**](PublicationAdvisoryServiceDto.md) |  | [optional] 
**ReadOnly** | **bool** | Determines if the organization&#39;s details can be modified. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

