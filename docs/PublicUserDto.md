# Coscine.ApiClient.Core.Model.PublicUserDto
Represents a public user data transfer object (DTO).

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** | Unique identifier of the user. | 
**DisplayName** | **string** | Display name of the user. | 
**GivenName** | **string** | Given name of the user. | 
**FamilyName** | **string** | Family name of the user. | 
**Email** | **string** | Email address of the user. | 
**Title** | [**TitleDto**](TitleDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

