# Coscine.ApiClient.Core.Model.HandleValueForUpdateDto
Data transfer object (DTO) representing updating a handle.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | The data type of the handle. | 
**ParsedData** | **Object** | The parsed data of the handle. | 
**Idx** | **int** | The id of the handle. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

