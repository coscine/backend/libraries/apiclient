# Coscine.ApiClient.Core.Model.GitLabOptionsDto
Represents the data transfer object (DTO) for GitLab options.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ProjectId** | **int** | The project ID associated with GitLab. | [optional] 
**RepoUrl** | **string** | The repository URL for GitLab. | [optional] 
**AccessToken** | **string** | The access token for GitLab. | [optional] 
**Branch** | **string** | The branch for GitLab. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

