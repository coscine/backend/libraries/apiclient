# Coscine.ApiClient.Core.Model.ProjectOrganizationDto
Represents a data transfer object (DTO) for an organization in the context of a project.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Uri** | **string** | The ROR (Research Organization Registry) ID of the organization. | 
**DisplayName** | **string** | The display name of the organization. | 
**Email** | **string** | The email address of the organization. | [optional] 
**PublicationAdvisoryService** | [**PublicationAdvisoryServiceDto**](PublicationAdvisoryServiceDto.md) |  | [optional] 
**Responsible** | **bool** | Determines if the organization is Responsible for a given project. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

