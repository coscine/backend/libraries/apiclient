# Coscine.ApiClient.Core.Model.Pagination
Represents pagination information for a collection of items.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CurrentPage** | **int** | Gets or sets the current page number. | [optional] 
**TotalPages** | **int** | Gets or sets the total number of pages. | [optional] 
**PageSize** | **int** | Gets or sets the number of items per page. | [optional] 
**TotalCount** | **long** | Gets or sets the total count of items across all pages. | [optional] 
**HasPrevious** | **bool** | Gets a value indicating whether there is a previous page. | [optional] [readonly] 
**HasNext** | **bool** | Gets a value indicating whether there is a next page. | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

