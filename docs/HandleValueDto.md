# Coscine.ApiClient.Core.Model.HandleValueDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Idx** | **int** |  | [optional] 
**Type** | **string** |  | [optional] 
**ParsedData** | **Object** |  | [optional] 
**Data** | **string** |  | [optional] 
**Timestamp** | **DateTime** |  | [optional] 
**TtlType** | **int** |  | [optional] 
**Ttl** | **int** |  | [optional] 
**Refs** | **List&lt;Object&gt;** |  | [optional] 
**Privs** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

