# Coscine.ApiClient.Core.Model.MetadataTreeForCreationDto
Data transfer object (DTO) representing the creation of a metadata tree.  Extends the base class Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.MetadataTreeForManipulationDto.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Path** | **string** | Gets or initializes the path of the metadata tree. | 
**Definition** | [**RdfDefinitionForManipulationDto**](RdfDefinitionForManipulationDto.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

