# Coscine.ApiClient.Core.Model.MetadataTreeExtractedDto
Represents a data transfer object (DTO) for extracted metadata within a tree structure, extending the base TreeDto.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Path** | **string** | The path of the tree item. | [optional] 
**Type** | **TreeDataType** |  | [optional] 
**MetadataId** | **string** | Gets the identifier of the metadata extraction graph. | [optional] 
**RawDataId** | **string** | Gets the identifier of the raw data extraction graph. | [optional] 
**Definition** | [**RdfDefinitionDto**](RdfDefinitionDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

