# Coscine.ApiClient.Core.Model.ApplicationProfileForResourceCreationDto
Data transfer object (DTO) representing the creation of an application profile for a resource.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Uri** | **string** | Gets or initializes the URI of the resource for the application profile. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

