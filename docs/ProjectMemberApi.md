# Coscine.ApiClient.Core.Api.ProjectMemberApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**AddMembership**](ProjectMemberApi.md#addmembership) | **POST** /api/v2/projects/{projectId}/members | Creates a project membership for a specified project. |
| [**ApiV2ProjectsProjectIdMembersOptions**](ProjectMemberApi.md#apiv2projectsprojectidmembersoptions) | **OPTIONS** /api/v2/projects/{projectId}/members | Responds with the HTTP methods allowed for the endpoint. |
| [**DeleteMembership**](ProjectMemberApi.md#deletemembership) | **DELETE** /api/v2/projects/{projectId}/members/{membershipId} | Deletes a project membership for a specified project. |
| [**GetMembership**](ProjectMemberApi.md#getmembership) | **GET** /api/v2/projects/{projectId}/members/{membershipId} | Retrieves a project membership for a specified project. |
| [**GetMemberships**](ProjectMemberApi.md#getmemberships) | **GET** /api/v2/projects/{projectId}/members | Retrieves all project memberships for a specified project. |
| [**UpdateMembership**](ProjectMemberApi.md#updatemembership) | **PUT** /api/v2/projects/{projectId}/members/{membershipId} | Updates a project membership for a specified project. |

<a id="addmembership"></a>
# **AddMembership**
> ProjectRoleDtoResponse AddMembership (string projectId, ProjectRoleForProjectCreationDto? projectRoleForProjectCreationDto = null)

Creates a project membership for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class AddMembershipExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectMemberApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var projectRoleForProjectCreationDto = new ProjectRoleForProjectCreationDto?(); // ProjectRoleForProjectCreationDto? | The project membership data for creation. (optional) 

            try
            {
                // Creates a project membership for a specified project.
                ProjectRoleDtoResponse result = apiInstance.AddMembership(projectId, projectRoleForProjectCreationDto);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectMemberApi.AddMembership: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the AddMembershipWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Creates a project membership for a specified project.
    ApiResponse<ProjectRoleDtoResponse> response = apiInstance.AddMembershipWithHttpInfo(projectId, projectRoleForProjectCreationDto);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectMemberApi.AddMembershipWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **projectRoleForProjectCreationDto** | [**ProjectRoleForProjectCreationDto?**](ProjectRoleForProjectCreationDto?.md) | The project membership data for creation. | [optional]  |

### Return type

[**ProjectRoleDtoResponse**](ProjectRoleDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Project membership created. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="apiv2projectsprojectidmembersoptions"></a>
# **ApiV2ProjectsProjectIdMembersOptions**
> void ApiV2ProjectsProjectIdMembersOptions (string projectId)

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ProjectsProjectIdMembersOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectMemberApi(config);
            var projectId = "projectId_example";  // string | 

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ProjectsProjectIdMembersOptions(projectId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectMemberApi.ApiV2ProjectsProjectIdMembersOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ProjectsProjectIdMembersOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ProjectsProjectIdMembersOptionsWithHttpInfo(projectId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectMemberApi.ApiV2ProjectsProjectIdMembersOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** |  |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="deletemembership"></a>
# **DeleteMembership**
> void DeleteMembership (string projectId, Guid membershipId)

Deletes a project membership for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class DeleteMembershipExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectMemberApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var membershipId = "membershipId_example";  // Guid | The ID of the project membership to delete.

            try
            {
                // Deletes a project membership for a specified project.
                apiInstance.DeleteMembership(projectId, membershipId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectMemberApi.DeleteMembership: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the DeleteMembershipWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Deletes a project membership for a specified project.
    apiInstance.DeleteMembershipWithHttpInfo(projectId, membershipId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectMemberApi.DeleteMembershipWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **membershipId** | **Guid** | The ID of the project membership to delete. |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Project membership deleted. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getmembership"></a>
# **GetMembership**
> ProjectRoleDtoResponse GetMembership (string projectId, Guid membershipId)

Retrieves a project membership for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetMembershipExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectMemberApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var membershipId = "membershipId_example";  // Guid | The ID of the project membership to retrieve.

            try
            {
                // Retrieves a project membership for a specified project.
                ProjectRoleDtoResponse result = apiInstance.GetMembership(projectId, membershipId);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectMemberApi.GetMembership: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetMembershipWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves a project membership for a specified project.
    ApiResponse<ProjectRoleDtoResponse> response = apiInstance.GetMembershipWithHttpInfo(projectId, membershipId);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectMemberApi.GetMembershipWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **membershipId** | **Guid** | The ID of the project membership to retrieve. |  |

### Return type

[**ProjectRoleDtoResponse**](ProjectRoleDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the project membership. |  -  |
| **404** | Project membership does not exist. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getmemberships"></a>
# **GetMemberships**
> ProjectRoleDtoPagedResponse GetMemberships (string projectId, int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Retrieves all project memberships for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetMembershipsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectMemberApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves all project memberships for a specified project.
                ProjectRoleDtoPagedResponse result = apiInstance.GetMemberships(projectId, pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectMemberApi.GetMemberships: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetMembershipsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all project memberships for a specified project.
    ApiResponse<ProjectRoleDtoPagedResponse> response = apiInstance.GetMembershipsWithHttpInfo(projectId, pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectMemberApi.GetMembershipsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**ProjectRoleDtoPagedResponse**](ProjectRoleDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the project memberships. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="updatemembership"></a>
# **UpdateMembership**
> void UpdateMembership (string projectId, Guid membershipId, ProjectRoleForProjectManipulationDto? projectRoleForProjectManipulationDto = null)

Updates a project membership for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class UpdateMembershipExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectMemberApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var membershipId = "membershipId_example";  // Guid | The ID of the project membership to update.
            var projectRoleForProjectManipulationDto = new ProjectRoleForProjectManipulationDto?(); // ProjectRoleForProjectManipulationDto? | The updated project membership data. (optional) 

            try
            {
                // Updates a project membership for a specified project.
                apiInstance.UpdateMembership(projectId, membershipId, projectRoleForProjectManipulationDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectMemberApi.UpdateMembership: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the UpdateMembershipWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Updates a project membership for a specified project.
    apiInstance.UpdateMembershipWithHttpInfo(projectId, membershipId, projectRoleForProjectManipulationDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectMemberApi.UpdateMembershipWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **membershipId** | **Guid** | The ID of the project membership to update. |  |
| **projectRoleForProjectManipulationDto** | [**ProjectRoleForProjectManipulationDto?**](ProjectRoleForProjectManipulationDto?.md) | The updated project membership data. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Project membership updated. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

