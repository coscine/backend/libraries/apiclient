# Coscine.ApiClient.Core.Model.ApplicationProfileDto
Represents an application profile data transfer object.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Uri** | **string** | The URI associated with the application profile. | 
**DisplayName** | **string** | The display name for the application profile. | [optional] 
**Description** | **string** | The description of the application profile. | [optional] 
**Definition** | [**RdfDefinitionDto**](RdfDefinitionDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

