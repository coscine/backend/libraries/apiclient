# Coscine.ApiClient.Core.Model.HandleDtoResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | [**HandleDto**](HandleDto.md) |  | [optional] 
**IsSuccess** | **bool** |  | [optional] [readonly] 
**StatusCode** | **int?** |  | [optional] 
**TraceId** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

