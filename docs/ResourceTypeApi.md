# Coscine.ApiClient.Core.Api.ResourceTypeApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2ResourceTypesOptions**](ResourceTypeApi.md#apiv2resourcetypesoptions) | **OPTIONS** /api/v2/resource-types | Responds with the HTTP methods allowed for the endpoint. |
| [**GetAllResourceTypesInformation**](ResourceTypeApi.md#getallresourcetypesinformation) | **GET** /api/v2/resource-types/types | Retrieves the entire global resource types information. |
| [**GetResourceTypeInformation**](ResourceTypeApi.md#getresourcetypeinformation) | **GET** /api/v2/resource-types/types/{resourceTypeId} | Retrieves the resource type information for a specific resource type. |

<a id="apiv2resourcetypesoptions"></a>
# **ApiV2ResourceTypesOptions**
> void ApiV2ResourceTypesOptions ()

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ResourceTypesOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ResourceTypeApi(config);

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ResourceTypesOptions();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ResourceTypeApi.ApiV2ResourceTypesOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ResourceTypesOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ResourceTypesOptionsWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ResourceTypeApi.ApiV2ResourceTypesOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getallresourcetypesinformation"></a>
# **GetAllResourceTypesInformation**
> ResourceTypeInformationDtoIEnumerableResponse GetAllResourceTypesInformation ()

Retrieves the entire global resource types information.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetAllResourceTypesInformationExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ResourceTypeApi(config);

            try
            {
                // Retrieves the entire global resource types information.
                ResourceTypeInformationDtoIEnumerableResponse result = apiInstance.GetAllResourceTypesInformation();
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ResourceTypeApi.GetAllResourceTypesInformation: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetAllResourceTypesInformationWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the entire global resource types information.
    ApiResponse<ResourceTypeInformationDtoIEnumerableResponse> response = apiInstance.GetAllResourceTypesInformationWithHttpInfo();
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ResourceTypeApi.GetAllResourceTypesInformationWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

[**ResourceTypeInformationDtoIEnumerableResponse**](ResourceTypeInformationDtoIEnumerableResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the entire global resource types information. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getresourcetypeinformation"></a>
# **GetResourceTypeInformation**
> ResourceTypeInformationDtoResponse GetResourceTypeInformation (Guid resourceTypeId)

Retrieves the resource type information for a specific resource type.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetResourceTypeInformationExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ResourceTypeApi(config);
            var resourceTypeId = "resourceTypeId_example";  // Guid | The ID of the resource type to retrieve.

            try
            {
                // Retrieves the resource type information for a specific resource type.
                ResourceTypeInformationDtoResponse result = apiInstance.GetResourceTypeInformation(resourceTypeId);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ResourceTypeApi.GetResourceTypeInformation: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetResourceTypeInformationWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves the resource type information for a specific resource type.
    ApiResponse<ResourceTypeInformationDtoResponse> response = apiInstance.GetResourceTypeInformationWithHttpInfo(resourceTypeId);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ResourceTypeApi.GetResourceTypeInformationWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **resourceTypeId** | **Guid** | The ID of the resource type to retrieve. |  |

### Return type

[**ResourceTypeInformationDtoResponse**](ResourceTypeInformationDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the resource type information. |  -  |
| **404** | Resource type does not exist. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

