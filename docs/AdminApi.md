# Coscine.ApiClient.Core.Api.AdminApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2AdminOptions**](AdminApi.md#apiv2adminoptions) | **OPTIONS** /api/v2/admin | Responds with the HTTP methods allowed for the endpoint. |
| [**GetAllActivityLogs**](AdminApi.md#getallactivitylogs) | **GET** /api/v2/admin/activity-logs | Retrieves all activity logs. |
| [**GetAllProjects**](AdminApi.md#getallprojects) | **GET** /api/v2/admin/projects | Retrieves all projects. |
| [**GetAllResources**](AdminApi.md#getallresources) | **GET** /api/v2/admin/resources | Retrieves all resources. |
| [**GetAllUsers**](AdminApi.md#getallusers) | **GET** /api/v2/admin/users | Retrieves all users. |
| [**GetDeployedGraphs**](AdminApi.md#getdeployedgraphs) | **GET** /api/v2/admin/graphs | Gets all deployed graphs. |
| [**GetMetadataGraph**](AdminApi.md#getmetadatagraph) | **GET** /api/v2/admin/metadata/{graph} | Gets a metadata graph. |
| [**PatchMetadata**](AdminApi.md#patchmetadata) | **PATCH** /api/v2/admin/metadata/{graph} | Patches a metadata graph. |
| [**UpdateMetadataGraph**](AdminApi.md#updatemetadatagraph) | **PUT** /api/v2/admin/metadata/{graph} | Updates a metadata graph. |

<a id="apiv2adminoptions"></a>
# **ApiV2AdminOptions**
> void ApiV2AdminOptions ()

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2AdminOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AdminApi(config);

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2AdminOptions();
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling AdminApi.ApiV2AdminOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2AdminOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2AdminOptionsWithHttpInfo();
}
catch (ApiException e)
{
    Debug.Print("Exception when calling AdminApi.ApiV2AdminOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters
This endpoint does not need any parameter.
### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getallactivitylogs"></a>
# **GetAllActivityLogs**
> ActivityLogDtoPagedResponse GetAllActivityLogs (DateTime? activityTimestampBefore = null, DateTime? activityTimestampAfter = null, Guid? guid = null, Guid? userId = null, string? regularExpression = null, CoscineHttpMethod? httpMethod = null, int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Retrieves all activity logs.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetAllActivityLogsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AdminApi(config);
            var activityTimestampBefore = DateTime.Parse("2013-10-20T19:20:30+01:00");  // DateTime? | The activity timestamp date before which activity logs should be retrieved. (optional) 
            var activityTimestampAfter = DateTime.Parse("2013-10-20T19:20:30+01:00");  // DateTime? | The activity timestamp date after which activity logs should be retrieved. (optional) 
            var guid = "guid_example";  // Guid? | The GUID for which activity logs should be retrieved. Can be a project, resource or other ID. (optional) 
            var userId = "userId_example";  // Guid? | The user ID for which activity logs should be retrieved. (optional) 
            var regularExpression = "regularExpression_example";  // string? | The regular expression to filter activity logs' API path by. The regex must be a valid, already escaped string. (optional) 
            var httpMethod = new CoscineHttpMethod?(); // CoscineHttpMethod? | The http method to filter activity logs by. (optional) 
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves all activity logs.
                ActivityLogDtoPagedResponse result = apiInstance.GetAllActivityLogs(activityTimestampBefore, activityTimestampAfter, guid, userId, regularExpression, httpMethod, pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling AdminApi.GetAllActivityLogs: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetAllActivityLogsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all activity logs.
    ApiResponse<ActivityLogDtoPagedResponse> response = apiInstance.GetAllActivityLogsWithHttpInfo(activityTimestampBefore, activityTimestampAfter, guid, userId, regularExpression, httpMethod, pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling AdminApi.GetAllActivityLogsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **activityTimestampBefore** | **DateTime?** | The activity timestamp date before which activity logs should be retrieved. | [optional]  |
| **activityTimestampAfter** | **DateTime?** | The activity timestamp date after which activity logs should be retrieved. | [optional]  |
| **guid** | **Guid?** | The GUID for which activity logs should be retrieved. Can be a project, resource or other ID. | [optional]  |
| **userId** | **Guid?** | The user ID for which activity logs should be retrieved. | [optional]  |
| **regularExpression** | **string?** | The regular expression to filter activity logs&#39; API path by. The regex must be a valid, already escaped string. | [optional]  |
| **httpMethod** | [**CoscineHttpMethod?**](CoscineHttpMethod?.md) | The http method to filter activity logs by. | [optional]  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**ActivityLogDtoPagedResponse**](ActivityLogDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the activity logs. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getallprojects"></a>
# **GetAllProjects**
> ProjectAdminDtoPagedResponse GetAllProjects (bool? topLevel = null, bool? includeDeleted = null, bool? includeQuotas = null, bool? includePublicationRequests = null, int? pageSize = null, int? pageNumber = null, string? orderBy = null)

Retrieves all projects.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetAllProjectsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AdminApi(config);
            var topLevel = true;  // bool? | Gets or sets a value indicating whether to filter top-level projects. (optional) 
            var includeDeleted = true;  // bool? | Gets or sets a value indicating whether to include deleted projects in the results. (optional) 
            var includeQuotas = true;  // bool? | Gets or sets a value indicating whether to include quotas in the results. (optional) 
            var includePublicationRequests = true;  // bool? | Gets or sets a value indicating whether to include project publication requests in the results. (optional) 
            var pageSize = 56;  // int? | Number of items per page. The maximum number of items per page is `250`. (optional) 
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves all projects.
                ProjectAdminDtoPagedResponse result = apiInstance.GetAllProjects(topLevel, includeDeleted, includeQuotas, includePublicationRequests, pageSize, pageNumber, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling AdminApi.GetAllProjects: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetAllProjectsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all projects.
    ApiResponse<ProjectAdminDtoPagedResponse> response = apiInstance.GetAllProjectsWithHttpInfo(topLevel, includeDeleted, includeQuotas, includePublicationRequests, pageSize, pageNumber, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling AdminApi.GetAllProjectsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **topLevel** | **bool?** | Gets or sets a value indicating whether to filter top-level projects. | [optional]  |
| **includeDeleted** | **bool?** | Gets or sets a value indicating whether to include deleted projects in the results. | [optional]  |
| **includeQuotas** | **bool?** | Gets or sets a value indicating whether to include quotas in the results. | [optional]  |
| **includePublicationRequests** | **bool?** | Gets or sets a value indicating whether to include project publication requests in the results. | [optional]  |
| **pageSize** | **int?** | Number of items per page. The maximum number of items per page is &#x60;250&#x60;. | [optional]  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**ProjectAdminDtoPagedResponse**](ProjectAdminDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the projects. |  -  |
| **403** | User is missing authorization requirements. This endpoint is only available to Coscine Admin users. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getallresources"></a>
# **GetAllResources**
> ResourceAdminDtoPagedResponse GetAllResources (bool? includeDeleted = null, bool? includeQuotas = null, int? pageSize = null, int? pageNumber = null, string? orderBy = null)

Retrieves all resources.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetAllResourcesExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AdminApi(config);
            var includeDeleted = true;  // bool? | Value indicating whether to include deleted resources. (optional) 
            var includeQuotas = true;  // bool? | Value indicating whether to include individual resource quotas. (optional) 
            var pageSize = 56;  // int? | Number of items per page. The maximum number of items per page is `250`. (optional) 
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves all resources.
                ResourceAdminDtoPagedResponse result = apiInstance.GetAllResources(includeDeleted, includeQuotas, pageSize, pageNumber, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling AdminApi.GetAllResources: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetAllResourcesWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all resources.
    ApiResponse<ResourceAdminDtoPagedResponse> response = apiInstance.GetAllResourcesWithHttpInfo(includeDeleted, includeQuotas, pageSize, pageNumber, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling AdminApi.GetAllResourcesWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **includeDeleted** | **bool?** | Value indicating whether to include deleted resources. | [optional]  |
| **includeQuotas** | **bool?** | Value indicating whether to include individual resource quotas. | [optional]  |
| **pageSize** | **int?** | Number of items per page. The maximum number of items per page is &#x60;250&#x60;. | [optional]  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**ResourceAdminDtoPagedResponse**](ResourceAdminDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the resources. |  -  |
| **403** | User is missing authorization requirements. This endpoint is only available to Coscine Admin users. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getallusers"></a>
# **GetAllUsers**
> UserDtoPagedResponse GetAllUsers (bool? tosAccepted = null, int? pageSize = null, int? pageNumber = null, string? orderBy = null)

Retrieves all users.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetAllUsersExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AdminApi(config);
            var tosAccepted = true;  // bool? | Gets or sets a value indicating whether the terms of service have been accepted by the user. (optional) 
            var pageSize = 56;  // int? | Number of items per page. The maximum number of items per page is `250`. (optional) 
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves all users.
                UserDtoPagedResponse result = apiInstance.GetAllUsers(tosAccepted, pageSize, pageNumber, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling AdminApi.GetAllUsers: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetAllUsersWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all users.
    ApiResponse<UserDtoPagedResponse> response = apiInstance.GetAllUsersWithHttpInfo(tosAccepted, pageSize, pageNumber, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling AdminApi.GetAllUsersWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **tosAccepted** | **bool?** | Gets or sets a value indicating whether the terms of service have been accepted by the user. | [optional]  |
| **pageSize** | **int?** | Number of items per page. The maximum number of items per page is &#x60;250&#x60;. | [optional]  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**UserDtoPagedResponse**](UserDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the users. |  -  |
| **403** | User is missing authorization requirements. This endpoint is only available to Coscine Admin users. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getdeployedgraphs"></a>
# **GetDeployedGraphs**
> DeployedGraphDtoPagedResponse GetDeployedGraphs (int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Gets all deployed graphs.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetDeployedGraphsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AdminApi(config);
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Gets all deployed graphs.
                DeployedGraphDtoPagedResponse result = apiInstance.GetDeployedGraphs(pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling AdminApi.GetDeployedGraphs: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetDeployedGraphsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Gets all deployed graphs.
    ApiResponse<DeployedGraphDtoPagedResponse> response = apiInstance.GetDeployedGraphsWithHttpInfo(pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling AdminApi.GetDeployedGraphsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**DeployedGraphDtoPagedResponse**](DeployedGraphDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the metadata. |  -  |
| **403** | User is missing authorization requirements. This endpoint is only available to Coscine Admin users. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getmetadatagraph"></a>
# **GetMetadataGraph**
> RdfDefinitionDtoResponse GetMetadataGraph (string graph, RdfFormat? format = null)

Gets a metadata graph.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetMetadataGraphExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AdminApi(config);
            var graph = "graph_example";  // string | The absolute URI of the graph to get.
            var format = new RdfFormat?(); // RdfFormat? | Gets or sets the RDF format. (optional) 

            try
            {
                // Gets a metadata graph.
                RdfDefinitionDtoResponse result = apiInstance.GetMetadataGraph(graph, format);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling AdminApi.GetMetadataGraph: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetMetadataGraphWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Gets a metadata graph.
    ApiResponse<RdfDefinitionDtoResponse> response = apiInstance.GetMetadataGraphWithHttpInfo(graph, format);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling AdminApi.GetMetadataGraphWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **graph** | **string** | The absolute URI of the graph to get. |  |
| **format** | [**RdfFormat?**](RdfFormat?.md) | Gets or sets the RDF format. | [optional]  |

### Return type

[**RdfDefinitionDtoResponse**](RdfDefinitionDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the metadata. |  -  |
| **403** | User is missing authorization requirements. This endpoint is only available to Coscine Admin users. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="patchmetadata"></a>
# **PatchMetadata**
> void PatchMetadata (string graph, RdfPatchDocumentDto? rdfPatchDocumentDto = null)

Patches a metadata graph.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class PatchMetadataExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AdminApi(config);
            var graph = "graph_example";  // string | The absolute URI of the graph to patch.
            var rdfPatchDocumentDto = new RdfPatchDocumentDto?(); // RdfPatchDocumentDto? | The well-formed RDF patch document containing the changes as operations to be applied to the graph. (optional) 

            try
            {
                // Patches a metadata graph.
                apiInstance.PatchMetadata(graph, rdfPatchDocumentDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling AdminApi.PatchMetadata: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the PatchMetadataWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Patches a metadata graph.
    apiInstance.PatchMetadataWithHttpInfo(graph, rdfPatchDocumentDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling AdminApi.PatchMetadataWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **graph** | **string** | The absolute URI of the graph to patch. |  |
| **rdfPatchDocumentDto** | [**RdfPatchDocumentDto?**](RdfPatchDocumentDto?.md) | The well-formed RDF patch document containing the changes as operations to be applied to the graph. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Metadata graph patched |  -  |
| **403** | User is missing authorization requirements. This endpoint is only available to Coscine Admin users. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="updatemetadatagraph"></a>
# **UpdateMetadataGraph**
> void UpdateMetadataGraph (string graph, MetadataUpdateAdminParameters? metadataUpdateAdminParameters = null)

Updates a metadata graph.

<p><strong>Required JWT roles for access:</strong> <code>administrator</code>.</p>

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class UpdateMetadataGraphExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new AdminApi(config);
            var graph = "graph_example";  // string | The absolute URI of the graph to update.
            var metadataUpdateAdminParameters = new MetadataUpdateAdminParameters?(); // MetadataUpdateAdminParameters? | The metadata update admin parameters. (optional) 

            try
            {
                // Updates a metadata graph.
                apiInstance.UpdateMetadataGraph(graph, metadataUpdateAdminParameters);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling AdminApi.UpdateMetadataGraph: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the UpdateMetadataGraphWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Updates a metadata graph.
    apiInstance.UpdateMetadataGraphWithHttpInfo(graph, metadataUpdateAdminParameters);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling AdminApi.UpdateMetadataGraphWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **graph** | **string** | The absolute URI of the graph to update. |  |
| **metadataUpdateAdminParameters** | [**MetadataUpdateAdminParameters?**](MetadataUpdateAdminParameters?.md) | The metadata update admin parameters. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Metadata graph updated |  -  |
| **403** | User is missing authorization requirements. This endpoint is only available to Coscine Admin users. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

