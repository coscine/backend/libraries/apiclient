# Coscine.ApiClient.Core.Model.ProjectRoleForProjectCreationDto
Data transfer object (DTO) representing the creation of a project role within a project.  Inherits from the base class Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ProjectRoleForProjectManipulationDto.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RoleId** | **Guid** | Gets or initializes the identifier of the role associated with the project manipulation. | 
**UserId** | **Guid** | Gets or initializes the identifier of the user associated with the project role creation. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

