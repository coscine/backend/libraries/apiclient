# Coscine.ApiClient.Core.Model.FileActionsDto
Represents a Data Transfer Object (DTO) for a collection of file actions, including download action details.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Download** | [**FileActionDto**](FileActionDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

