# Coscine.ApiClient.Core.Api.ProjectResourceApi

All URIs are relative to *https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine*

| Method | HTTP request | Description |
|--------|--------------|-------------|
| [**ApiV2ProjectsProjectIdResourcesOptions**](ProjectResourceApi.md#apiv2projectsprojectidresourcesoptions) | **OPTIONS** /api/v2/projects/{projectId}/resources | Responds with the HTTP methods allowed for the endpoint. |
| [**CreateResourceForProject**](ProjectResourceApi.md#createresourceforproject) | **POST** /api/v2/projects/{projectId}/resources | Creates a new resource for a specified project. |
| [**DeleteResourceForProject**](ProjectResourceApi.md#deleteresourceforproject) | **DELETE** /api/v2/projects/{projectId}/resources/{resourceId} | Deletes a resource for a specified project. |
| [**GetResourceForProject**](ProjectResourceApi.md#getresourceforproject) | **GET** /api/v2/projects/{projectId}/resources/{resourceId} | Retrieves a resource for a specified project. |
| [**GetResourcesForProject**](ProjectResourceApi.md#getresourcesforproject) | **GET** /api/v2/projects/{projectId}/resources | Retrieves all resources for a specified project. |
| [**UpdateResourceForProject**](ProjectResourceApi.md#updateresourceforproject) | **PUT** /api/v2/projects/{projectId}/resources/{resourceId} | Updates a resource for a specified project. |

<a id="apiv2projectsprojectidresourcesoptions"></a>
# **ApiV2ProjectsProjectIdResourcesOptions**
> void ApiV2ProjectsProjectIdResourcesOptions (string projectId)

Responds with the HTTP methods allowed for the endpoint.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class ApiV2ProjectsProjectIdResourcesOptionsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectResourceApi(config);
            var projectId = "projectId_example";  // string | 

            try
            {
                // Responds with the HTTP methods allowed for the endpoint.
                apiInstance.ApiV2ProjectsProjectIdResourcesOptions(projectId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectResourceApi.ApiV2ProjectsProjectIdResourcesOptions: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the ApiV2ProjectsProjectIdResourcesOptionsWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Responds with the HTTP methods allowed for the endpoint.
    apiInstance.ApiV2ProjectsProjectIdResourcesOptionsWithHttpInfo(projectId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectResourceApi.ApiV2ProjectsProjectIdResourcesOptionsWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** |  |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="createresourceforproject"></a>
# **CreateResourceForProject**
> ResourceDtoResponse CreateResourceForProject (string projectId, ResourceForCreationDto? resourceForCreationDto = null)

Creates a new resource for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class CreateResourceForProjectExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectResourceApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var resourceForCreationDto = new ResourceForCreationDto?(); // ResourceForCreationDto? | The resource data for creation. (optional) 

            try
            {
                // Creates a new resource for a specified project.
                ResourceDtoResponse result = apiInstance.CreateResourceForProject(projectId, resourceForCreationDto);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectResourceApi.CreateResourceForProject: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the CreateResourceForProjectWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Creates a new resource for a specified project.
    ApiResponse<ResourceDtoResponse> response = apiInstance.CreateResourceForProjectWithHttpInfo(projectId, resourceForCreationDto);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectResourceApi.CreateResourceForProjectWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **resourceForCreationDto** | [**ResourceForCreationDto?**](ResourceForCreationDto?.md) | The resource data for creation. | [optional]  |

### Return type

[**ResourceDtoResponse**](ResourceDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **201** | Resource created. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="deleteresourceforproject"></a>
# **DeleteResourceForProject**
> void DeleteResourceForProject (string projectId, Guid resourceId)

Deletes a resource for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class DeleteResourceForProjectExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectResourceApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The ID of the resource to delete.

            try
            {
                // Deletes a resource for a specified project.
                apiInstance.DeleteResourceForProject(projectId, resourceId);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectResourceApi.DeleteResourceForProject: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the DeleteResourceForProjectWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Deletes a resource for a specified project.
    apiInstance.DeleteResourceForProjectWithHttpInfo(projectId, resourceId);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectResourceApi.DeleteResourceForProjectWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **resourceId** | **Guid** | The ID of the resource to delete. |  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Resource deleted. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format or the resource is write-protected due to its archived status. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getresourceforproject"></a>
# **GetResourceForProject**
> ResourceDtoResponse GetResourceForProject (string projectId, Guid resourceId)

Retrieves a resource for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetResourceForProjectExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectResourceApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The ID of the resource to retrieve.

            try
            {
                // Retrieves a resource for a specified project.
                ResourceDtoResponse result = apiInstance.GetResourceForProject(projectId, resourceId);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectResourceApi.GetResourceForProject: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetResourceForProjectWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves a resource for a specified project.
    ApiResponse<ResourceDtoResponse> response = apiInstance.GetResourceForProjectWithHttpInfo(projectId, resourceId);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectResourceApi.GetResourceForProjectWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **resourceId** | **Guid** | The ID of the resource to retrieve. |  |

### Return type

[**ResourceDtoResponse**](ResourceDtoResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the resource. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format. |  -  |
| **404** | Resource does not exist or has been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="getresourcesforproject"></a>
# **GetResourcesForProject**
> ResourceDtoPagedResponse GetResourcesForProject (string projectId, int? pageNumber = null, int? pageSize = null, string? orderBy = null)

Retrieves all resources for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class GetResourcesForProjectExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectResourceApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var pageNumber = 56;  // int? | The desired page number. Should be greater than or equal to 1. Default is 1. (optional) 
            var pageSize = 56;  // int? | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. (optional) 
            var orderBy = "propertyA asc, propertyB desc";  // string? | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \"asc\" or \"desc\" and separate properties by commas. Default is asc. (optional) 

            try
            {
                // Retrieves all resources for a specified project.
                ResourceDtoPagedResponse result = apiInstance.GetResourcesForProject(projectId, pageNumber, pageSize, orderBy);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectResourceApi.GetResourcesForProject: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the GetResourcesForProjectWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Retrieves all resources for a specified project.
    ApiResponse<ResourceDtoPagedResponse> response = apiInstance.GetResourcesForProjectWithHttpInfo(projectId, pageNumber, pageSize, orderBy);
    Debug.Write("Status Code: " + response.StatusCode);
    Debug.Write("Response Headers: " + response.Headers);
    Debug.Write("Response Body: " + response.Data);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectResourceApi.GetResourcesForProjectWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **pageNumber** | **int?** | The desired page number. Should be greater than or equal to 1. Default is 1. | [optional]  |
| **pageSize** | **int?** | The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10. | [optional]  |
| **orderBy** | **string?** | Gets or sets the field name used for ordering the results.  The order is constructed by an order string.  Use the property followed by \&quot;asc\&quot; or \&quot;desc\&quot; and separate properties by commas. Default is asc. | [optional]  |

### Return type

[**ResourceDtoPagedResponse**](ResourceDtoPagedResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Returns the resources. |  -  |
| **403** | User is missing authorization requirements. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a id="updateresourceforproject"></a>
# **UpdateResourceForProject**
> void UpdateResourceForProject (string projectId, Guid resourceId, ResourceForUpdateDto? resourceForUpdateDto = null)

Updates a resource for a specified project.

### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Example
{
    public class UpdateResourceForProjectExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "https://coscine-api-hristov.web.vulcanus.otc.coscine.dev/coscine";
            // Configure API key authorization: Bearer
            config.AddApiKey("Authorization", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("Authorization", "Bearer");

            var apiInstance = new ProjectResourceApi(config);
            var projectId = "projectId_example";  // string | The Id or slug of the project.
            var resourceId = "resourceId_example";  // Guid | The ID of the resource to update.
            var resourceForUpdateDto = new ResourceForUpdateDto?(); // ResourceForUpdateDto? | The updated resource data. (optional) 

            try
            {
                // Updates a resource for a specified project.
                apiInstance.UpdateResourceForProject(projectId, resourceId, resourceForUpdateDto);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling ProjectResourceApi.UpdateResourceForProject: " + e.Message);
                Debug.Print("Status Code: " + e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

#### Using the UpdateResourceForProjectWithHttpInfo variant
This returns an ApiResponse object which contains the response data, status code and headers.

```csharp
try
{
    // Updates a resource for a specified project.
    apiInstance.UpdateResourceForProjectWithHttpInfo(projectId, resourceId, resourceForUpdateDto);
}
catch (ApiException e)
{
    Debug.Print("Exception when calling ProjectResourceApi.UpdateResourceForProjectWithHttpInfo: " + e.Message);
    Debug.Print("Status Code: " + e.ErrorCode);
    Debug.Print(e.StackTrace);
}
```

### Parameters

| Name | Type | Description | Notes |
|------|------|-------------|-------|
| **projectId** | **string** | The Id or slug of the project. |  |
| **resourceId** | **Guid** | The ID of the resource to update. |  |
| **resourceForUpdateDto** | [**ResourceForUpdateDto?**](ResourceForUpdateDto?.md) | The updated resource data. | [optional]  |

### Return type

void (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json, text/json, application/*+json
 - **Accept**: Not defined


### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | Resource updated. |  -  |
| **403** | User is missing authorization requirements. |  -  |
| **400** | Provided input has a bad format or the resource is write-protected due to its archived status. |  -  |
| **404** | Provided input refers to entries that do not exist or have been deleted. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

