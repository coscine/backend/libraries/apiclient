# Coscine.ApiClient.Core.Model.MessageDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** | ID of the message. | 
**Body** | **Dictionary&lt;string, string&gt;** | Message body in multiple languages as key-value pairs.   The key is the ISO 639-1 Alpha-2 two-letter language code, and the value is the message in that language. | [optional] 
**Type** | **MessageType** |  | [optional] 
**Title** | **string** | Title of the message. | [optional] 
**Href** | **string** | URL related to the message. | [optional] 
**StartDate** | **DateTime?** | Start date of the message (UTC). | [optional] 
**EndDate** | **DateTime?** | End date of the message (UTC). | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

