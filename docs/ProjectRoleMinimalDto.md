# Coscine.ApiClient.Core.Model.ProjectRoleMinimalDto
Represents a minimal data transfer object (DTO) for a project role.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ProjectId** | **Guid** | Identifier of the project associated with the role. | [optional] 
**UserId** | **Guid** | Identifier of the user associated with the role. | [optional] 
**RoleId** | **Guid** | Identifier of the role. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

