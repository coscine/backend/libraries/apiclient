# Coscine.ApiClient.Core.Model.ResourceContentPageEntriesViewDto
Represents information about the resource type for the columns within the entries view Vue component.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Columns** | [**ResourceContentPageColumnsDto**](ResourceContentPageColumnsDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

