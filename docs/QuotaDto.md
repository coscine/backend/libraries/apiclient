# Coscine.ApiClient.Core.Model.QuotaDto
Represents a Data Transfer Object (DTO) for quota values.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Value** | **float** | The value of the quota. | 
**Unit** | **QuotaUnit** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

