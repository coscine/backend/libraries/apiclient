# Coscine.ApiClient.Core.Model.ApplicationProfileForCreationDto
Data transfer object (DTO) representing the creation of an application profile.

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** | Gets or initializes the name of the application profile. | 
**Uri** | **string** | Gets or initializes the URI of the application profile. | 
**Definition** | [**RdfDefinitionForManipulationDto**](RdfDefinitionForManipulationDto.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

