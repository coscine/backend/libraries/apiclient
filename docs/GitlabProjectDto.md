# Coscine.ApiClient.Core.Model.GitlabProjectDto
Represents a GitLab project data transfer object (DTO).

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int** | The unique identifier for the GitLab project. | 
**Archived** | **bool** | Indicates if the GitLab project is archived. | 
**Name** | **string** | The name of the GitLab project. | 
**NameWithNamespace** | **string** | The full name of the GitLab project including namespace. | 
**Description** | **string** | A brief description of the GitLab project. | 
**DefaultBranch** | **string** | The default branch of the GitLab project. | 
**Path** | **string** | The path of the GitLab project. | 
**PathWithNamespace** | **string** | The path of the GitLab project including namespace. | 
**LastActivityAt** | **string** | The timestamp of the last activity related to the GitLab project. | 
**CreatedAt** | **string** | The creation timestamp of the GitLab project. | 
**WebUrl** | **string** | The URL to view the GitLab project in a web browser. | 
**HttpUrlToRepo** | **string** | The HTTP URL to access the GitLab project repository. | 
**SshUrlToRepo** | **string** | The SSH URL to access the GitLab project repository. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

