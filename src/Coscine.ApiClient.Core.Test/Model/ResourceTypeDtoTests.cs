/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Coscine.ApiClient.Core.Model;
using Coscine.ApiClient.Core.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Coscine.ApiClient.Core.Test.Model
{
    /// <summary>
    ///  Class for testing ResourceTypeDto
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class ResourceTypeDtoTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for ResourceTypeDto
        //private ResourceTypeDto instance;

        public ResourceTypeDtoTests()
        {
            // TODO uncomment below to create an instance of ResourceTypeDto
            //instance = new ResourceTypeDto();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of ResourceTypeDto
        /// </summary>
        [Fact]
        public void ResourceTypeDtoInstanceTest()
        {
            // TODO uncomment below to test "IsType" ResourceTypeDto
            //Assert.IsType<ResourceTypeDto>(instance);
        }

        /// <summary>
        /// Test the property 'Id'
        /// </summary>
        [Fact]
        public void IdTest()
        {
            // TODO unit test for the property 'Id'
        }

        /// <summary>
        /// Test the property 'GeneralType'
        /// </summary>
        [Fact]
        public void GeneralTypeTest()
        {
            // TODO unit test for the property 'GeneralType'
        }

        /// <summary>
        /// Test the property 'SpecificType'
        /// </summary>
        [Fact]
        public void SpecificTypeTest()
        {
            // TODO unit test for the property 'SpecificType'
        }

        /// <summary>
        /// Test the property 'Options'
        /// </summary>
        [Fact]
        public void OptionsTest()
        {
            // TODO unit test for the property 'Options'
        }
    }
}
