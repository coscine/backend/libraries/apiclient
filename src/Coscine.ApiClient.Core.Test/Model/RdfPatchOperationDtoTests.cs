/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Coscine.ApiClient.Core.Model;
using Coscine.ApiClient.Core.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Coscine.ApiClient.Core.Test.Model
{
    /// <summary>
    ///  Class for testing RdfPatchOperationDto
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class RdfPatchOperationDtoTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for RdfPatchOperationDto
        //private RdfPatchOperationDto instance;

        public RdfPatchOperationDtoTests()
        {
            // TODO uncomment below to create an instance of RdfPatchOperationDto
            //instance = new RdfPatchOperationDto();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of RdfPatchOperationDto
        /// </summary>
        [Fact]
        public void RdfPatchOperationDtoInstanceTest()
        {
            // TODO uncomment below to test "IsType" RdfPatchOperationDto
            //Assert.IsType<RdfPatchOperationDto>(instance);
        }

        /// <summary>
        /// Test the property 'OperationType'
        /// </summary>
        [Fact]
        public void OperationTypeTest()
        {
            // TODO unit test for the property 'OperationType'
        }

        /// <summary>
        /// Test the property 'Changes'
        /// </summary>
        [Fact]
        public void ChangesTest()
        {
            // TODO unit test for the property 'Changes'
        }
    }
}
