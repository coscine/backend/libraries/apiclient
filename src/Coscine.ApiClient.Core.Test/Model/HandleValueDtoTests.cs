/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Coscine.ApiClient.Core.Model;
using Coscine.ApiClient.Core.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Coscine.ApiClient.Core.Test.Model
{
    /// <summary>
    ///  Class for testing HandleValueDto
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class HandleValueDtoTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for HandleValueDto
        //private HandleValueDto instance;

        public HandleValueDtoTests()
        {
            // TODO uncomment below to create an instance of HandleValueDto
            //instance = new HandleValueDto();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of HandleValueDto
        /// </summary>
        [Fact]
        public void HandleValueDtoInstanceTest()
        {
            // TODO uncomment below to test "IsType" HandleValueDto
            //Assert.IsType<HandleValueDto>(instance);
        }

        /// <summary>
        /// Test the property 'Idx'
        /// </summary>
        [Fact]
        public void IdxTest()
        {
            // TODO unit test for the property 'Idx'
        }

        /// <summary>
        /// Test the property 'Type'
        /// </summary>
        [Fact]
        public void TypeTest()
        {
            // TODO unit test for the property 'Type'
        }

        /// <summary>
        /// Test the property 'ParsedData'
        /// </summary>
        [Fact]
        public void ParsedDataTest()
        {
            // TODO unit test for the property 'ParsedData'
        }

        /// <summary>
        /// Test the property 'Data'
        /// </summary>
        [Fact]
        public void DataTest()
        {
            // TODO unit test for the property 'Data'
        }

        /// <summary>
        /// Test the property 'Timestamp'
        /// </summary>
        [Fact]
        public void TimestampTest()
        {
            // TODO unit test for the property 'Timestamp'
        }

        /// <summary>
        /// Test the property 'TtlType'
        /// </summary>
        [Fact]
        public void TtlTypeTest()
        {
            // TODO unit test for the property 'TtlType'
        }

        /// <summary>
        /// Test the property 'Ttl'
        /// </summary>
        [Fact]
        public void TtlTest()
        {
            // TODO unit test for the property 'Ttl'
        }

        /// <summary>
        /// Test the property 'Refs'
        /// </summary>
        [Fact]
        public void RefsTest()
        {
            // TODO unit test for the property 'Refs'
        }

        /// <summary>
        /// Test the property 'Privs'
        /// </summary>
        [Fact]
        public void PrivsTest()
        {
            // TODO unit test for the property 'Privs'
        }
    }
}
