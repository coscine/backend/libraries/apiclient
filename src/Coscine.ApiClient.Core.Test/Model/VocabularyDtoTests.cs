/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Coscine.ApiClient.Core.Model;
using Coscine.ApiClient.Core.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Coscine.ApiClient.Core.Test.Model
{
    /// <summary>
    ///  Class for testing VocabularyDto
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class VocabularyDtoTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for VocabularyDto
        //private VocabularyDto instance;

        public VocabularyDtoTests()
        {
            // TODO uncomment below to create an instance of VocabularyDto
            //instance = new VocabularyDto();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of VocabularyDto
        /// </summary>
        [Fact]
        public void VocabularyDtoInstanceTest()
        {
            // TODO uncomment below to test "IsType" VocabularyDto
            //Assert.IsType<VocabularyDto>(instance);
        }

        /// <summary>
        /// Test the property 'GraphUri'
        /// </summary>
        [Fact]
        public void GraphUriTest()
        {
            // TODO unit test for the property 'GraphUri'
        }

        /// <summary>
        /// Test the property 'ClassUri'
        /// </summary>
        [Fact]
        public void ClassUriTest()
        {
            // TODO unit test for the property 'ClassUri'
        }

        /// <summary>
        /// Test the property 'DisplayName'
        /// </summary>
        [Fact]
        public void DisplayNameTest()
        {
            // TODO unit test for the property 'DisplayName'
        }

        /// <summary>
        /// Test the property 'Description'
        /// </summary>
        [Fact]
        public void DescriptionTest()
        {
            // TODO unit test for the property 'Description'
        }
    }
}
