/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Coscine.ApiClient.Core.Model;
using Coscine.ApiClient.Core.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Coscine.ApiClient.Core.Test.Model
{
    /// <summary>
    ///  Class for testing ResourceMinimalDto
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class ResourceMinimalDtoTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for ResourceMinimalDto
        //private ResourceMinimalDto instance;

        public ResourceMinimalDtoTests()
        {
            // TODO uncomment below to create an instance of ResourceMinimalDto
            //instance = new ResourceMinimalDto();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of ResourceMinimalDto
        /// </summary>
        [Fact]
        public void ResourceMinimalDtoInstanceTest()
        {
            // TODO uncomment below to test "IsType" ResourceMinimalDto
            //Assert.IsType<ResourceMinimalDto>(instance);
        }

        /// <summary>
        /// Test the property 'Id'
        /// </summary>
        [Fact]
        public void IdTest()
        {
            // TODO unit test for the property 'Id'
        }
    }
}
