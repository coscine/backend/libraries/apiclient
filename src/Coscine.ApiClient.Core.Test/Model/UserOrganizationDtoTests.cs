/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Coscine.ApiClient.Core.Model;
using Coscine.ApiClient.Core.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Coscine.ApiClient.Core.Test.Model
{
    /// <summary>
    ///  Class for testing UserOrganizationDto
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class UserOrganizationDtoTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for UserOrganizationDto
        //private UserOrganizationDto instance;

        public UserOrganizationDtoTests()
        {
            // TODO uncomment below to create an instance of UserOrganizationDto
            //instance = new UserOrganizationDto();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of UserOrganizationDto
        /// </summary>
        [Fact]
        public void UserOrganizationDtoInstanceTest()
        {
            // TODO uncomment below to test "IsType" UserOrganizationDto
            //Assert.IsType<UserOrganizationDto>(instance);
        }

        /// <summary>
        /// Test the property 'Uri'
        /// </summary>
        [Fact]
        public void UriTest()
        {
            // TODO unit test for the property 'Uri'
        }

        /// <summary>
        /// Test the property 'DisplayName'
        /// </summary>
        [Fact]
        public void DisplayNameTest()
        {
            // TODO unit test for the property 'DisplayName'
        }

        /// <summary>
        /// Test the property 'Email'
        /// </summary>
        [Fact]
        public void EmailTest()
        {
            // TODO unit test for the property 'Email'
        }

        /// <summary>
        /// Test the property 'PublicationAdvisoryService'
        /// </summary>
        [Fact]
        public void PublicationAdvisoryServiceTest()
        {
            // TODO unit test for the property 'PublicationAdvisoryService'
        }

        /// <summary>
        /// Test the property 'ReadOnly'
        /// </summary>
        [Fact]
        public void ReadOnlyTest()
        {
            // TODO unit test for the property 'ReadOnly'
        }
    }
}
