/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Coscine.ApiClient.Core.Model;
using Coscine.ApiClient.Core.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Coscine.ApiClient.Core.Test.Model
{
    /// <summary>
    ///  Class for testing PublicationRequestForCreationDto
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class PublicationRequestForCreationDtoTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for PublicationRequestForCreationDto
        //private PublicationRequestForCreationDto instance;

        public PublicationRequestForCreationDtoTests()
        {
            // TODO uncomment below to create an instance of PublicationRequestForCreationDto
            //instance = new PublicationRequestForCreationDto();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of PublicationRequestForCreationDto
        /// </summary>
        [Fact]
        public void PublicationRequestForCreationDtoInstanceTest()
        {
            // TODO uncomment below to test "IsType" PublicationRequestForCreationDto
            //Assert.IsType<PublicationRequestForCreationDto>(instance);
        }

        /// <summary>
        /// Test the property 'PublicationServiceRorId'
        /// </summary>
        [Fact]
        public void PublicationServiceRorIdTest()
        {
            // TODO unit test for the property 'PublicationServiceRorId'
        }

        /// <summary>
        /// Test the property 'ResourceIds'
        /// </summary>
        [Fact]
        public void ResourceIdsTest()
        {
            // TODO unit test for the property 'ResourceIds'
        }

        /// <summary>
        /// Test the property 'Message'
        /// </summary>
        [Fact]
        public void MessageTest()
        {
            // TODO unit test for the property 'Message'
        }
    }
}
