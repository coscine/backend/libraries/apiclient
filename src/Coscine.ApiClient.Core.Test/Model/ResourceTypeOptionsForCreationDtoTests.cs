/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Coscine.ApiClient.Core.Model;
using Coscine.ApiClient.Core.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Coscine.ApiClient.Core.Test.Model
{
    /// <summary>
    ///  Class for testing ResourceTypeOptionsForCreationDto
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class ResourceTypeOptionsForCreationDtoTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for ResourceTypeOptionsForCreationDto
        //private ResourceTypeOptionsForCreationDto instance;

        public ResourceTypeOptionsForCreationDtoTests()
        {
            // TODO uncomment below to create an instance of ResourceTypeOptionsForCreationDto
            //instance = new ResourceTypeOptionsForCreationDto();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of ResourceTypeOptionsForCreationDto
        /// </summary>
        [Fact]
        public void ResourceTypeOptionsForCreationDtoInstanceTest()
        {
            // TODO uncomment below to test "IsType" ResourceTypeOptionsForCreationDto
            //Assert.IsType<ResourceTypeOptionsForCreationDto>(instance);
        }

        /// <summary>
        /// Test the property 'LinkedResourceTypeOptions'
        /// </summary>
        [Fact]
        public void LinkedResourceTypeOptionsTest()
        {
            // TODO unit test for the property 'LinkedResourceTypeOptions'
        }

        /// <summary>
        /// Test the property 'GitlabResourceTypeOptions'
        /// </summary>
        [Fact]
        public void GitlabResourceTypeOptionsTest()
        {
            // TODO unit test for the property 'GitlabResourceTypeOptions'
        }

        /// <summary>
        /// Test the property 'RdsResourceTypeOptions'
        /// </summary>
        [Fact]
        public void RdsResourceTypeOptionsTest()
        {
            // TODO unit test for the property 'RdsResourceTypeOptions'
        }

        /// <summary>
        /// Test the property 'RdsS3ResourceTypeOptions'
        /// </summary>
        [Fact]
        public void RdsS3ResourceTypeOptionsTest()
        {
            // TODO unit test for the property 'RdsS3ResourceTypeOptions'
        }

        /// <summary>
        /// Test the property 'RdsS3WormResourceTypeOptions'
        /// </summary>
        [Fact]
        public void RdsS3WormResourceTypeOptionsTest()
        {
            // TODO unit test for the property 'RdsS3WormResourceTypeOptions'
        }
    }
}
