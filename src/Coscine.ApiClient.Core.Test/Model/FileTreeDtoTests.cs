/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Coscine.ApiClient.Core.Model;
using Coscine.ApiClient.Core.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Coscine.ApiClient.Core.Test.Model
{
    /// <summary>
    ///  Class for testing FileTreeDto
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class FileTreeDtoTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for FileTreeDto
        //private FileTreeDto instance;

        public FileTreeDtoTests()
        {
            // TODO uncomment below to create an instance of FileTreeDto
            //instance = new FileTreeDto();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of FileTreeDto
        /// </summary>
        [Fact]
        public void FileTreeDtoInstanceTest()
        {
            // TODO uncomment below to test "IsType" FileTreeDto
            //Assert.IsType<FileTreeDto>(instance);
        }

        /// <summary>
        /// Test the property 'Path'
        /// </summary>
        [Fact]
        public void PathTest()
        {
            // TODO unit test for the property 'Path'
        }

        /// <summary>
        /// Test the property 'Type'
        /// </summary>
        [Fact]
        public void TypeTest()
        {
            // TODO unit test for the property 'Type'
        }

        /// <summary>
        /// Test the property 'Directory'
        /// </summary>
        [Fact]
        public void DirectoryTest()
        {
            // TODO unit test for the property 'Directory'
        }

        /// <summary>
        /// Test the property 'Name'
        /// </summary>
        [Fact]
        public void NameTest()
        {
            // TODO unit test for the property 'Name'
        }

        /// <summary>
        /// Test the property 'Extension'
        /// </summary>
        [Fact]
        public void ExtensionTest()
        {
            // TODO unit test for the property 'Extension'
        }

        /// <summary>
        /// Test the property 'Size'
        /// </summary>
        [Fact]
        public void SizeTest()
        {
            // TODO unit test for the property 'Size'
        }

        /// <summary>
        /// Test the property 'CreationDate'
        /// </summary>
        [Fact]
        public void CreationDateTest()
        {
            // TODO unit test for the property 'CreationDate'
        }

        /// <summary>
        /// Test the property 'ChangeDate'
        /// </summary>
        [Fact]
        public void ChangeDateTest()
        {
            // TODO unit test for the property 'ChangeDate'
        }

        /// <summary>
        /// Test the property 'Actions'
        /// </summary>
        [Fact]
        public void ActionsTest()
        {
            // TODO unit test for the property 'Actions'
        }

        /// <summary>
        /// Test the property 'Hidden'
        /// </summary>
        [Fact]
        public void HiddenTest()
        {
            // TODO unit test for the property 'Hidden'
        }
    }
}
