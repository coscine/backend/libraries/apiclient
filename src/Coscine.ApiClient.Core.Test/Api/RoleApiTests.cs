/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using Xunit;

using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Api;
// uncomment below to import models
//using Coscine.ApiClient.Core.Model;

namespace Coscine.ApiClient.Core.Test.Api
{
    /// <summary>
    ///  Class for testing RoleApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    public class RoleApiTests : IDisposable
    {
        private RoleApi instance;

        public RoleApiTests()
        {
            instance = new RoleApi();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of RoleApi
        /// </summary>
        [Fact]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsType' RoleApi
            //Assert.IsType<RoleApi>(instance);
        }

        /// <summary>
        /// Test ApiV2RolesOptions
        /// </summary>
        [Fact]
        public void ApiV2RolesOptionsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //instance.ApiV2RolesOptions();
        }

        /// <summary>
        /// Test GetRole
        /// </summary>
        [Fact]
        public void GetRoleTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //Guid roleId = null;
            //var response = instance.GetRole(roleId);
            //Assert.IsType<RoleDtoResponse>(response);
        }

        /// <summary>
        /// Test GetRoles
        /// </summary>
        [Fact]
        public void GetRolesTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? pageNumber = null;
            //int? pageSize = null;
            //string? orderBy = null;
            //var response = instance.GetRoles(pageNumber, pageSize, orderBy);
            //Assert.IsType<RoleDtoPagedResponse>(response);
        }
    }
}
