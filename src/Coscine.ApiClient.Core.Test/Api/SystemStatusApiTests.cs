/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using Xunit;

using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Api;
// uncomment below to import models
//using Coscine.ApiClient.Core.Model;

namespace Coscine.ApiClient.Core.Test.Api
{
    /// <summary>
    ///  Class for testing SystemStatusApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    public class SystemStatusApiTests : IDisposable
    {
        private SystemStatusApi instance;

        public SystemStatusApiTests()
        {
            instance = new SystemStatusApi();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of SystemStatusApi
        /// </summary>
        [Fact]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsType' SystemStatusApi
            //Assert.IsType<SystemStatusApi>(instance);
        }

        /// <summary>
        /// Test ApiV2SystemStatusOptions
        /// </summary>
        [Fact]
        public void ApiV2SystemStatusOptionsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //instance.ApiV2SystemStatusOptions();
        }

        /// <summary>
        /// Test GetInternalMessages
        /// </summary>
        [Fact]
        public void GetInternalMessagesTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //DateTime? startDateAfter = null;
            //DateTime? startDateBefore = null;
            //DateTime? endDateAfter = null;
            //DateTime? endDateBefore = null;
            //MessageType? type = null;
            //string? searchTerm = null;
            //int? pageNumber = null;
            //int? pageSize = null;
            //string? orderBy = null;
            //var response = instance.GetInternalMessages(startDateAfter, startDateBefore, endDateAfter, endDateBefore, type, searchTerm, pageNumber, pageSize, orderBy);
            //Assert.IsType<MessageDtoPagedResponse>(response);
        }

        /// <summary>
        /// Test GetNocMessages
        /// </summary>
        [Fact]
        public void GetNocMessagesTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //DateTime? startDateAfter = null;
            //DateTime? startDateBefore = null;
            //DateTime? endDateAfter = null;
            //DateTime? endDateBefore = null;
            //MessageType? type = null;
            //string? searchTerm = null;
            //int? pageNumber = null;
            //int? pageSize = null;
            //string? orderBy = null;
            //var response = instance.GetNocMessages(startDateAfter, startDateBefore, endDateAfter, endDateBefore, type, searchTerm, pageNumber, pageSize, orderBy);
            //Assert.IsType<MessageDtoPagedResponse>(response);
        }
    }
}
