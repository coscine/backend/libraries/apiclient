/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using Xunit;

using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Api;
// uncomment below to import models
//using Coscine.ApiClient.Core.Model;

namespace Coscine.ApiClient.Core.Test.Api
{
    /// <summary>
    ///  Class for testing SelfApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    public class SelfApiTests : IDisposable
    {
        private SelfApi instance;

        public SelfApiTests()
        {
            instance = new SelfApi();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of SelfApi
        /// </summary>
        [Fact]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsType' SelfApi
            //Assert.IsType<SelfApi>(instance);
        }

        /// <summary>
        /// Test AcceptCurrentTos
        /// </summary>
        [Fact]
        public void AcceptCurrentTosTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //UserTermsOfServiceAcceptDto? userTermsOfServiceAcceptDto = null;
            //instance.AcceptCurrentTos(userTermsOfServiceAcceptDto);
        }

        /// <summary>
        /// Test ApiV2SelfOptions
        /// </summary>
        [Fact]
        public void ApiV2SelfOptionsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //instance.ApiV2SelfOptions();
        }

        /// <summary>
        /// Test ConfirmUserEmail
        /// </summary>
        [Fact]
        public void ConfirmUserEmailTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //Guid confirmationToken = null;
            //instance.ConfirmUserEmail(confirmationToken);
        }

        /// <summary>
        /// Test GetCurrentUser
        /// </summary>
        [Fact]
        public void GetCurrentUserTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //var response = instance.GetCurrentUser();
            //Assert.IsType<UserDtoResponse>(response);
        }

        /// <summary>
        /// Test InitiateUserMerge
        /// </summary>
        [Fact]
        public void InitiateUserMergeTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //IdentityProviders identityProvider = null;
            //var response = instance.InitiateUserMerge(identityProvider);
            //Assert.IsType<UserMergeDtoResponse>(response);
        }

        /// <summary>
        /// Test ResolveProjectInvitation
        /// </summary>
        [Fact]
        public void ResolveProjectInvitationTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //ProjectInvitationResolveDto? projectInvitationResolveDto = null;
            //instance.ResolveProjectInvitation(projectInvitationResolveDto);
        }

        /// <summary>
        /// Test UpdateCurrentUser
        /// </summary>
        [Fact]
        public void UpdateCurrentUserTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //UserForUpdateDto? userForUpdateDto = null;
            //instance.UpdateCurrentUser(userForUpdateDto);
        }
    }
}
