/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// Data transfer object (DTO) representing a language for user manipulation.
    /// </summary>
    [DataContract(Name = "LanguageForUserManipulationDto")]
    public partial class LanguageForUserManipulationDto : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageForUserManipulationDto" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected LanguageForUserManipulationDto() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageForUserManipulationDto" /> class.
        /// </summary>
        /// <param name="id">Gets or initializes the identifier of the language. (required).</param>
        public LanguageForUserManipulationDto(Guid id = default(Guid))
        {
            this.Id = id;
        }

        /// <summary>
        /// Gets or initializes the identifier of the language.
        /// </summary>
        /// <value>Gets or initializes the identifier of the language.</value>
        [DataMember(Name = "id", IsRequired = true, EmitDefaultValue = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("class LanguageForUserManipulationDto {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
