/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// Represents a GitLab branch data transfer object (DTO).
    /// </summary>
    [DataContract(Name = "GitlabBranchDto")]
    public partial class GitlabBranchDto : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GitlabBranchDto" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected GitlabBranchDto() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="GitlabBranchDto" /> class.
        /// </summary>
        /// <param name="name">The name of the GitLab branch. (required).</param>
        /// <param name="merged">Indicates if the branch is merged. (required).</param>
        /// <param name="varProtected">Indicates if the branch is protected. (required).</param>
        /// <param name="varDefault">Indicates if the branch is set as default. (required).</param>
        /// <param name="developersCanPush">Indicates if developers can push changes to the branch. (required).</param>
        /// <param name="developersCanMerge">Indicates if developers can merge changes into the branch. (required).</param>
        /// <param name="canPush">Indicates if the user can push changes to the branch. (required).</param>
        public GitlabBranchDto(string name = default(string), bool merged = default(bool), bool varProtected = default(bool), bool varDefault = default(bool), bool developersCanPush = default(bool), bool developersCanMerge = default(bool), bool canPush = default(bool))
        {
            // to ensure "name" is required (not null)
            if (name == null)
            {
                throw new ArgumentNullException("name is a required property for GitlabBranchDto and cannot be null");
            }
            this.Name = name;
            this.Merged = merged;
            this.Protected = varProtected;
            this.Default = varDefault;
            this.DevelopersCanPush = developersCanPush;
            this.DevelopersCanMerge = developersCanMerge;
            this.CanPush = canPush;
        }

        /// <summary>
        /// The name of the GitLab branch.
        /// </summary>
        /// <value>The name of the GitLab branch.</value>
        [DataMember(Name = "name", IsRequired = true, EmitDefaultValue = true)]
        public string Name { get; set; }

        /// <summary>
        /// Indicates if the branch is merged.
        /// </summary>
        /// <value>Indicates if the branch is merged.</value>
        [DataMember(Name = "merged", IsRequired = true, EmitDefaultValue = true)]
        public bool Merged { get; set; }

        /// <summary>
        /// Indicates if the branch is protected.
        /// </summary>
        /// <value>Indicates if the branch is protected.</value>
        [DataMember(Name = "protected", IsRequired = true, EmitDefaultValue = true)]
        public bool Protected { get; set; }

        /// <summary>
        /// Indicates if the branch is set as default.
        /// </summary>
        /// <value>Indicates if the branch is set as default.</value>
        [DataMember(Name = "default", IsRequired = true, EmitDefaultValue = true)]
        public bool Default { get; set; }

        /// <summary>
        /// Indicates if developers can push changes to the branch.
        /// </summary>
        /// <value>Indicates if developers can push changes to the branch.</value>
        [DataMember(Name = "developersCanPush", IsRequired = true, EmitDefaultValue = true)]
        public bool DevelopersCanPush { get; set; }

        /// <summary>
        /// Indicates if developers can merge changes into the branch.
        /// </summary>
        /// <value>Indicates if developers can merge changes into the branch.</value>
        [DataMember(Name = "developersCanMerge", IsRequired = true, EmitDefaultValue = true)]
        public bool DevelopersCanMerge { get; set; }

        /// <summary>
        /// Indicates if the user can push changes to the branch.
        /// </summary>
        /// <value>Indicates if the user can push changes to the branch.</value>
        [DataMember(Name = "canPush", IsRequired = true, EmitDefaultValue = true)]
        public bool CanPush { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("class GitlabBranchDto {\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Merged: ").Append(Merged).Append("\n");
            sb.Append("  Protected: ").Append(Protected).Append("\n");
            sb.Append("  Default: ").Append(Default).Append("\n");
            sb.Append("  DevelopersCanPush: ").Append(DevelopersCanPush).Append("\n");
            sb.Append("  DevelopersCanMerge: ").Append(DevelopersCanMerge).Append("\n");
            sb.Append("  CanPush: ").Append(CanPush).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
