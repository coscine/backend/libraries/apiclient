/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// Represents a data transfer object (DTO) for project information.
    /// </summary>
    [DataContract(Name = "ProjectDto")]
    public partial class ProjectDto : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectDto" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected ProjectDto() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectDto" /> class.
        /// </summary>
        /// <param name="id">Unique identifier for the project. (required).</param>
        /// <param name="pid">Persistent identifier for the project. (required).</param>
        /// <param name="name">Name of the project. (required).</param>
        /// <param name="description">Description of the project. (required).</param>
        /// <param name="startDate">Start date of the project. (required).</param>
        /// <param name="endDate">End date of the project. (required).</param>
        /// <param name="keywords">Collection of keywords associated with the project..</param>
        /// <param name="displayName">Display name of the project..</param>
        /// <param name="principleInvestigators">Principal investigators involved in the project..</param>
        /// <param name="grantId">Grant ID associated with the project..</param>
        /// <param name="visibility">visibility (required).</param>
        /// <param name="disciplines">Disciplines related to the project. (required).</param>
        /// <param name="organizations">Organizations associated with the project. (required).</param>
        /// <param name="slug">Slug for the project. (required).</param>
        /// <param name="creator">creator.</param>
        /// <param name="creationDate">Date of creation of the project..</param>
        /// <param name="subProjects">Collection of sub-projects associated with this project..</param>
        /// <param name="parent">parent.</param>
        public ProjectDto(Guid id = default(Guid), string pid = default(string), string name = default(string), string description = default(string), DateTime startDate = default(DateTime), DateTime endDate = default(DateTime), List<string> keywords = default(List<string>), string displayName = default(string), string principleInvestigators = default(string), string grantId = default(string), VisibilityDto visibility = default(VisibilityDto), List<DisciplineDto> disciplines = default(List<DisciplineDto>), List<ProjectOrganizationDto> organizations = default(List<ProjectOrganizationDto>), string slug = default(string), UserMinimalDto creator = default(UserMinimalDto), DateTime? creationDate = default(DateTime?), List<ProjectDto> subProjects = default(List<ProjectDto>), ProjectMinimalDto parent = default(ProjectMinimalDto))
        {
            this.Id = id;
            // to ensure "pid" is required (not null)
            if (pid == null)
            {
                throw new ArgumentNullException("pid is a required property for ProjectDto and cannot be null");
            }
            this.Pid = pid;
            // to ensure "name" is required (not null)
            if (name == null)
            {
                throw new ArgumentNullException("name is a required property for ProjectDto and cannot be null");
            }
            this.Name = name;
            // to ensure "description" is required (not null)
            if (description == null)
            {
                throw new ArgumentNullException("description is a required property for ProjectDto and cannot be null");
            }
            this.Description = description;
            this.StartDate = startDate;
            this.EndDate = endDate;
            // to ensure "visibility" is required (not null)
            if (visibility == null)
            {
                throw new ArgumentNullException("visibility is a required property for ProjectDto and cannot be null");
            }
            this.Visibility = visibility;
            // to ensure "disciplines" is required (not null)
            if (disciplines == null)
            {
                throw new ArgumentNullException("disciplines is a required property for ProjectDto and cannot be null");
            }
            this.Disciplines = disciplines;
            // to ensure "organizations" is required (not null)
            if (organizations == null)
            {
                throw new ArgumentNullException("organizations is a required property for ProjectDto and cannot be null");
            }
            this.Organizations = organizations;
            // to ensure "slug" is required (not null)
            if (slug == null)
            {
                throw new ArgumentNullException("slug is a required property for ProjectDto and cannot be null");
            }
            this.Slug = slug;
            this.Keywords = keywords;
            this.DisplayName = displayName;
            this.PrincipleInvestigators = principleInvestigators;
            this.GrantId = grantId;
            this.Creator = creator;
            this.CreationDate = creationDate;
            this.SubProjects = subProjects;
            this.Parent = parent;
        }

        /// <summary>
        /// Unique identifier for the project.
        /// </summary>
        /// <value>Unique identifier for the project.</value>
        [DataMember(Name = "id", IsRequired = true, EmitDefaultValue = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// Persistent identifier for the project.
        /// </summary>
        /// <value>Persistent identifier for the project.</value>
        [DataMember(Name = "pid", IsRequired = true, EmitDefaultValue = true)]
        public string Pid { get; set; }

        /// <summary>
        /// Name of the project.
        /// </summary>
        /// <value>Name of the project.</value>
        [DataMember(Name = "name", IsRequired = true, EmitDefaultValue = true)]
        public string Name { get; set; }

        /// <summary>
        /// Description of the project.
        /// </summary>
        /// <value>Description of the project.</value>
        [DataMember(Name = "description", IsRequired = true, EmitDefaultValue = true)]
        public string Description { get; set; }

        /// <summary>
        /// Start date of the project.
        /// </summary>
        /// <value>Start date of the project.</value>
        [DataMember(Name = "startDate", IsRequired = true, EmitDefaultValue = true)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// End date of the project.
        /// </summary>
        /// <value>End date of the project.</value>
        [DataMember(Name = "endDate", IsRequired = true, EmitDefaultValue = true)]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Collection of keywords associated with the project.
        /// </summary>
        /// <value>Collection of keywords associated with the project.</value>
        [DataMember(Name = "keywords", EmitDefaultValue = true)]
        public List<string> Keywords { get; set; }

        /// <summary>
        /// Display name of the project.
        /// </summary>
        /// <value>Display name of the project.</value>
        [DataMember(Name = "displayName", EmitDefaultValue = true)]
        public string DisplayName { get; set; }

        /// <summary>
        /// Principal investigators involved in the project.
        /// </summary>
        /// <value>Principal investigators involved in the project.</value>
        [DataMember(Name = "principleInvestigators", EmitDefaultValue = true)]
        public string PrincipleInvestigators { get; set; }

        /// <summary>
        /// Grant ID associated with the project.
        /// </summary>
        /// <value>Grant ID associated with the project.</value>
        [DataMember(Name = "grantId", EmitDefaultValue = true)]
        public string GrantId { get; set; }

        /// <summary>
        /// Gets or Sets Visibility
        /// </summary>
        [DataMember(Name = "visibility", IsRequired = true, EmitDefaultValue = true)]
        public VisibilityDto Visibility { get; set; }

        /// <summary>
        /// Disciplines related to the project.
        /// </summary>
        /// <value>Disciplines related to the project.</value>
        [DataMember(Name = "disciplines", IsRequired = true, EmitDefaultValue = true)]
        public List<DisciplineDto> Disciplines { get; set; }

        /// <summary>
        /// Organizations associated with the project.
        /// </summary>
        /// <value>Organizations associated with the project.</value>
        [DataMember(Name = "organizations", IsRequired = true, EmitDefaultValue = true)]
        public List<ProjectOrganizationDto> Organizations { get; set; }

        /// <summary>
        /// Slug for the project.
        /// </summary>
        /// <value>Slug for the project.</value>
        [DataMember(Name = "slug", IsRequired = true, EmitDefaultValue = true)]
        public string Slug { get; set; }

        /// <summary>
        /// Gets or Sets Creator
        /// </summary>
        [DataMember(Name = "creator", EmitDefaultValue = false)]
        public UserMinimalDto Creator { get; set; }

        /// <summary>
        /// Date of creation of the project.
        /// </summary>
        /// <value>Date of creation of the project.</value>
        [DataMember(Name = "creationDate", EmitDefaultValue = true)]
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// Collection of sub-projects associated with this project.
        /// </summary>
        /// <value>Collection of sub-projects associated with this project.</value>
        [DataMember(Name = "subProjects", EmitDefaultValue = true)]
        public List<ProjectDto> SubProjects { get; set; }

        /// <summary>
        /// Gets or Sets Parent
        /// </summary>
        [DataMember(Name = "parent", EmitDefaultValue = false)]
        public ProjectMinimalDto Parent { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("class ProjectDto {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Pid: ").Append(Pid).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Description: ").Append(Description).Append("\n");
            sb.Append("  StartDate: ").Append(StartDate).Append("\n");
            sb.Append("  EndDate: ").Append(EndDate).Append("\n");
            sb.Append("  Keywords: ").Append(Keywords).Append("\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  PrincipleInvestigators: ").Append(PrincipleInvestigators).Append("\n");
            sb.Append("  GrantId: ").Append(GrantId).Append("\n");
            sb.Append("  Visibility: ").Append(Visibility).Append("\n");
            sb.Append("  Disciplines: ").Append(Disciplines).Append("\n");
            sb.Append("  Organizations: ").Append(Organizations).Append("\n");
            sb.Append("  Slug: ").Append(Slug).Append("\n");
            sb.Append("  Creator: ").Append(Creator).Append("\n");
            sb.Append("  CreationDate: ").Append(CreationDate).Append("\n");
            sb.Append("  SubProjects: ").Append(SubProjects).Append("\n");
            sb.Append("  Parent: ").Append(Parent).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
