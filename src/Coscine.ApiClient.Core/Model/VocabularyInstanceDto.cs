/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// Represents a Data Transfer Object (DTO) for vocabulary instance details.
    /// </summary>
    [DataContract(Name = "VocabularyInstanceDto")]
    public partial class VocabularyInstanceDto : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VocabularyInstanceDto" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected VocabularyInstanceDto() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="VocabularyInstanceDto" /> class.
        /// </summary>
        /// <param name="graphUri">The URI of the graph containing the vocabulary. (required).</param>
        /// <param name="instanceUri">The URI of the instance..</param>
        /// <param name="typeUri">The URI representing the type of the instance..</param>
        /// <param name="subClassOfUri">The URI of the direct parent class..</param>
        /// <param name="displayName">The display name of the vocabulary instance..</param>
        /// <param name="description">The description of the vocabulary instance..</param>
        public VocabularyInstanceDto(string graphUri = default(string), string instanceUri = default(string), string typeUri = default(string), string subClassOfUri = default(string), string displayName = default(string), string description = default(string))
        {
            // to ensure "graphUri" is required (not null)
            if (graphUri == null)
            {
                throw new ArgumentNullException("graphUri is a required property for VocabularyInstanceDto and cannot be null");
            }
            this.GraphUri = graphUri;
            this.InstanceUri = instanceUri;
            this.TypeUri = typeUri;
            this.SubClassOfUri = subClassOfUri;
            this.DisplayName = displayName;
            this.Description = description;
        }

        /// <summary>
        /// The URI of the graph containing the vocabulary.
        /// </summary>
        /// <value>The URI of the graph containing the vocabulary.</value>
        [DataMember(Name = "graphUri", IsRequired = true, EmitDefaultValue = true)]
        public string GraphUri { get; set; }

        /// <summary>
        /// The URI of the instance.
        /// </summary>
        /// <value>The URI of the instance.</value>
        [DataMember(Name = "instanceUri", EmitDefaultValue = false)]
        public string InstanceUri { get; set; }

        /// <summary>
        /// The URI representing the type of the instance.
        /// </summary>
        /// <value>The URI representing the type of the instance.</value>
        [DataMember(Name = "typeUri", EmitDefaultValue = true)]
        public string TypeUri { get; set; }

        /// <summary>
        /// The URI of the direct parent class.
        /// </summary>
        /// <value>The URI of the direct parent class.</value>
        [DataMember(Name = "subClassOfUri", EmitDefaultValue = true)]
        public string SubClassOfUri { get; set; }

        /// <summary>
        /// The display name of the vocabulary instance.
        /// </summary>
        /// <value>The display name of the vocabulary instance.</value>
        [DataMember(Name = "displayName", EmitDefaultValue = true)]
        public string DisplayName { get; set; }

        /// <summary>
        /// The description of the vocabulary instance.
        /// </summary>
        /// <value>The description of the vocabulary instance.</value>
        [DataMember(Name = "description", EmitDefaultValue = true)]
        public string Description { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("class VocabularyInstanceDto {\n");
            sb.Append("  GraphUri: ").Append(GraphUri).Append("\n");
            sb.Append("  InstanceUri: ").Append(InstanceUri).Append("\n");
            sb.Append("  TypeUri: ").Append(TypeUri).Append("\n");
            sb.Append("  SubClassOfUri: ").Append(SubClassOfUri).Append("\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  Description: ").Append(Description).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
