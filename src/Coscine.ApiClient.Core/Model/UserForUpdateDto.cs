/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// Represents the data transfer object (DTO) for updating user details.
    /// </summary>
    [DataContract(Name = "UserForUpdateDto")]
    public partial class UserForUpdateDto : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserForUpdateDto" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected UserForUpdateDto() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="UserForUpdateDto" /> class.
        /// </summary>
        /// <param name="givenName">The user&#39;s given name. (required).</param>
        /// <param name="familyName">The user&#39;s family name. (required).</param>
        /// <param name="email">The user&#39;s email address. (required).</param>
        /// <param name="title">title.</param>
        /// <param name="language">language (required).</param>
        /// <param name="organization">The user&#39;s organization..</param>
        /// <param name="disciplines">The disciplines associated with the user for manipulation. (required).</param>
        public UserForUpdateDto(string givenName = default(string), string familyName = default(string), string email = default(string), TitleForUserManipulationDto title = default(TitleForUserManipulationDto), LanguageForUserManipulationDto language = default(LanguageForUserManipulationDto), string organization = default(string), List<DisciplineForUserManipulationDto> disciplines = default(List<DisciplineForUserManipulationDto>))
        {
            // to ensure "givenName" is required (not null)
            if (givenName == null)
            {
                throw new ArgumentNullException("givenName is a required property for UserForUpdateDto and cannot be null");
            }
            this.GivenName = givenName;
            // to ensure "familyName" is required (not null)
            if (familyName == null)
            {
                throw new ArgumentNullException("familyName is a required property for UserForUpdateDto and cannot be null");
            }
            this.FamilyName = familyName;
            // to ensure "email" is required (not null)
            if (email == null)
            {
                throw new ArgumentNullException("email is a required property for UserForUpdateDto and cannot be null");
            }
            this.Email = email;
            // to ensure "language" is required (not null)
            if (language == null)
            {
                throw new ArgumentNullException("language is a required property for UserForUpdateDto and cannot be null");
            }
            this.Language = language;
            // to ensure "disciplines" is required (not null)
            if (disciplines == null)
            {
                throw new ArgumentNullException("disciplines is a required property for UserForUpdateDto and cannot be null");
            }
            this.Disciplines = disciplines;
            this.Title = title;
            this.Organization = organization;
        }

        /// <summary>
        /// The user&#39;s given name.
        /// </summary>
        /// <value>The user&#39;s given name.</value>
        [DataMember(Name = "givenName", IsRequired = true, EmitDefaultValue = true)]
        public string GivenName { get; set; }

        /// <summary>
        /// The user&#39;s family name.
        /// </summary>
        /// <value>The user&#39;s family name.</value>
        [DataMember(Name = "familyName", IsRequired = true, EmitDefaultValue = true)]
        public string FamilyName { get; set; }

        /// <summary>
        /// The user&#39;s email address.
        /// </summary>
        /// <value>The user&#39;s email address.</value>
        [DataMember(Name = "email", IsRequired = true, EmitDefaultValue = true)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or Sets Title
        /// </summary>
        [DataMember(Name = "title", EmitDefaultValue = false)]
        public TitleForUserManipulationDto Title { get; set; }

        /// <summary>
        /// Gets or Sets Language
        /// </summary>
        [DataMember(Name = "language", IsRequired = true, EmitDefaultValue = true)]
        public LanguageForUserManipulationDto Language { get; set; }

        /// <summary>
        /// The user&#39;s organization.
        /// </summary>
        /// <value>The user&#39;s organization.</value>
        [DataMember(Name = "organization", EmitDefaultValue = true)]
        public string Organization { get; set; }

        /// <summary>
        /// The disciplines associated with the user for manipulation.
        /// </summary>
        /// <value>The disciplines associated with the user for manipulation.</value>
        [DataMember(Name = "disciplines", IsRequired = true, EmitDefaultValue = true)]
        public List<DisciplineForUserManipulationDto> Disciplines { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("class UserForUpdateDto {\n");
            sb.Append("  GivenName: ").Append(GivenName).Append("\n");
            sb.Append("  FamilyName: ").Append(FamilyName).Append("\n");
            sb.Append("  Email: ").Append(Email).Append("\n");
            sb.Append("  Title: ").Append(Title).Append("\n");
            sb.Append("  Language: ").Append(Language).Append("\n");
            sb.Append("  Organization: ").Append(Organization).Append("\n");
            sb.Append("  Disciplines: ").Append(Disciplines).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            // GivenName (string) minLength
            if (this.GivenName != null && this.GivenName.Length < 1)
            {
                yield return new ValidationResult("Invalid value for GivenName, length must be greater than 1.", new [] { "GivenName" });
            }

            // FamilyName (string) minLength
            if (this.FamilyName != null && this.FamilyName.Length < 1)
            {
                yield return new ValidationResult("Invalid value for FamilyName, length must be greater than 1.", new [] { "FamilyName" });
            }

            // Email (string) minLength
            if (this.Email != null && this.Email.Length < 1)
            {
                yield return new ValidationResult("Invalid value for Email, length must be greater than 1.", new [] { "Email" });
            }

            yield break;
        }
    }

}
