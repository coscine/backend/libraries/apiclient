/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// Specifies the search item type.
    /// </summary>
    /// <value>Specifies the search item type.</value>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SearchCategoryType
    {
        /// <summary>
        /// Enum None for value: None
        /// </summary>
        [EnumMember(Value = "None")]
        None = 1,

        /// <summary>
        /// Enum HttpsPurlOrgCoscineTermsStructureMetadata for value: https://purl.org/coscine/terms/structure#Metadata
        /// </summary>
        [EnumMember(Value = "https://purl.org/coscine/terms/structure#Metadata")]
        HttpsPurlOrgCoscineTermsStructureMetadata = 2,

        /// <summary>
        /// Enum HttpsPurlOrgCoscineTermsStructureProject for value: https://purl.org/coscine/terms/structure#Project
        /// </summary>
        [EnumMember(Value = "https://purl.org/coscine/terms/structure#Project")]
        HttpsPurlOrgCoscineTermsStructureProject = 3,

        /// <summary>
        /// Enum HttpsPurlOrgCoscineTermsStructureResource for value: https://purl.org/coscine/terms/structure#Resource
        /// </summary>
        [EnumMember(Value = "https://purl.org/coscine/terms/structure#Resource")]
        HttpsPurlOrgCoscineTermsStructureResource = 4
    }

}
