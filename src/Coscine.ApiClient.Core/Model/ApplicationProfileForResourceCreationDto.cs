/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// Data transfer object (DTO) representing the creation of an application profile for a resource.
    /// </summary>
    [DataContract(Name = "ApplicationProfileForResourceCreationDto")]
    public partial class ApplicationProfileForResourceCreationDto : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationProfileForResourceCreationDto" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected ApplicationProfileForResourceCreationDto() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationProfileForResourceCreationDto" /> class.
        /// </summary>
        /// <param name="uri">Gets or initializes the URI of the resource for the application profile. (required).</param>
        public ApplicationProfileForResourceCreationDto(string uri = default(string))
        {
            // to ensure "uri" is required (not null)
            if (uri == null)
            {
                throw new ArgumentNullException("uri is a required property for ApplicationProfileForResourceCreationDto and cannot be null");
            }
            this.Uri = uri;
        }

        /// <summary>
        /// Gets or initializes the URI of the resource for the application profile.
        /// </summary>
        /// <value>Gets or initializes the URI of the resource for the application profile.</value>
        [DataMember(Name = "uri", IsRequired = true, EmitDefaultValue = true)]
        public string Uri { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("class ApplicationProfileForResourceCreationDto {\n");
            sb.Append("  Uri: ").Append(Uri).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
