/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// Represents the data transfer object for a discipline.
    /// </summary>
    [DataContract(Name = "DisciplineDto")]
    public partial class DisciplineDto : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisciplineDto" /> class.
        /// </summary>
        /// <param name="id">The unique identifier for the discipline..</param>
        /// <param name="uri">The URI associated with the discipline..</param>
        /// <param name="displayNameDe">The display name of the discipline in German..</param>
        /// <param name="displayNameEn">The display name of the discipline in English..</param>
        public DisciplineDto(Guid id = default(Guid), string uri = default(string), string displayNameDe = default(string), string displayNameEn = default(string))
        {
            this.Id = id;
            this.Uri = uri;
            this.DisplayNameDe = displayNameDe;
            this.DisplayNameEn = displayNameEn;
        }

        /// <summary>
        /// The unique identifier for the discipline.
        /// </summary>
        /// <value>The unique identifier for the discipline.</value>
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public Guid Id { get; set; }

        /// <summary>
        /// The URI associated with the discipline.
        /// </summary>
        /// <value>The URI associated with the discipline.</value>
        [DataMember(Name = "uri", EmitDefaultValue = false)]
        public string Uri { get; set; }

        /// <summary>
        /// The display name of the discipline in German.
        /// </summary>
        /// <value>The display name of the discipline in German.</value>
        [DataMember(Name = "displayNameDe", EmitDefaultValue = false)]
        public string DisplayNameDe { get; set; }

        /// <summary>
        /// The display name of the discipline in English.
        /// </summary>
        /// <value>The display name of the discipline in English.</value>
        [DataMember(Name = "displayNameEn", EmitDefaultValue = false)]
        public string DisplayNameEn { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("class DisciplineDto {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Uri: ").Append(Uri).Append("\n");
            sb.Append("  DisplayNameDe: ").Append(DisplayNameDe).Append("\n");
            sb.Append("  DisplayNameEn: ").Append(DisplayNameEn).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
