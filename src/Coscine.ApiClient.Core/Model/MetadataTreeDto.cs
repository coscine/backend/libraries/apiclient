/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// Represents a data transfer object (DTO) for metadata within a tree structure, extending the base TreeDto.
    /// </summary>
    [DataContract(Name = "MetadataTreeDto")]
    public partial class MetadataTreeDto : IValidatableObject
    {

        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        [DataMember(Name = "type", EmitDefaultValue = false)]
        public TreeDataType? Type { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="MetadataTreeDto" /> class.
        /// </summary>
        /// <param name="path">The path of the tree item..</param>
        /// <param name="type">type.</param>
        /// <param name="id">Gets or sets the id of the specific metadata tree..</param>
        /// <param name="varVersion">Gets or sets the version associated with the metadata..</param>
        /// <param name="availableVersions">Gets or sets the collection of available versions related to the metadata..</param>
        /// <param name="definition">definition.</param>
        /// <param name="extracted">extracted.</param>
        /// <param name="provenance">provenance.</param>
        public MetadataTreeDto(string path = default(string), TreeDataType? type = default(TreeDataType?), string id = default(string), string varVersion = default(string), List<string> availableVersions = default(List<string>), RdfDefinitionDto definition = default(RdfDefinitionDto), MetadataTreeExtractedDto extracted = default(MetadataTreeExtractedDto), ProvenanceDto provenance = default(ProvenanceDto))
        {
            this.Path = path;
            this.Type = type;
            this.Id = id;
            this.VarVersion = varVersion;
            this.AvailableVersions = availableVersions;
            this.Definition = definition;
            this.Extracted = extracted;
            this.Provenance = provenance;
        }

        /// <summary>
        /// The path of the tree item.
        /// </summary>
        /// <value>The path of the tree item.</value>
        [DataMember(Name = "path", EmitDefaultValue = false)]
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the id of the specific metadata tree.
        /// </summary>
        /// <value>Gets or sets the id of the specific metadata tree.</value>
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the version associated with the metadata.
        /// </summary>
        /// <value>Gets or sets the version associated with the metadata.</value>
        [DataMember(Name = "version", EmitDefaultValue = false)]
        public string VarVersion { get; set; }

        /// <summary>
        /// Gets or sets the collection of available versions related to the metadata.
        /// </summary>
        /// <value>Gets or sets the collection of available versions related to the metadata.</value>
        [DataMember(Name = "availableVersions", EmitDefaultValue = false)]
        public List<string> AvailableVersions { get; set; }

        /// <summary>
        /// Gets or Sets Definition
        /// </summary>
        [DataMember(Name = "definition", EmitDefaultValue = false)]
        public RdfDefinitionDto Definition { get; set; }

        /// <summary>
        /// Gets or Sets Extracted
        /// </summary>
        [DataMember(Name = "extracted", EmitDefaultValue = false)]
        public MetadataTreeExtractedDto Extracted { get; set; }

        /// <summary>
        /// Gets or Sets Provenance
        /// </summary>
        [DataMember(Name = "provenance", EmitDefaultValue = false)]
        public ProvenanceDto Provenance { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("class MetadataTreeDto {\n");
            sb.Append("  Path: ").Append(Path).Append("\n");
            sb.Append("  Type: ").Append(Type).Append("\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  VarVersion: ").Append(VarVersion).Append("\n");
            sb.Append("  AvailableVersions: ").Append(AvailableVersions).Append("\n");
            sb.Append("  Definition: ").Append(Definition).Append("\n");
            sb.Append("  Extracted: ").Append(Extracted).Append("\n");
            sb.Append("  Provenance: ").Append(Provenance).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
