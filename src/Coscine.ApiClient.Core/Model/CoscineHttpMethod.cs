/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// An enumeration representing the supported HTTP verbs.
    /// </summary>
    /// <value>An enumeration representing the supported HTTP verbs.</value>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CoscineHttpMethod
    {
        /// <summary>
        /// Enum GET for value: GET
        /// </summary>
        [EnumMember(Value = "GET")]
        GET = 1,

        /// <summary>
        /// Enum HEAD for value: HEAD
        /// </summary>
        [EnumMember(Value = "HEAD")]
        HEAD = 2,

        /// <summary>
        /// Enum POST for value: POST
        /// </summary>
        [EnumMember(Value = "POST")]
        POST = 3,

        /// <summary>
        /// Enum PUT for value: PUT
        /// </summary>
        [EnumMember(Value = "PUT")]
        PUT = 4,

        /// <summary>
        /// Enum DELETE for value: DELETE
        /// </summary>
        [EnumMember(Value = "DELETE")]
        DELETE = 5
    }

}
