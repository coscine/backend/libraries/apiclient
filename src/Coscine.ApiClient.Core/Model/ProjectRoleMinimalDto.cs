/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// Represents a minimal data transfer object (DTO) for a project role.
    /// </summary>
    [DataContract(Name = "ProjectRoleMinimalDto")]
    public partial class ProjectRoleMinimalDto : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectRoleMinimalDto" /> class.
        /// </summary>
        /// <param name="projectId">Identifier of the project associated with the role..</param>
        /// <param name="userId">Identifier of the user associated with the role..</param>
        /// <param name="roleId">Identifier of the role..</param>
        public ProjectRoleMinimalDto(Guid projectId = default(Guid), Guid userId = default(Guid), Guid roleId = default(Guid))
        {
            this.ProjectId = projectId;
            this.UserId = userId;
            this.RoleId = roleId;
        }

        /// <summary>
        /// Identifier of the project associated with the role.
        /// </summary>
        /// <value>Identifier of the project associated with the role.</value>
        [DataMember(Name = "projectId", EmitDefaultValue = false)]
        public Guid ProjectId { get; set; }

        /// <summary>
        /// Identifier of the user associated with the role.
        /// </summary>
        /// <value>Identifier of the user associated with the role.</value>
        [DataMember(Name = "userId", EmitDefaultValue = false)]
        public Guid UserId { get; set; }

        /// <summary>
        /// Identifier of the role.
        /// </summary>
        /// <value>Identifier of the role.</value>
        [DataMember(Name = "roleId", EmitDefaultValue = false)]
        public Guid RoleId { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("class ProjectRoleMinimalDto {\n");
            sb.Append("  ProjectId: ").Append(ProjectId).Append("\n");
            sb.Append("  UserId: ").Append(UserId).Append("\n");
            sb.Append("  RoleId: ").Append(RoleId).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
