/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// Represents an application profile data transfer object.
    /// </summary>
    [DataContract(Name = "ApplicationProfileDto")]
    public partial class ApplicationProfileDto : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationProfileDto" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected ApplicationProfileDto() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationProfileDto" /> class.
        /// </summary>
        /// <param name="uri">The URI associated with the application profile. (required).</param>
        /// <param name="displayName">The display name for the application profile..</param>
        /// <param name="description">The description of the application profile..</param>
        /// <param name="definition">definition.</param>
        public ApplicationProfileDto(string uri = default(string), string displayName = default(string), string description = default(string), RdfDefinitionDto definition = default(RdfDefinitionDto))
        {
            // to ensure "uri" is required (not null)
            if (uri == null)
            {
                throw new ArgumentNullException("uri is a required property for ApplicationProfileDto and cannot be null");
            }
            this.Uri = uri;
            this.DisplayName = displayName;
            this.Description = description;
            this.Definition = definition;
        }

        /// <summary>
        /// The URI associated with the application profile.
        /// </summary>
        /// <value>The URI associated with the application profile.</value>
        [DataMember(Name = "uri", IsRequired = true, EmitDefaultValue = true)]
        public string Uri { get; set; }

        /// <summary>
        /// The display name for the application profile.
        /// </summary>
        /// <value>The display name for the application profile.</value>
        [DataMember(Name = "displayName", EmitDefaultValue = true)]
        public string DisplayName { get; set; }

        /// <summary>
        /// The description of the application profile.
        /// </summary>
        /// <value>The description of the application profile.</value>
        [DataMember(Name = "description", EmitDefaultValue = true)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or Sets Definition
        /// </summary>
        [DataMember(Name = "definition", EmitDefaultValue = false)]
        public RdfDefinitionDto Definition { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("class ApplicationProfileDto {\n");
            sb.Append("  Uri: ").Append(Uri).Append("\n");
            sb.Append("  DisplayName: ").Append(DisplayName).Append("\n");
            sb.Append("  Description: ").Append(Description).Append("\n");
            sb.Append("  Definition: ").Append(Definition).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
