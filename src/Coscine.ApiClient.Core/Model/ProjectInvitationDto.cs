/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = Coscine.ApiClient.Core.Client.OpenAPIDateConverter;

namespace Coscine.ApiClient.Core.Model
{
    /// <summary>
    /// Represents a data transfer object (DTO) for project invitations.
    /// </summary>
    [DataContract(Name = "ProjectInvitationDto")]
    public partial class ProjectInvitationDto : IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectInvitationDto" /> class.
        /// </summary>
        /// <param name="id">Unique identifier for the invitation..</param>
        /// <param name="expirationDate">Expiration date of the invitation..</param>
        /// <param name="email">Email associated with the invitation..</param>
        /// <param name="issuer">issuer.</param>
        /// <param name="project">project.</param>
        /// <param name="role">role.</param>
        public ProjectInvitationDto(Guid id = default(Guid), DateTime expirationDate = default(DateTime), string email = default(string), PublicUserDto issuer = default(PublicUserDto), ProjectMinimalDto project = default(ProjectMinimalDto), RoleMinimalDto role = default(RoleMinimalDto))
        {
            this.Id = id;
            this.ExpirationDate = expirationDate;
            this.Email = email;
            this.Issuer = issuer;
            this.Project = project;
            this.Role = role;
        }

        /// <summary>
        /// Unique identifier for the invitation.
        /// </summary>
        /// <value>Unique identifier for the invitation.</value>
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public Guid Id { get; set; }

        /// <summary>
        /// Expiration date of the invitation.
        /// </summary>
        /// <value>Expiration date of the invitation.</value>
        [DataMember(Name = "expirationDate", EmitDefaultValue = false)]
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Email associated with the invitation.
        /// </summary>
        /// <value>Email associated with the invitation.</value>
        [DataMember(Name = "email", EmitDefaultValue = false)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or Sets Issuer
        /// </summary>
        [DataMember(Name = "issuer", EmitDefaultValue = false)]
        public PublicUserDto Issuer { get; set; }

        /// <summary>
        /// Gets or Sets Project
        /// </summary>
        [DataMember(Name = "project", EmitDefaultValue = false)]
        public ProjectMinimalDto Project { get; set; }

        /// <summary>
        /// Gets or Sets Role
        /// </summary>
        [DataMember(Name = "role", EmitDefaultValue = false)]
        public RoleMinimalDto Role { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("class ProjectInvitationDto {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  ExpirationDate: ").Append(ExpirationDate).Append("\n");
            sb.Append("  Email: ").Append(Email).Append("\n");
            sb.Append("  Issuer: ").Append(Issuer).Append("\n");
            sb.Append("  Project: ").Append(Project).Append("\n");
            sb.Append("  Role: ").Append(Role).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
