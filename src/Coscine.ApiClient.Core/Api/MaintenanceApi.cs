/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Mime;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Coscine.ApiClient.Core.Api
{

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IMaintenanceApiSync : IApiAccessor
    {
        #region Synchronous Operations
        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint.
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns></returns>
        void ApiV2MaintenancesOptions(int operationIndex = 0);

        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of Object(void)</returns>
        ApiResponse<Object> ApiV2MaintenancesOptionsWithHttpInfo(int operationIndex = 0);
        /// <summary>
        /// Retrieves the current maintenance messages.
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>MaintenanceDtoPagedResponse</returns>
        [Obsolete]
        MaintenanceDtoPagedResponse GetCurrentMaintenances(int operationIndex = 0);

        /// <summary>
        /// Retrieves the current maintenance messages.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of MaintenanceDtoPagedResponse</returns>
        [Obsolete]
        ApiResponse<MaintenanceDtoPagedResponse> GetCurrentMaintenancesWithHttpInfo(int operationIndex = 0);
        #endregion Synchronous Operations
    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IMaintenanceApiAsync : IApiAccessor
    {
        #region Asynchronous Operations
        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of void</returns>
        System.Threading.Tasks.Task ApiV2MaintenancesOptionsAsync(int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken));

        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse</returns>
        System.Threading.Tasks.Task<ApiResponse<Object>> ApiV2MaintenancesOptionsWithHttpInfoAsync(int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken));
        /// <summary>
        /// Retrieves the current maintenance messages.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of MaintenanceDtoPagedResponse</returns>
        [Obsolete]
        System.Threading.Tasks.Task<MaintenanceDtoPagedResponse> GetCurrentMaintenancesAsync(int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken));

        /// <summary>
        /// Retrieves the current maintenance messages.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (MaintenanceDtoPagedResponse)</returns>
        [Obsolete]
        System.Threading.Tasks.Task<ApiResponse<MaintenanceDtoPagedResponse>> GetCurrentMaintenancesWithHttpInfoAsync(int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken));
        #endregion Asynchronous Operations
    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IMaintenanceApi : IMaintenanceApiSync, IMaintenanceApiAsync
    {

    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class MaintenanceApi : IMaintenanceApi
    {
        private Coscine.ApiClient.Core.Client.ExceptionFactory _exceptionFactory = (name, response) => null;

        /// <summary>
        /// Initializes a new instance of the <see cref="MaintenanceApi"/> class.
        /// </summary>
        /// <returns></returns>
        public MaintenanceApi() : this((string)null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaintenanceApi"/> class.
        /// </summary>
        /// <returns></returns>
        public MaintenanceApi(string basePath)
        {
            this.Configuration = Coscine.ApiClient.Core.Client.Configuration.MergeConfigurations(
                Coscine.ApiClient.Core.Client.GlobalConfiguration.Instance,
                new Coscine.ApiClient.Core.Client.Configuration { BasePath = basePath }
            );
            this.Client = new Coscine.ApiClient.Core.Client.ApiClient(this.Configuration.BasePath);
            this.AsynchronousClient = new Coscine.ApiClient.Core.Client.ApiClient(this.Configuration.BasePath);
            this.ExceptionFactory = Coscine.ApiClient.Core.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaintenanceApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public MaintenanceApi(Coscine.ApiClient.Core.Client.Configuration configuration)
        {
            if (configuration == null) throw new ArgumentNullException("configuration");

            this.Configuration = Coscine.ApiClient.Core.Client.Configuration.MergeConfigurations(
                Coscine.ApiClient.Core.Client.GlobalConfiguration.Instance,
                configuration
            );
            this.Client = new Coscine.ApiClient.Core.Client.ApiClient(this.Configuration.BasePath);
            this.AsynchronousClient = new Coscine.ApiClient.Core.Client.ApiClient(this.Configuration.BasePath);
            ExceptionFactory = Coscine.ApiClient.Core.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MaintenanceApi"/> class
        /// using a Configuration object and client instance.
        /// </summary>
        /// <param name="client">The client interface for synchronous API access.</param>
        /// <param name="asyncClient">The client interface for asynchronous API access.</param>
        /// <param name="configuration">The configuration object.</param>
        public MaintenanceApi(Coscine.ApiClient.Core.Client.ISynchronousClient client, Coscine.ApiClient.Core.Client.IAsynchronousClient asyncClient, Coscine.ApiClient.Core.Client.IReadableConfiguration configuration)
        {
            if (client == null) throw new ArgumentNullException("client");
            if (asyncClient == null) throw new ArgumentNullException("asyncClient");
            if (configuration == null) throw new ArgumentNullException("configuration");

            this.Client = client;
            this.AsynchronousClient = asyncClient;
            this.Configuration = configuration;
            this.ExceptionFactory = Coscine.ApiClient.Core.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// The client for accessing this underlying API asynchronously.
        /// </summary>
        public Coscine.ApiClient.Core.Client.IAsynchronousClient AsynchronousClient { get; set; }

        /// <summary>
        /// The client for accessing this underlying API synchronously.
        /// </summary>
        public Coscine.ApiClient.Core.Client.ISynchronousClient Client { get; set; }

        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        public string GetBasePath()
        {
            return this.Configuration.BasePath;
        }

        /// <summary>
        /// Gets or sets the configuration object
        /// </summary>
        /// <value>An instance of the Configuration</value>
        public Coscine.ApiClient.Core.Client.IReadableConfiguration Configuration { get; set; }

        /// <summary>
        /// Provides a factory method hook for the creation of exceptions.
        /// </summary>
        public Coscine.ApiClient.Core.Client.ExceptionFactory ExceptionFactory
        {
            get
            {
                if (_exceptionFactory != null && _exceptionFactory.GetInvocationList().Length > 1)
                {
                    throw new InvalidOperationException("Multicast delegate for ExceptionFactory is unsupported.");
                }
                return _exceptionFactory;
            }
            set { _exceptionFactory = value; }
        }

        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns></returns>
        public void ApiV2MaintenancesOptions(int operationIndex = 0)
        {
            ApiV2MaintenancesOptionsWithHttpInfo();
        }

        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of Object(void)</returns>
        public Coscine.ApiClient.Core.Client.ApiResponse<Object> ApiV2MaintenancesOptionsWithHttpInfo(int operationIndex = 0)
        {
            Coscine.ApiClient.Core.Client.RequestOptions localVarRequestOptions = new Coscine.ApiClient.Core.Client.RequestOptions();

            string[] _contentTypes = new string[] {
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
            };

            var localVarContentType = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            var localVarMultipartFormData = localVarContentType == "multipart/form-data";
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }


            localVarRequestOptions.Operation = "MaintenanceApi.ApiV2MaintenancesOptions";
            localVarRequestOptions.OperationIndex = operationIndex;

            // authentication (Bearer) required
            if (!string.IsNullOrEmpty(this.Configuration.GetApiKeyWithPrefix("Authorization")))
            {
                localVarRequestOptions.HeaderParameters.Add("Authorization", this.Configuration.GetApiKeyWithPrefix("Authorization"));
            }

            // make the HTTP request
            var localVarResponse = this.Client.Options<Object>("/api/v2/maintenances", localVarRequestOptions, this.Configuration);
            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("ApiV2MaintenancesOptions", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of void</returns>
        public async System.Threading.Tasks.Task ApiV2MaintenancesOptionsAsync(int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
        {
            await ApiV2MaintenancesOptionsWithHttpInfoAsync(operationIndex, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse</returns>
        public async System.Threading.Tasks.Task<Coscine.ApiClient.Core.Client.ApiResponse<Object>> ApiV2MaintenancesOptionsWithHttpInfoAsync(int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
        {

            Coscine.ApiClient.Core.Client.RequestOptions localVarRequestOptions = new Coscine.ApiClient.Core.Client.RequestOptions();

            string[] _contentTypes = new string[] {
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
            };

            var localVarContentType = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }


            localVarRequestOptions.Operation = "MaintenanceApi.ApiV2MaintenancesOptions";
            localVarRequestOptions.OperationIndex = operationIndex;

            // authentication (Bearer) required
            if (!string.IsNullOrEmpty(this.Configuration.GetApiKeyWithPrefix("Authorization")))
            {
                localVarRequestOptions.HeaderParameters.Add("Authorization", this.Configuration.GetApiKeyWithPrefix("Authorization"));
            }

            // make the HTTP request
            var localVarResponse = await this.AsynchronousClient.OptionsAsync<Object>("/api/v2/maintenances", localVarRequestOptions, this.Configuration, cancellationToken).ConfigureAwait(false);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("ApiV2MaintenancesOptions", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

        /// <summary>
        /// Retrieves the current maintenance messages. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>MaintenanceDtoPagedResponse</returns>
        [Obsolete]
        public MaintenanceDtoPagedResponse GetCurrentMaintenances(int operationIndex = 0)
        {
            Coscine.ApiClient.Core.Client.ApiResponse<MaintenanceDtoPagedResponse> localVarResponse = GetCurrentMaintenancesWithHttpInfo();
            return localVarResponse.Data;
        }

        /// <summary>
        /// Retrieves the current maintenance messages. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of MaintenanceDtoPagedResponse</returns>
        [Obsolete]
        public Coscine.ApiClient.Core.Client.ApiResponse<MaintenanceDtoPagedResponse> GetCurrentMaintenancesWithHttpInfo(int operationIndex = 0)
        {
            Coscine.ApiClient.Core.Client.RequestOptions localVarRequestOptions = new Coscine.ApiClient.Core.Client.RequestOptions();

            string[] _contentTypes = new string[] {
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
                "application/json",
                "text/json"
            };

            var localVarContentType = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            var localVarMultipartFormData = localVarContentType == "multipart/form-data";
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }


            localVarRequestOptions.Operation = "MaintenanceApi.GetCurrentMaintenances";
            localVarRequestOptions.OperationIndex = operationIndex;

            // authentication (Bearer) required
            if (!string.IsNullOrEmpty(this.Configuration.GetApiKeyWithPrefix("Authorization")))
            {
                localVarRequestOptions.HeaderParameters.Add("Authorization", this.Configuration.GetApiKeyWithPrefix("Authorization"));
            }

            // make the HTTP request
            var localVarResponse = this.Client.Get<MaintenanceDtoPagedResponse>("/api/v2/maintenances", localVarRequestOptions, this.Configuration);
            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("GetCurrentMaintenances", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

        /// <summary>
        /// Retrieves the current maintenance messages. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of MaintenanceDtoPagedResponse</returns>
        [Obsolete]
        public async System.Threading.Tasks.Task<MaintenanceDtoPagedResponse> GetCurrentMaintenancesAsync(int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
        {
            Coscine.ApiClient.Core.Client.ApiResponse<MaintenanceDtoPagedResponse> localVarResponse = await GetCurrentMaintenancesWithHttpInfoAsync(operationIndex, cancellationToken).ConfigureAwait(false);
            return localVarResponse.Data;
        }

        /// <summary>
        /// Retrieves the current maintenance messages. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (MaintenanceDtoPagedResponse)</returns>
        [Obsolete]
        public async System.Threading.Tasks.Task<Coscine.ApiClient.Core.Client.ApiResponse<MaintenanceDtoPagedResponse>> GetCurrentMaintenancesWithHttpInfoAsync(int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
        {

            Coscine.ApiClient.Core.Client.RequestOptions localVarRequestOptions = new Coscine.ApiClient.Core.Client.RequestOptions();

            string[] _contentTypes = new string[] {
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
                "application/json",
                "text/json"
            };

            var localVarContentType = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }


            localVarRequestOptions.Operation = "MaintenanceApi.GetCurrentMaintenances";
            localVarRequestOptions.OperationIndex = operationIndex;

            // authentication (Bearer) required
            if (!string.IsNullOrEmpty(this.Configuration.GetApiKeyWithPrefix("Authorization")))
            {
                localVarRequestOptions.HeaderParameters.Add("Authorization", this.Configuration.GetApiKeyWithPrefix("Authorization"));
            }

            // make the HTTP request
            var localVarResponse = await this.AsynchronousClient.GetAsync<MaintenanceDtoPagedResponse>("/api/v2/maintenances", localVarRequestOptions, this.Configuration, cancellationToken).ConfigureAwait(false);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("GetCurrentMaintenances", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

    }
}
