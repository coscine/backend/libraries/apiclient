/*
 * Coscine Web API
 *
 * Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.
 *
 * The version of the OpenAPI document: 2.0
 * Contact: servicedesk@itc.rwth-aachen.de
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Mime;
using Coscine.ApiClient.Core.Client;
using Coscine.ApiClient.Core.Model;

namespace Coscine.ApiClient.Core.Api
{

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IResourceApiSync : IApiAccessor
    {
        #region Synchronous Operations
        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint.
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns></returns>
        void ApiV2ResourcesOptions(int operationIndex = 0);

        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of Object(void)</returns>
        ApiResponse<Object> ApiV2ResourcesOptionsWithHttpInfo(int operationIndex = 0);
        /// <summary>
        /// Retrieves a resource by its ID.
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="resourceId">The ID of the resource to retrieve.</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ResourceDtoResponse</returns>
        ResourceDtoResponse GetResource(Guid resourceId, int operationIndex = 0);

        /// <summary>
        /// Retrieves a resource by its ID.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="resourceId">The ID of the resource to retrieve.</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of ResourceDtoResponse</returns>
        ApiResponse<ResourceDtoResponse> GetResourceWithHttpInfo(Guid resourceId, int operationIndex = 0);
        #endregion Synchronous Operations
    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IResourceApiAsync : IApiAccessor
    {
        #region Asynchronous Operations
        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of void</returns>
        System.Threading.Tasks.Task ApiV2ResourcesOptionsAsync(int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken));

        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse</returns>
        System.Threading.Tasks.Task<ApiResponse<Object>> ApiV2ResourcesOptionsWithHttpInfoAsync(int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken));
        /// <summary>
        /// Retrieves a resource by its ID.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="resourceId">The ID of the resource to retrieve.</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ResourceDtoResponse</returns>
        System.Threading.Tasks.Task<ResourceDtoResponse> GetResourceAsync(Guid resourceId, int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken));

        /// <summary>
        /// Retrieves a resource by its ID.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="resourceId">The ID of the resource to retrieve.</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (ResourceDtoResponse)</returns>
        System.Threading.Tasks.Task<ApiResponse<ResourceDtoResponse>> GetResourceWithHttpInfoAsync(Guid resourceId, int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken));
        #endregion Asynchronous Operations
    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IResourceApi : IResourceApiSync, IResourceApiAsync
    {

    }

    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class ResourceApi : IResourceApi
    {
        private Coscine.ApiClient.Core.Client.ExceptionFactory _exceptionFactory = (name, response) => null;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceApi"/> class.
        /// </summary>
        /// <returns></returns>
        public ResourceApi() : this((string)null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceApi"/> class.
        /// </summary>
        /// <returns></returns>
        public ResourceApi(string basePath)
        {
            this.Configuration = Coscine.ApiClient.Core.Client.Configuration.MergeConfigurations(
                Coscine.ApiClient.Core.Client.GlobalConfiguration.Instance,
                new Coscine.ApiClient.Core.Client.Configuration { BasePath = basePath }
            );
            this.Client = new Coscine.ApiClient.Core.Client.ApiClient(this.Configuration.BasePath);
            this.AsynchronousClient = new Coscine.ApiClient.Core.Client.ApiClient(this.Configuration.BasePath);
            this.ExceptionFactory = Coscine.ApiClient.Core.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public ResourceApi(Coscine.ApiClient.Core.Client.Configuration configuration)
        {
            if (configuration == null) throw new ArgumentNullException("configuration");

            this.Configuration = Coscine.ApiClient.Core.Client.Configuration.MergeConfigurations(
                Coscine.ApiClient.Core.Client.GlobalConfiguration.Instance,
                configuration
            );
            this.Client = new Coscine.ApiClient.Core.Client.ApiClient(this.Configuration.BasePath);
            this.AsynchronousClient = new Coscine.ApiClient.Core.Client.ApiClient(this.Configuration.BasePath);
            ExceptionFactory = Coscine.ApiClient.Core.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceApi"/> class
        /// using a Configuration object and client instance.
        /// </summary>
        /// <param name="client">The client interface for synchronous API access.</param>
        /// <param name="asyncClient">The client interface for asynchronous API access.</param>
        /// <param name="configuration">The configuration object.</param>
        public ResourceApi(Coscine.ApiClient.Core.Client.ISynchronousClient client, Coscine.ApiClient.Core.Client.IAsynchronousClient asyncClient, Coscine.ApiClient.Core.Client.IReadableConfiguration configuration)
        {
            if (client == null) throw new ArgumentNullException("client");
            if (asyncClient == null) throw new ArgumentNullException("asyncClient");
            if (configuration == null) throw new ArgumentNullException("configuration");

            this.Client = client;
            this.AsynchronousClient = asyncClient;
            this.Configuration = configuration;
            this.ExceptionFactory = Coscine.ApiClient.Core.Client.Configuration.DefaultExceptionFactory;
        }

        /// <summary>
        /// The client for accessing this underlying API asynchronously.
        /// </summary>
        public Coscine.ApiClient.Core.Client.IAsynchronousClient AsynchronousClient { get; set; }

        /// <summary>
        /// The client for accessing this underlying API synchronously.
        /// </summary>
        public Coscine.ApiClient.Core.Client.ISynchronousClient Client { get; set; }

        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        public string GetBasePath()
        {
            return this.Configuration.BasePath;
        }

        /// <summary>
        /// Gets or sets the configuration object
        /// </summary>
        /// <value>An instance of the Configuration</value>
        public Coscine.ApiClient.Core.Client.IReadableConfiguration Configuration { get; set; }

        /// <summary>
        /// Provides a factory method hook for the creation of exceptions.
        /// </summary>
        public Coscine.ApiClient.Core.Client.ExceptionFactory ExceptionFactory
        {
            get
            {
                if (_exceptionFactory != null && _exceptionFactory.GetInvocationList().Length > 1)
                {
                    throw new InvalidOperationException("Multicast delegate for ExceptionFactory is unsupported.");
                }
                return _exceptionFactory;
            }
            set { _exceptionFactory = value; }
        }

        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns></returns>
        public void ApiV2ResourcesOptions(int operationIndex = 0)
        {
            ApiV2ResourcesOptionsWithHttpInfo();
        }

        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of Object(void)</returns>
        public Coscine.ApiClient.Core.Client.ApiResponse<Object> ApiV2ResourcesOptionsWithHttpInfo(int operationIndex = 0)
        {
            Coscine.ApiClient.Core.Client.RequestOptions localVarRequestOptions = new Coscine.ApiClient.Core.Client.RequestOptions();

            string[] _contentTypes = new string[] {
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
            };

            var localVarContentType = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            var localVarMultipartFormData = localVarContentType == "multipart/form-data";
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }


            localVarRequestOptions.Operation = "ResourceApi.ApiV2ResourcesOptions";
            localVarRequestOptions.OperationIndex = operationIndex;

            // authentication (Bearer) required
            if (!string.IsNullOrEmpty(this.Configuration.GetApiKeyWithPrefix("Authorization")))
            {
                localVarRequestOptions.HeaderParameters.Add("Authorization", this.Configuration.GetApiKeyWithPrefix("Authorization"));
            }

            // make the HTTP request
            var localVarResponse = this.Client.Options<Object>("/api/v2/resources", localVarRequestOptions, this.Configuration);
            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("ApiV2ResourcesOptions", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of void</returns>
        public async System.Threading.Tasks.Task ApiV2ResourcesOptionsAsync(int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
        {
            await ApiV2ResourcesOptionsWithHttpInfoAsync(operationIndex, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Responds with the HTTP methods allowed for the endpoint. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse</returns>
        public async System.Threading.Tasks.Task<Coscine.ApiClient.Core.Client.ApiResponse<Object>> ApiV2ResourcesOptionsWithHttpInfoAsync(int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
        {

            Coscine.ApiClient.Core.Client.RequestOptions localVarRequestOptions = new Coscine.ApiClient.Core.Client.RequestOptions();

            string[] _contentTypes = new string[] {
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
            };

            var localVarContentType = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }


            localVarRequestOptions.Operation = "ResourceApi.ApiV2ResourcesOptions";
            localVarRequestOptions.OperationIndex = operationIndex;

            // authentication (Bearer) required
            if (!string.IsNullOrEmpty(this.Configuration.GetApiKeyWithPrefix("Authorization")))
            {
                localVarRequestOptions.HeaderParameters.Add("Authorization", this.Configuration.GetApiKeyWithPrefix("Authorization"));
            }

            // make the HTTP request
            var localVarResponse = await this.AsynchronousClient.OptionsAsync<Object>("/api/v2/resources", localVarRequestOptions, this.Configuration, cancellationToken).ConfigureAwait(false);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("ApiV2ResourcesOptions", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

        /// <summary>
        /// Retrieves a resource by its ID. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="resourceId">The ID of the resource to retrieve.</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ResourceDtoResponse</returns>
        public ResourceDtoResponse GetResource(Guid resourceId, int operationIndex = 0)
        {
            Coscine.ApiClient.Core.Client.ApiResponse<ResourceDtoResponse> localVarResponse = GetResourceWithHttpInfo(resourceId);
            return localVarResponse.Data;
        }

        /// <summary>
        /// Retrieves a resource by its ID. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="resourceId">The ID of the resource to retrieve.</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <returns>ApiResponse of ResourceDtoResponse</returns>
        public Coscine.ApiClient.Core.Client.ApiResponse<ResourceDtoResponse> GetResourceWithHttpInfo(Guid resourceId, int operationIndex = 0)
        {
            Coscine.ApiClient.Core.Client.RequestOptions localVarRequestOptions = new Coscine.ApiClient.Core.Client.RequestOptions();

            string[] _contentTypes = new string[] {
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
                "application/json",
                "text/json"
            };

            var localVarContentType = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            var localVarMultipartFormData = localVarContentType == "multipart/form-data";
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }

            localVarRequestOptions.PathParameters.Add("resourceId", Coscine.ApiClient.Core.Client.ClientUtils.ParameterToString(resourceId)); // path parameter

            localVarRequestOptions.Operation = "ResourceApi.GetResource";
            localVarRequestOptions.OperationIndex = operationIndex;

            // authentication (Bearer) required
            if (!string.IsNullOrEmpty(this.Configuration.GetApiKeyWithPrefix("Authorization")))
            {
                localVarRequestOptions.HeaderParameters.Add("Authorization", this.Configuration.GetApiKeyWithPrefix("Authorization"));
            }

            // make the HTTP request
            var localVarResponse = this.Client.Get<ResourceDtoResponse>("/api/v2/resources/{resourceId}", localVarRequestOptions, this.Configuration);
            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("GetResource", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

        /// <summary>
        /// Retrieves a resource by its ID. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="resourceId">The ID of the resource to retrieve.</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ResourceDtoResponse</returns>
        public async System.Threading.Tasks.Task<ResourceDtoResponse> GetResourceAsync(Guid resourceId, int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
        {
            Coscine.ApiClient.Core.Client.ApiResponse<ResourceDtoResponse> localVarResponse = await GetResourceWithHttpInfoAsync(resourceId, operationIndex, cancellationToken).ConfigureAwait(false);
            return localVarResponse.Data;
        }

        /// <summary>
        /// Retrieves a resource by its ID. 
        /// </summary>
        /// <exception cref="Coscine.ApiClient.Core.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="resourceId">The ID of the resource to retrieve.</param>
        /// <param name="operationIndex">Index associated with the operation.</param>
        /// <param name="cancellationToken">Cancellation Token to cancel the request.</param>
        /// <returns>Task of ApiResponse (ResourceDtoResponse)</returns>
        public async System.Threading.Tasks.Task<Coscine.ApiClient.Core.Client.ApiResponse<ResourceDtoResponse>> GetResourceWithHttpInfoAsync(Guid resourceId, int operationIndex = 0, System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
        {

            Coscine.ApiClient.Core.Client.RequestOptions localVarRequestOptions = new Coscine.ApiClient.Core.Client.RequestOptions();

            string[] _contentTypes = new string[] {
            };

            // to determine the Accept header
            string[] _accepts = new string[] {
                "application/json",
                "text/json"
            };

            var localVarContentType = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderContentType(_contentTypes);
            if (localVarContentType != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Content-Type", localVarContentType);
            }

            var localVarAccept = Coscine.ApiClient.Core.Client.ClientUtils.SelectHeaderAccept(_accepts);
            if (localVarAccept != null)
            {
                localVarRequestOptions.HeaderParameters.Add("Accept", localVarAccept);
            }

            localVarRequestOptions.PathParameters.Add("resourceId", Coscine.ApiClient.Core.Client.ClientUtils.ParameterToString(resourceId)); // path parameter

            localVarRequestOptions.Operation = "ResourceApi.GetResource";
            localVarRequestOptions.OperationIndex = operationIndex;

            // authentication (Bearer) required
            if (!string.IsNullOrEmpty(this.Configuration.GetApiKeyWithPrefix("Authorization")))
            {
                localVarRequestOptions.HeaderParameters.Add("Authorization", this.Configuration.GetApiKeyWithPrefix("Authorization"));
            }

            // make the HTTP request
            var localVarResponse = await this.AsynchronousClient.GetAsync<ResourceDtoResponse>("/api/v2/resources/{resourceId}", localVarRequestOptions, this.Configuration, cancellationToken).ConfigureAwait(false);

            if (this.ExceptionFactory != null)
            {
                Exception _exception = this.ExceptionFactory("GetResource", localVarResponse);
                if (_exception != null)
                {
                    throw _exception;
                }
            }

            return localVarResponse;
        }

    }
}
