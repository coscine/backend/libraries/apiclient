﻿using Coscine.ApiClient.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coscine.ApiClient;

/// <summary>
/// Provides helper methods to work with paginated data.
/// </summary>
public static class PaginationHelper
{
    // TODO: The naming is confusing, since this method is awaitable. This should be GetAllAsync
    /// <summary>
    /// Asynchronously retrieves all items of type <typeparamref name="T"/> across all pages.
    /// </summary>
    /// <remarks>
    /// This method repeatedly calls the provided <paramref name="pageRequest"/> function to fetch items from multiple pages.
    /// It lazily generates an asynchronous sequence of items, yielding each item as it becomes available. The method employs reflection
    /// to dynamically interact with the response object of type <typeparamref name="S"/>, enabling a flexible approach to handle various types of paginated data.
    /// Ensure that the <typeparamref name="S"/> type has "Data" and "Pagination" properties, with "Data" being an <see cref="IEnumerable{T}"/>.
    /// </remarks>
    /// <typeparam name="S">The type of the paginated response object. This object must have "Data" and "Pagination" properties.</typeparam>
    /// <typeparam name="T">The type of the items in the paginated collection. This type must be a class.</typeparam>
    /// <param name="pageRequest">
    /// A function that takes an integer (representing the page number) and returns a <see cref="Task{TResult}"/> where <c>TResult</c> is <typeparamref name="S"/>.
    /// This function should encapsulate the logic to retrieve items for the specified page number.
    /// </param>
    /// <returns>
    /// An awaitable Task with an enumerable <see cref="IEnumerable{T}"/> that represents the sequence of items.
    /// The sequence is lazily generated, yielding items as they are retrieved from each page.
    /// </returns>
    /// <example>
    /// <para>
    /// Usage example:
    /// </para>
    /// <code>
    /// var allItems = PaginationHelper.GetAll&lt;ProjectAdminDtoPagedResponse, ProjectAdminDto&gt;(pageNumber => FetchItemsPageAsync(pageNumber));
    /// </code>
    /// <para>
    /// This asynchronously retrieves all items across all pages using the provided function <c>FetchItemsPageAsync</c>,
    /// which is expected to return a paginated response object with "Data" and "Pagination" properties.
    /// </para>
    /// </example>
    public static async Task<IEnumerable<T>> GetAll<S, T>(Func<int, Task<S>> pageRequest) where T : class
    {
        return await GetAllAsync<S, T>(pageRequest).ToListAsync();
    }

    // TODO: The naming is confusing, since this method is nothing which is awaitable. This should be something like GetAllAsyncList
    /// <summary>
    /// Retrieves all items of type <typeparamref name="T"/> across all pages with an asynchronous list.
    /// </summary>
    /// <remarks>
    /// This method repeatedly calls the provided <paramref name="pageRequest"/> function to fetch items from multiple pages.
    /// It lazily generates an asynchronous sequence of items, yielding each item as it becomes available. The method employs reflection
    /// to dynamically interact with the response object of type <typeparamref name="S"/>, enabling a flexible approach to handle various types of paginated data.
    /// Ensure that the <typeparamref name="S"/> type has "Data" and "Pagination" properties, with "Data" being an <see cref="IEnumerable{T}"/>.
    /// </remarks>
    /// <typeparam name="S">The type of the paginated response object. This object must have "Data" and "Pagination" properties.</typeparam>
    /// <typeparam name="T">The type of the items in the paginated collection. This type must be a class.</typeparam>
    /// <param name="pageRequest">
    /// A function that takes an integer (representing the page number) and returns a <see cref="Task{TResult}"/> where <c>TResult</c> is <typeparamref name="S"/>.
    /// This function should encapsulate the logic to retrieve items for the specified page number.
    /// </param>
    /// <returns>
    /// An asynchronous enumerable <see cref="IAsyncEnumerable{T}"/> that represents the sequence of items.
    /// The sequence is lazily generated, yielding items as they are retrieved from each page.
    /// </returns>
    /// <example>
    /// <para>
    /// Usage example:
    /// </para>
    /// <code>
    /// var allItems = PaginationHelper.GetAllAsync&lt;ProjectAdminDtoPagedResponse, ProjectAdminDto&gt;(pageNumber => FetchItemsPageAsync(pageNumber));
    /// </code>
    /// <para>
    /// This retrieves all items across all pages using the provided function <c>FetchItemsPageAsync</c>,
    /// which is expected to return a paginated response object with "Data" and "Pagination" properties and provides them with a yield statement.
    /// </para>
    /// </example>
    public static async IAsyncEnumerable<T> GetAllAsync<S, T>(Func<int, Task<S>> pageRequest) where T : class
    {
        var currentPage = 1;
        bool hasNext = true;

        while (hasNext)
        {
            var iteration = await pageRequest(currentPage);

            // Using reflection to dynamically access "Data" and "Pagination" properties
            var dataProperty = iteration?.GetType().GetProperty("Data");
            var paginationProperty = iteration?.GetType().GetProperty("Pagination");

            if (dataProperty?.GetValue(iteration) is IEnumerable<T> data &&
                paginationProperty?.GetValue(iteration) is Pagination pagination)
            {
                foreach (var item in data)
                {
                    yield return item;
                }
                hasNext = pagination.HasNext;
            }
            else
            {
                // Break the loop if properties are not found or cast fails
                hasNext = false;
            }

            currentPage++;
        }
    }
}
