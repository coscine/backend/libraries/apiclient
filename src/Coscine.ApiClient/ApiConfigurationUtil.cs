﻿using Coscine.ApiClient.ConfigurationModels;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Winton.Extensions.Configuration.Consul;

namespace Coscine.ApiClient;

/// <summary>
/// Provides utility methods for retrieving API configuration settings and generating JWT (JSON Web Token).
/// </summary>
public class ApiConfigurationUtil
{
    /// <summary>
    /// Retrieves the API configuration settings from either a specified Consul URL or environment variables, 
    /// accommodating different environments. This configuration includes JWT settings and other API-related settings.
    /// </summary>
    /// <param name="environment">The environment name to target, defaulting to "development".</param>
    /// <param name="givenConsulUrl">An optional URL to the Consul service. If not provided, the method attempts to use the CONSUL_URL environment variable.</param>
    /// <returns>An <see cref="IConfiguration"/> object containing the API configuration settings.</returns>
    public static IConfiguration RetrieveApiConfiguration(string environment = "development", string? givenConsulUrl = null)
    {
        var consulUrl = givenConsulUrl ?? Environment.GetEnvironmentVariable("CONSUL_URL") ?? "http://localhost:8500";

        var configuration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"appsettings.{environment}.json", optional: true, reloadOnChange: true)
            .AddConsul(
                "coscine/Coscine.Api/appsettings",
                options =>
                {
                    options.ConsulConfigurationOptions =
                        cco => cco.Address = new Uri(consulUrl);
                    options.Optional = true;
                    options.ReloadOnChange = true;
                    options.PollWaitTime = TimeSpan.FromSeconds(5);
                    options.OnLoadException = exceptionContext => exceptionContext.Ignore = true;
                }
            )
            .AddConsul(
                $"coscine/Coscine.Api/appsettings.{environment}",
                options =>
                {
                    options.ConsulConfigurationOptions =
                        cco => cco.Address = new Uri(consulUrl);
                    options.Optional = true;
                    options.ReloadOnChange = true;
                    options.PollWaitTime = TimeSpan.FromSeconds(5);
                    options.OnLoadException = exceptionContext => exceptionContext.Ignore = true;
                }
            )
            .AddEnvironmentVariables()
            .Build();
        
        return configuration;
    }

    /// <summary>
    /// Retrieves JWT configuration settings from the API configuration. This can include settings specific to JWT, such as issuer, audience, and keys.
    /// </summary>
    /// <param name="environment">The environment name for which to retrieve the configuration, defaulting to "development".</param>
    /// <param name="givenConsulUrl">An optional Consul URL to use instead of the environment variable.</param>
    /// <returns>A <see cref="JwtConfiguration"/> object populated with JWT-specific settings.</returns>
    public static JwtConfiguration RetrieveJwtConfiguration(string environment = "development", string? givenConsulUrl = null)
    {
        var configuration = RetrieveApiConfiguration(environment, givenConsulUrl);
        return RetrieveJwtConfiguration(configuration);
    }

    /// <summary>
    /// Binds a given <see cref="IConfiguration"/> instance to a <see cref="JwtConfiguration"/> object, extracting JWT-specific settings.
    /// </summary>
    /// <param name="configuration">The configuration instance from which to extract JWT settings.</param>
    /// <returns>A <see cref="JwtConfiguration"/> object populated with values from the provided configuration instance.</returns>
    public static JwtConfiguration RetrieveJwtConfiguration(IConfiguration configuration)
    {
        var jwtConfiguration = new JwtConfiguration();
        configuration.Bind(JwtConfiguration.Section, jwtConfiguration);
        return jwtConfiguration;
    }

    /// <summary>
    /// Generates an administration token with a default expiry of <b>1 day</b>.
    /// </summary>
    /// <param name="jwtConfiguration">The JWT configuration to use for token generation.</param>
    /// <param name="expiresInDays">The number of days until the token expires, defaulting to <c>1</c>.</param>
    /// <returns>A JWT <see cref="string"/> for an administrator token.</returns>
    public static string GenerateAdminToken(JwtConfiguration jwtConfiguration, int expiresInDays = 1)
    {
        var jwtSecurityToken = GenerateJwtSecurityToken(
            jwtConfiguration,
            new Dictionary<string, string>
            {
                [ClaimTypes.Role] = "administrator"
            },
            TimeSpan.FromDays(expiresInDays).TotalMinutes,
            jwtConfiguration.JsonWebKeys.First(x => x.KeySize >= 256)
        );

        return GetJwtStringFromToken(jwtSecurityToken);
    }

    /// <summary>
    /// Generates a JWT security token using specified payload, expiration time, and a signing key.
    /// </summary>
    /// <param name="jwtConfiguration">The JWT configuration containing the necessary settings for token generation.</param>
    /// <param name="payloadContents">A dictionary containing the payload contents of the token.</param>
    /// <param name="expiresInMinutes">The token's expiration time in minutes.</param>
    /// <param name="jsonWebKey">The Json Web Key used to sign the token.</param>
    /// <returns>A <see cref="JwtSecurityToken"/> instance.</returns>
    public static JwtSecurityToken GenerateJwtSecurityToken(JwtConfiguration jwtConfiguration, IReadOnlyDictionary<string, string> payloadContents, double expiresInMinutes, JsonWebKey jsonWebKey)
    {
        var now = DateTime.UtcNow;

        // Create claims for the JWT token
        var claims = payloadContents.Select(c => new Claim(c.Key, c.Value)).ToList();
        claims.Add(new Claim("iat", EpochTime.GetIntDate(now).ToString(), ClaimValueTypes.Integer64));

        // Create the security key from the secret key
        var securityKey = new SymmetricSecurityKey(Base64UrlEncoder.DecodeBytes(jsonWebKey.K));

        // Create signing credentials using the security key
        var signingCredentials = new SigningCredentials(securityKey, jsonWebKey.Alg);

        // Create the JWT token
        return new JwtSecurityToken(
            issuer: jwtConfiguration.ValidIssuers[0],
            audience: jwtConfiguration.ValidAudiences[0],
            claims: claims,
            notBefore: now,
            expires: now.AddMinutes(expiresInMinutes), // Token expiration time
            signingCredentials: signingCredentials
        );
    }

    /// <summary>
    /// Converts a <see cref="JwtSecurityToken"/> into its string representation.
    /// </summary>
    /// <param name="token">The <see cref="JwtSecurityToken"/> to convert.</param>
    /// <returns>A <see cref="string"/> representation of the JWT.</returns>
    public static string GetJwtStringFromToken(JwtSecurityToken token)
    {
        // Generate the encoded JWT token
        var tokenHandler = new JwtSecurityTokenHandler();

        return tokenHandler.WriteToken(token);
    }
}
