﻿using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;

namespace Coscine.ApiClient.ConfigurationModels;

/// <summary>
/// Represents the configuration settings for JSON Web Tokens (JWT) used in the application.
/// </summary>
public class JwtConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "JwtConfiguration";

    /// <summary>
    /// Indicates whether the issuer should be validated during token validation.
    /// </summary>
    public bool ValidateIssuer { get; set; } = true;

    /// <summary>
    /// Indicates whether the audience should be validated during token validation.
    /// </summary>
    public bool ValidateAudience { get; set; } = true;

    /// <summary>
    /// Indicates whether the lifetime should be validated during token validation.
    /// </summary>
    public bool ValidateLifetime { get; set; } = true;

    /// <summary>
    /// Indicates whether the issuer signing key should be validated during token validation.
    /// </summary>
    public bool ValidateIssuerSigningKey { get; set; } = true;

    /// <summary>
    /// A list of valid issuers for the JWT.
    /// </summary>
    public List<string> ValidIssuers { get; set; } = new List<string>();

    /// <summary>
    /// A list of valid audiences for the JWT.
    /// </summary>
    public List<string> ValidAudiences { get; set; } = new List<string>();

    /// <summary>
    /// The default expiration time in minutes for generated JWTs.
    /// </summary>
    public int ExpiresInMinutes { get; set; }

    /// <summary>
    /// A list of Json Web Keys used for signing the JWT.
    /// </summary>
    public List<JsonWebKey> JsonWebKeys { get; set; } = new List<JsonWebKey>();
}